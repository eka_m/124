<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Collection;
use App\Models\Site\Mix;
use App\Models\Site\Works;
use App\Models\Site\Page;
use App\Models\Site\Portfolio;
use App\Models\Site\PortfolioItem;
use Illuminate\Http\Request;


class PagesController extends BaseController
{

	public function show ( Request $request, $url )
	{
		$funcname = 'get' . ucfirst( $url );
		if ( method_exists( $this, $funcname ) ) {
			return call_user_func_array( [ $this, $funcname ], [ $request, $url ] );
		}

		$page = Page::slug( $url )->firstOrFail();
		$data = compact( 'page' );
		return view( 'pages.pages.' . $url, $data );
	}

	public function getGallery ( Request $request, $category = 'all' )
	{

		$portfolio = Collection::where( 'slug', 'mix' )->firstOrFail();
		$categories = makeCollection( $portfolio->scheme )->where( 'name', 'category' )->first()[ 'value' ];
		$categories = explode( ',', $categories );
		$items = Mix::orderBy( 'position', 'ASC' )->where( function ( $query ) use ( $category ) {
			if ( $category === 'all' ) {
				return $query;
			}
			return $query->where( 'category', $category );
		} )->paginate( 12 );

		if ( $request->ajax() ) {
			$data = [];
			foreach ( $items as $item ) {
				$data[] = view( 'partials.gallery-item', compact( 'item' ) )->render();
			}
			return response()->json( [ "items" => $data, "pagination" => [ "total" => $items->total(), "current_page" => $items->currentPage(), "last_page" => $items->lastPage() ] ] );
		}
		$page = Page::slug( 'gallery' )->firstOrFail();
		return view( 'pages.pages.gallery', compact( 'page', 'categories', 'items' ) );
	}

	public function getPortfolio ( Request $request, $url )
	{
		$page = Page::slug( $url )->firstOrFail();
		$projects = Works::orderBy( 'position', 'ASC' )->paginate( 12 );
		if ( $request->ajax() ) {
			$data = [];
			foreach ( $projects as $project ) {
				$data[] = view( 'partials.portfolio-item', compact( 'project' ) )->render();
			}
			return response()->json( [ "items" => $data, "pagination" => [ "total" => $projects->total(), "current_page" => $projects->currentPage(), "last_page" => $projects->lastPage() ] ] );
		}
		return view( 'pages.pages.' . $url, compact( 'page', 'projects' ) );
	}

	public function showProject ( $slug )
	{
		$project = Works::where( 'slug', $slug )->firstOrFail();
		return view( 'pages.pages.portfolio-show', compact( 'project' ) );
	}
}
