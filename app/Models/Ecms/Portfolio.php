<?php

namespace App\Models\Ecms;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'portfolio';

    protected $fillable = [
        'name',
        'slug',
        'status',
        'categories',
        'content',
        'keywords',
        'description',
        'params',
        'status',
    ];

    protected $casts = [
        'content' => 'array',
        'keywords' => 'array',
        'description' => 'array',
    ];

    protected $rules = [
        'name' => 'required',
        'slug' => 'required|unique:portfolio,slug',
    ];

    public function items() {
        return $this->hasMany('App\Models\Ecms\PortfolioItem');
    }
}
