<div class="container-fluid" data-scroll-reveal="enter left move 200px over 1s after 0.5s">
    <div class="row">
        <div class="col-xs-12 no-padding" data-aos-delay="500" data-aos-duration="500" data-aos="fade-up" data-aos-once="true">
            <div id="map"></div>
        </div>
    </div>
</div>
