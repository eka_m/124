import localization from "./store/localization-store";
import slimImage from "./store/slim-image-store";
import helpers from "./store/helpers-store";
import gallery from "./store/gallery-store";
export default {
    lang: localization,
    helpers: helpers,
    slimImage:slimImage,
    gallery:gallery,
    init(obj, value) {
        return _.merge(this[obj],value);
    },
    set(obj, key, value) {
        return _.set(this[obj], key, value);
    },
    get(obj, key, value = false) {
        return _.get(this[obj], key, value);
    }
};
