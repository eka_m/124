@extends('ecms::layouts.base')
@section('title','Editing the portfolio '.json_decode($item->name,true)[\Setting::get('languages.default')])
@section('js')
<script src="{{asset('ecms/plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('ecms.portfolio.update',$item->id)}}" method="post">
				@csrf @method('PUT')
				<div class="card">
					<div class="card-status bg-teal"></div>
					<div class="card-header">
						<h3 class="card-title">Editing portfolio {{json_decode($item->name,true)[\Setting::get('languages.default')]}}</h3>
						<div class="card-tabs pl-5 ml-5">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
							</ul>
						</div>
						<div class="card-options">
							<language-toggler></language-toggler>
							<a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
						</div>
					</div>
					<div class="card-body">
						<div class="dimmer loading-content active">
							<div class="loader"></div>
							<div class="dimmer-content">
									@include('ecms::pages.portfolio.__form')
							</div>
						</div>
					</div>
					<div class="card-footer">
						<div class="form-group text-right">
							<button class="btn btn-pill btn-teal">Save</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
