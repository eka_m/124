<!--
START: Navbar Mobile

Additional Classes:
.nk-navbar-dark
.nk-navbar-align-center
.nk-navbar-align-right
.nk-navbar-items-effect-1
.nk-navbar-drop-effect-1
.nk-navbar-drop-effect-2
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center nk-navbar-dark nk-navbar-drop-effect-1" id="nk-nav-mobile">

  <div class="nk-navbar-bg">
    <div class="bg-image">
      {{--<img src="{{asset('site/img/bg-menu-full.jpg')}}" alt="" class="jarallax-img">--}}
    </div>
  </div>

  <div class="nk-nav-table">
    <div class="nk-nav-row">
      <div class="container-fluid">
        <div class="nk-nav-header">

          <div class="nk-nav-logo">
            <a href="{{route('home')}}" class="nk-nav-logo al">

              <img src="{{asset('site/img/logo-white.png')}}" alt="" width="60" class="nk-nav-logo-img-dark">
              <img src="{{asset('site/img/logo-white.png')}}" alt="" width="60" class="nk-nav-logo-img-light">

            </a>
          </div>

          <div class="nk-nav-close nk-navbar-full-toggle">
            <span class="nk-icon-close"></span>
          </div>
        </div>
      </div>
    </div>
    <div class="nk-nav-row-full nk-nav-row">
      <div class="nano">
        <div class="nano-content">
          <div class="nk-nav-table">
            <div class="nk-nav-row nk-nav-row-full nk-nav-row-center nk-navbar-mobile-content">
              <ul class="nk-nav">
                <!-- Here will be inserted menu from [data-mobile-menu="#nk-nav-mobile"] -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="nk-nav-row">
      <div class="container">
        <div class="nk-social">
          <ul>
            <li><a class="nk-social-facebook" href="https://www.facebook.com/"><span><span class="nk-social-front fa fa-facebook"></span><span class="nk-social-back fa fa-facebook"></span></span></a></li>
            <li><a class="nk-social-instagram" href="https://www.instagram.com/"><span><span class="nk-social-front fa fa-instagram"></span><span class="nk-social-back fa fa-instagram"></span></span></a></li>
            <li><a class="nk-social-youtube" href="https://www.instagram.com/"><span><span class="nk-social-front fa fa-youtube-play"></span><span class="nk-social-back fa fa-youtube"></span></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>
<!-- END: Navbar Mobile -->