@extends('layouts.base')
@section('title', " | ".$page->name)
@section('content')
	<div class="nk-main">
		<div class="nk-fullpage nk-fullpage-split nk-fullpage-nav-style-1 bg-black">
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/creative-sec-icon.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<h2 class="nk-fullpage-title display-4 text-white r-head"><a href="">CREATIVE</a>
										</h2>
										<p class="text-white r-text mb-0">
											Just like people, every company is different. This is why <span class="text-main">we focus on meeting your real needs</span>,
											building
											brands with the values you want to spread. Our work is finalized to give you a clear identity,
											make
											you
											recognizable, raise your brand awareness and bolster your customer loyalty.
										</p>
										<ul class="text-white r-text">
											<li>Branding</li>
											<li>Graphic design</li>
											<li>Video production</li>
											<li>Photo shooting</li>
											<li>Copywriting</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/social-sec-icon.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<h2 class="text-white nk-fullpage-title r-head"><a href="">SOCIAL</a>
										</h2>
										<p class="text-white r-text mb-0">Social media have become an active part of our everyday life.
											Facebook,
											Twitter, Instagram, Youtube…
											The development of an accurate social media strategy is essential to make your
											name known and to establish an enduring relationship with your users.
											<span class="text-main">We know how to deliver it.</span></p>
										<ul class="text-white r-text">
											<li>Social Media Profiles creation and management</li>
											<li>Social Media Marketing</li>
											<li>Advertising campaign on Social Networks</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/world-wide-web.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<h2 class="nk-fullpage-title display-4 text-white r-head"><a href="">WEB</a>
										</h2>
										<p class="text-white r-text mb-0">
											When consumers visit a website, on average it takes just 7 seconds to communicate a message to
											their
											mind about your company. We are able to develop the perfect balance of great communication and
											usability, while giving you the best results in terms of online performances.
											<span class="text-main">Starting from scratch, we
                bring any project from conception to realization.</span>
										</p>
										<ul class="text-white r-text">
											<li>Websites creation and managment</li>
											<li>Search engine optimization (SEO)</li>
											<li>Google advertising</li>
											<li>E-mail marketing (newsletter management)</li>
											<li>E-commerce</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>

			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/promotion.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<h2 class="nk-fullpage-title display-4 text-white r-head"><a href="">PR & MEDIA</a>
										</h2>
										<p class="text-white r-text mb-0">
											<span class="text-main">We strongly believe in human relationships.</span> And that is why over
											time we
											have invested in the creation
											of an international network of people connected with us. Through our network, promoting our and
											your
											projects becomes easy. At the same time, we believe in the power of the Media, as information
											disseminators and strategic partners for our communication campaigns.
										</p>
										<ul class="text-white r-text">
											<li>Networking</li>
											<li>PR management</li>
											<li>Press Office</li>
											<li>Media strategy</li>
											<li>Social and Corporate Events organization</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/musical-sec-icon.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-8 offset-md-2">
										<h2 class="nk-fullpage-title display-4 text-white r-head"><a href="">MUSICAL</a>
										</h2>
										<p class="text-white r-text mb-0">
											We love music. We believe in music as something that unites people, merges cultures, lights up
											hearts.
											As a music agency, we want to give local and international talented artists the opportunities they
											deserve and make them shine like stars. <span
												class="text-main">For a united world, dare the challenge.</span>
										</p>
										<ul class="text-white r-text">
											<li>Original Soundtracks composition and production for Cinema/TV and Commercials</li>
											<li>Video Clip production</li>
											<li>Artists & Musicians recruitment for concerts/events</li>
											<li>Music-composition concept development for events</li>
											<li>Sound design & Electronic music composition for installations and exhibitions</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
