<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    @include('ecms::partials.meta')
    @include('ecms::partials.css')
</head>

<body class="">
    <div id="app" class="page page-wrapper chiller-theme sidebar-bg bg1">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="javascript:void(0)">
                    <i class="fas fa-bars"></i>
                </a>
    @include('ecms::partials.sidebar')
        <div class="page-main page-content">
            {{--
    @include('ecms::partials.header')
    @include('ecms::partials.nav') --}}
            <div class="my-3 my-md-5">
                @yield('content')
            </div>
        </div>
        {{--
    @include('ecms::partials.footer-nav') --}}
    @include('ecms::partials.footer')
    </div>
    @include('ecms::partials.js')
</body>

</html>