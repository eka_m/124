@extends('layouts.base')
@section('title', ' | Welcome')
@section("js")
  @parent
  <script type="text/javascript" src="{{asset("plugins/three/three.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("plugins/perlin/perlin.js")}}"></script>
  <script type="text/javascript" src="{{asset("plugins/tween-max/TweenMax.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("site/js/animation-home.min.js")}}"></script>
@endsection
@section("content")
  <canvas class="scene scene--full" id="scene"></canvas>
  @include('partials.content-sections')
@endsection