<?php

namespace App\Http\Controllers\Ecms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends BaseController
{
    public function show() {
        return view('ecms::pages.home.index');
    }
}
