import getSlug from "speakingurl";
const Helpers = {
    install(Vue, options) {
        Vue.prototype.$helpers = this.methods; // we use $ because it's the Vue convention
        String.prototype.isJson = function() {
            try {
                JSON.parse(this.toString());
                return true;
            } catch (ex) {
                return false;
            }
        };
    },
    methods: {
        isEmpty(value) {
            switch (value) {
                case "":
                case 0:
                case "0":
                case null:
                case false:
                case undefined:
                    return true;
                    break;
                default:
                    return false;
            }
        },
        languages: () => {
            if (!$('meta[name="languages"]').length) {
                console.log('ERROR:: Meta tag "languages" is not exists');
                return false;
            }
            return JSON.parse($('meta[name="languages"]').attr("content"));
        },
        checkTranslates(data) {
            const languages = this.languages();
            if (languages) {
                return languages.all.filter(
                    x => !Object.keys(data).includes(x)
                );
            }
            return false;
        },
        array_values(input) {
            var tmp_arr = new Array(),
                cnt = 0;
            for (key in input) {
                tmp_arr.push(input[key]);
                cnt++;
            }
            return tmp_arr;
        },
        generateSlug(value) {
            return getSlug(value, {
                separator: "-",
                custom: {
                    ə: "e",
                    ü: "u",
                    ç: "ch",
                    ğ: "g",
                    ö: "o",
                    ş: "sh"
                }
            });
        },
        randomColor() {
            const letters = "0123456789ABCDEF";
            var color = "#";
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
    }
};
export default Helpers;
