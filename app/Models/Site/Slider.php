<?php

namespace App\Models\Site;

use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use ImageManager;
    protected $table = "slider";

    public function getImageAttribute($value) {
        return $this->makeImage(json_decode($value));
    }
}
