<?php

namespace App\Models\Site;

use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Mix extends Model
{
	use ImageManager;
	protected $table = 'mix';

	public function getImageAttribute ( $value )
	{
		return $this->makeImage( json_decode($value) );
	}
}
