<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Portfolio;
use App\Models\Ecms\PortfolioItem;
use Illuminate\Http\Request;

class PortfolioController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Portfolio::orderBy('created_at', 'DESC')->get();
        return view('ecms::pages.portfolio.index', compact('items'));
    }

    public function items($id)
    {
        $portfolio = Portfolio::with('items')->findOrFail($id);
        return view('ecms::pages.portfolio.items', compact('portfolio'));
    }

    public function storeItems(Request $request, $id)
    {
        $portfolio = Portfolio::find($id);
        try {
						$portfolioItems = [];
						$pos = 0;
            foreach (json_decode($request->items, true) as $item) {
                $itemObject = new PortfolioItem();
                $itemObject->fill($item);
                $itemObject->portfolio_id = $id;
                $itemObject->position = $pos;
								$portfolioItems[] = $itemObject;
								$pos ++;
            }
            $portfolio->items()->delete();
            $portfolio->items()->saveMany($portfolioItems);
            \Toastr::success("Items was successfully added!");
            return redirect()->route("ecms.portfolio.items", $id);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new Portfolio;
        return view('ecms::pages.portfolio.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $portfolio = new Portfolio;
            $portfolio->fill($request->all());
            $request->slug = str_slug(json_decode($request->name, true)[\Setting::get('languages.default')], '-');
            if ($portfolio->save()) {
                \Toastr::success("Portfolio was successfully added!");
                return redirect()->route("ecms.portfolio.index");
            }
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index")->with(['errors' => $portfolio->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ecms\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portfolio $portfolio)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ecms\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Portfolio::find($id);
        return view('ecms::pages.portfolio.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ecms\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $portfolio = Portfolio::find($id);
            $portfolio->fill($request->all());
            $request->slug = str_slug(json_decode($request->name, true)[\Setting::get('languages.default')], '-');
            if ($portfolio->save()) {
                \Toastr::success("Portfolio was successfully saved!");
                return redirect()->route("ecms.portfolio.index");
            }
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index")->with(['errors' => $portfolio->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ecms\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $portfolio = Portfolio::find($id);
            $portfolio->items()->delete();
            $portfolio->delete();
            \Toastr::success("Portfolio was successfully deleted!");
            return redirect()->route("ecms.portfolio.index");
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.portfolio.index");
        }
    }
}
