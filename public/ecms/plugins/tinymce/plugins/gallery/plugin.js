tinymce.PluginManager.add('gallery', function (editor, url) {
    function insert_or_replace(content) {
        editor.focus();
        if (editor.selection) editor.selection.setContent(content);
        else editor.insertContent(content);
    }

    function buildListItems(inputList, itemCallback, startItems) {
        function appendItems(values, output) {
            output = output || [];

            tinymce.each(values, function (item) {
                var menuItem = {text: item.text || item.title, value: ''};
                itemCallback(menuItem, item);
                output.push(menuItem);
            });

            return output;
        }

        return appendItems(inputList, startItems || []);
    }

    function ajax(cb) {
        return function () {
            editor.setProgressState(1); // Show progress
            tinymce.util.XHR.send({
                url: '/admin/gallery',
                success: function (res) {
                    editor.setProgressState(0); // Hide progress
                    cb(!!res ? tinymce.util.JSON.parse(res) : res);
                }
            });
        };
    }

    function popup(data) {
        var listBox, win;
        win = editor.windowManager.open({
            title: 'Gallery',
            width: 400,
            height: 100,
            body: [
                {
                    type: 'listbox',
                    name: 'galleries',
                    label: 'Select',
                    tooltip: 'Select item',
                    values: buildListItems(data, function (item, datum) {
                        item.value = datum;
                        item.text = datum.name;
                    }),
                    onPostRender() {
                        listBox = this;
                    }
                }
            ],
            buttons: [
                {
                    text: 'Insert',
                    subtype: 'primary',
                    onclick() {
                        var selected = listBox.value();
                        var strStyles = `width: 100%; height: 200px; margin: 10px auto; padding: 5px; text-align: center; background:#0096F3;  color:#FFFFFF; display: flex; align-items: center;
                                justify-content: center; `;
                        var styles = tinymce.util.JSON.serialize({
                            width: "100%",
                            margin: "10px 0 0 10px",
                            float: "none"
                        });
                        if (!!selected)
                            editor.insertContent(
                                `
                                <gallery id="${selected.id}" 
                                class="mceNonEditable"
                                style="${strStyles}"
                                params="${styles}"
                                >
                                Gallery:: <a style="color:#FFFFFF;" href="/admin/gallery/${selected.id}/edit">${selected.name}</a>
                                </gallery>
                                
                                `);
                        win.close();
                    }
                },
                {text: 'Cancel', onclick: 'close'}
            ]
        });
    }

    function edit(elem) {
        let win,
            width = "100%",
            align = "none";
        let margin = {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
        };
        win = editor.windowManager.open({
            title: 'Gallery',
            width: 480,
            height: 130,
            body: [
                {
                    type: "container",
                    layout: "flex",
                    label: "Width",
                    spacing:10,
                    items: [
                        {
                            type: 'textbox', name: 'width', label: 'Width px', title: 'Width', text:'width',
                            oninput() {
                                width = (this.value() + "px");
                            }
                        },
                        {
                            type: 'listbox', label: 'Float', name: "align",
                            values: [
                                {text: "Center", value: "none"},
                                {text: "Right", value: "right"},
                                {text: "Left", value: "left"},
                            ],
                            onselect() {
                                align = this.value()
                            }
                        }
                    ]
                },
                {
                    type: 'form',
                    label: 'Margin',
                    oninput() {
                        let $values = this.toJSON();
                        tinymce.each($values, function (item, index) {
                            margin[index] = item && item !== "" && item !== null ? item + "px" : 0;
                        });
                    },
                    items: [
                        {
                            type: 'container',
                            layout: 'flex',
                            spacing: 5,
                            items: [
                                {type: 'textbox', name: 'top', label: 'Top', size: 10},
                                {type: 'textbox', name: 'right', label: 'Right', size: 10},
                                {type: 'textbox', name: 'bottom', label: 'Bottom', size: 10},
                                {type: 'textbox', name: 'left', label: 'Left', size: 10},
                            ]

                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Insert',
                    subtype: 'primary',
                    onclick() {
                        const gallery = $(elem);
                        let styles = {
                            width: width,
                            float: align,
                            margin: Object.values(margin).join(" ")
                        };
                        gallery.css(styles);
                        gallery.attr('params', JSON.stringify(styles));
                        win.close();
                    }
                },
                {text: 'Cancel', onclick: 'close'}
            ]
        });
    }


    editor.addButton('gallery', {
        tooltip: 'Select gallery',
        image: url + '/gallery.png',
        onclick: ajax(popup)
    });

    editor.on('DblClick', (e) => {
        if (e.target.nodeName.toLowerCase() === "gallery") {
            edit(e.target);
        }
    });


});
