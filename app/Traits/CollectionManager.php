<?php

namespace App\Traits;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

trait CollectionManager
{

    protected $fields = [
        'text' => 'text',
        'textarea' => 'text',
        'editor' => 'longtext',
        'image' => 'string',
        'file' => 'string',
        'cropableimage' => 'string',
        'date' => 'string',
        'datetime' => 'string',
        'select' => 'text',
    ];

    /* CREATING  table & model ____________________________________________________________________*/

    public function createTable($model)
    {
        $fields = $this->fields;
        $scheme = $model->scheme;
        $name = strtolower($model->name);
        if (!Schema::hasTable($name)) {
            Schema::create($name, function (Blueprint $table) use ($fields, $scheme) {
                $table->increments('id');
                $table->integer('collection_id')->default(0);
                foreach ($scheme as $value) {
                    $table->{$fields[$value->type]}($value->name)->nullable(!$value->required);
                }
                $table->string('slug')->nullable();
                $table->integer('position')->default(0);
                $table->boolean('active')->default(false);
                $table->timestamps();
            });
            $this->createModel($model);
        }
        return $this;

    }

    private function createModel($model)
    {
        $name = $this->getModelName($model->name);
        $fileName = $this->getModelName($model->name, true);
        $stub = $this->getStub();
        $content = $this->replaceClassName($stub, strtolower($name));
        $content = $this->replaceTable($content, strtolower($name));
        $content = $this->replaceFillable($content, $model->scheme);
        $content = $this->replaceRules($content, $model->scheme);
        return Storage::disk('app')->put($fileName, $content);
    }

    /* END CREATING  table & model ____________________________________________________________________*/

    /* EDITING  table & model ____________________________________________________________________*/

    public function updateTable($model, $oldModel)
    {
        $scheme = makeCollection($model->scheme);
        $tableName = strtolower($model->name);
        $oldModelName = $oldModel['name'];
        $oldScheme = makeCollection(json_decode($oldModel['scheme']));

        $columnsToDrop = [];
        $columnsToRename = [];
        foreach ($oldScheme as $field) {
            $elem = $scheme->firstWhere("id", $field->id);
            if ($elem === null) {
                $columnsToDrop[] = $field->name;
            }
            if ($elem !== null) {
                if ($field->name != $elem->name) {
                    $columnsToRename[$field->name] = $elem->name;
                }
            }
        }
        if ($tableName !== $oldModelName) {
            if (Schema::hasTable($oldModelName)) {
                Schema::rename($oldModelName, $tableName);
            }
        }
        if (Schema::hasTable($tableName)) {
            $this->renameColumns($tableName, $columnsToRename);
            $this->updateColumns($tableName, $scheme);
            $this->dropColumns($tableName, $columnsToDrop);
            $this->updateModel($model, $oldModelName);
        }
        return $this;
    }

    private function updateColumns($tableName, $scheme)
    {
        $fields = $this->fields;
        Schema::table($tableName, function (Blueprint $table) use ($tableName, $scheme, $fields) {
            $after = 'collection_id';
            foreach ($scheme as $value) {
                if (Schema::hasColumn($tableName, $value->name)) {
                    $table->{$fields[$value->type]}($value->name)->nullable(!$value->required)->change();
                } else {
                    $table->{$fields[$value->type]}($value->name)->nullable(!$value->required)->after($after);
                    $after = $value->name;
                }
            }
        });
    }

    private function renameColumns($tableName, $columnsToRename)
    {
        if (!empty($columnsToRename)) {
            Schema::table($tableName, function (Blueprint $table) use ($tableName, $columnsToRename) {
                foreach ($columnsToRename as $oldName => $newName) {
                    if (Schema::hasColumn($tableName, $oldName)) {
                        $table->renameColumn($oldName, $newName);
                    }
                }
            });
        }
    }

    private function dropColumns($tableName, $columnsToDrop)
    {
        if (!empty($columnsToDrop)) {
            Schema::table($tableName, function (Blueprint $table) use ($tableName, $columnsToDrop) {
                if (!empty($columnsToDrop)) {
                    foreach ($columnsToDrop as $column) {
                        if (Schema::hasColumn($tableName, $column)) {
                            $table->dropColumn($column);
                        }
                    }
                }
            });
        }
    }

    public function updateModel($model, $oldModel)
    {
        $fileName = $this->getModelName($oldModel);
        if (Storage::disk('app')->exists($fileName)) {
            Storage::disk('app')->delete($fileName);
        }
        return $this->createModel($model);
    }


    /* END EDITING  table & model ____________________________________________________________________*/

    private function getStub()
    {
        return Storage::disk('app')->get('/Stubs/model.stub');
    }

    protected function replaceClassName($stub, $name)
    {
        return str_replace(
            '{{class}}', ucfirst($name), $stub
        );
    }

    protected function replaceTable($stub, $table)
    {
        return str_replace(
            '{{table}}', $table, $stub
        );
    }

    protected function replaceFillable($stub, $scheme)
    {
        if (!empty($scheme)) {
            $fillable = ["'slug'", "'position'", "'collection_id'", "'active'"];
            foreach ($scheme as $value) {
                $fillable[] = "'" . $value->name . "'";
            }
            $fillable = implode(',', $fillable);
        }
        return str_replace(
            '{{fillable}}', isset($fillable) ? $fillable : '', $stub
        );
    }

    protected function replaceRules($stub, $scheme)
    {
        if (!empty($scheme)) {
            $rules = [];
            foreach ($scheme as $value) {
                if ($value->required) {
                    $rules[] = "'$value->name' => 'required'";
                }
            }
            $rules = implode(',', $rules);
        }
        return str_replace(
            '{{rules}}', isset($rules) ? $rules : '', $stub
        );
    }

    private function getModelName($name, $fullpath = false)
    {
        $name = str_plural(ucfirst($name), 1);
        return $fullpath ? "/Models/Ecms/$name.php" : $name;
    }
}
