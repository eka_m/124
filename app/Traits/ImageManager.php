<?php

namespace App\Traits;

use File;
trait ImageManager
{

    public function setImages($image)
    {
        if (!$this->isJson($image)) {
            return $image;
        }
        $result = [];
        $image = json_decode($image);
        foreach ($image as $type => $img) {
            $result[$type] = $this->makeImage(json_decode($img));
        }
        return $result;
    }

    public function makeImage($image)
    {
//        try {
            if (isset($image->params) && !empty($image->params)) {
//                $img = \Image::cache(function ($pic) use ($image) {
//                    $w = round($image->params->width);
//                    $h = round($image->params->height);
//                    $x = round($image->params->x);
//                    $y = round($image->params->y);
//                    $res = $pic->make(public_path() . $image->path)->crop($w, $h, $x, $y);
//                    $filename = str_after($image->path, '/uploads/images/');
//                    if(!Storage::disk('uploads')->exists('optimized/'.$filename)) {
////                        Storage::disk('uploads')->put( 'optimized/'. $filename, $res);/**/
//                        $res->save(public_path('/optimized/' .$filename));
//                    }
//
//                    return $res->encode('data-url');
//                });
                return $this->optimize($image);
            }
//        } catch (\Exception $e) {
//            return "no_image";
//        }
    }

    public function optimize($image,$quality = 60) {
        $originalImage = trim($image->path,'"');
        if(File::exists(public_path().$image->path)) {

            $w = round($image->params->width);
            $h = round($image->params->height);
            $x = round($image->params->x);
            $y = round($image->params->y);

            $imageName = basename($originalImage);
            $directory = rtrim($originalImage, $imageName);
            $imagedata = "$w-$h-$x-$y"."_";

            $thumbDirectory = "optimized/";

            $oldimage = str_replace($imageName, $thumbDirectory."*".$imageName, $originalImage);
            $newImageName = str_replace($imageName, $thumbDirectory.$imagedata.$imageName, $originalImage);

            if(File::exists(public_path().$newImageName)) {
                return $newImageName;
            }

            $optimizedimg = \Image::make(public_path().$image->path)->crop($w, $h, $x, $y);

            if(!File::isDirectory(public_path().$directory.$thumbDirectory)) {
                File::makeDirectory(public_path().$directory.$thumbDirectory);
            }

            $optimizedimg->save(public_path().$newImageName,$quality);
        };
        return $originalImage;
    }

    public function makeImagePacker($image)
    {
        try {
            if (isset($image->params) && !empty($image->params)) {
                $w = $image->params->width;
                $h = $image->params->height;
                $x = $image->params->x;
                $y = $image->params->y;
                $image = \Packer::img(public_path() . $image->path, "resizeCrop,$w,$h,$x,$y");

                return $image;
            }
            return $image;
        } catch (\Exception $e) {
            return "no_image";
        }
    }

    public function getImageManagerAttributes()
    {
        return is_array($this->imageManager)
            ? $this->imageManager
            : [];
    }

    public function isImageManagerAttribute(string $key)
    {
        return in_array($key, $this->getImageManagerAttributes());
    }

    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getImageManagerAttributes() as $name) {
            if (isset($attributes[$name])) {
                $attributes[$name] = $this->setImages($attributes[$name]);
            }
        }
        return $attributes;
    }

    public function isJson($string)
    {
        try {
            return json_decode($string);
        } catch (\Exception $e) {
            return false;
        }
    }
}
