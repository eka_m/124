<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Auth::routes();
Route::group( [ 'prefix' => LaravelLocalization::setLocale() ], function () {
	Route::get( '/', 'Site\HomeController@show' )->name( 'home' );
	Route::get( '/page/gallery/{category?}', 'Site\PagesController@getGallery' );
	Route::get( '/page/move', 'Site\PagesController@movePortfolio' );
	Route::get( '/page/{url}', 'Site\PagesController@show' )->name( 'page' );
	Route::get( '/project/{slug}', 'Site\PagesController@showProject' )->name( 'showProject' );
} );
