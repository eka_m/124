<?php

namespace App\Models\Site;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Base
{
    use HasTranslations;
    use SoftDeletes;

    protected $casts = ['name' => 'array', 'content' => 'array'];

    public $translatable = ['name', 'content', 'keywords', 'description'];

    public function getImageAttribute($value)
    {
        if (isJSON($value)) {
            return $this->makeImage(json_decode($value));
        }
        return $value;
    }

    public function getCoverAttribute($value)
    {
        if (isJSON($value)) {
            return $this->makeImage(json_decode($value));
        }
        return $value;
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\Site\Page', 'parent_id', 'id')->orderBy('position', 'ASC');
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 4, 'children')))->where('parent_id', '=', NULL)->get();
    }

    public static function slug($slug)
    {
        return static::where('slug', $slug);
    }

    public static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node->parent_id != $parent) continue;
            $node->children = self::mapTree($dataset, $node->id);
            $tree[$id] = $node;
        }
        return $tree;
    }
}
