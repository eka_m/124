@extends('ecms::layouts.base')
@section('title', 'Gallery')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Galleries</h4>
            <div class="card-options text-right">
              <a href="{{route('ecms.gallery.create')}}" class="btn btn-icon btn-teal"><i class="fas fa-plus"></i></a>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              @foreach ($items as $item)
                <div class="col-md-3">
                  <div class="card">
                    <div class="card-body text-center">
                      <div class="card-category">{{ucfirst($item->name)}}</div>
                      <div class="text-center mt-6">
                        <a href="{{route('ecms.gallery.edit',$item->id)}}" class="btn btn-azure">
                          <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="{{route('ecms.gallery.destroy',$item->id)}}" class="btn btn-danger"
                           data-method="delete" data-token="{{csrf_token()}}"
                           data-confirm="Are you sure?">
                          <i class="fa fa-trash"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
          <div class="card-footer">
            <div class="d-flex justify-content-md-end justify-content-center">
              {{ $items->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
