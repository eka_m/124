<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Gallery;
use Illuminate\Http\Request;

class GalleryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Gallery::orderBy('created_at', 'DESC');
        if ($request->ajax()) {
            $items = $query->get();
            return response()->json($items);
        }
        $items = $query->paginate(10);
        return view('ecms::pages.gallery.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new Gallery();
        return view('ecms::pages.gallery.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        try{
        $item = new Gallery();
        $item->fill($request->all());
        if ($item->save()) {
            \Toastr::success('Gallery was successfully created!');
            return redirect()->route('ecms.gallery.index');
        }
        \Toastr::error("Validation error ( ");
        return redirect()->route("ecms.gallery.index")->with(['errors' => $item->getErrors()]);
//        } catch (\Exception $e) {
//            \Toastr::success("Something went wrong ;( ");
//            return redirect()->route("ecms.gallery.index");
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Gallery::findOrFAil($id);
        return view('ecms::pages.gallery.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $item = Gallery::find($id);
            $item->fill($request->all());
            if ($item->save()) {
                \Toastr::success('Gallery was successfully updated!');
                return redirect()->route('ecms.gallery.index');
            }
            \Toastr::error("Validation error ( ");
            return redirect()->route("ecms.gallery.index")->with(['errors' => $item->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.gallery.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Gallery::destroy($id);
            \Toastr::success('Gallery was successfully removed!');
            return redirect()->route('ecms.gallery.index');
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.gallery.index");
        }
    }
}
