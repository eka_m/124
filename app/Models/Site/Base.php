<?php

namespace App\Models\Site;

use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    use ImageManager;
}
