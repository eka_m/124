<?php

namespace App\Http\Controllers\Ecms;

use Illuminate\Http\Request;

class LocaleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return view('ecms::pages.locale.index');
        } catch (\Exception $e) {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $result = [];
            $locales = json_decode($request->locales, true);
            foreach ($locales as $value) {
                $result[$value['slug']] = $value;
            }
            \Setting::set("languages.all", $result);
            \Setting::set("languages.default", $locales[0]['slug']);
            \Toastr::success("Locales was successfully saved!");
            return redirect()->route("ecms.locales.index");
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.locales.index");
        }
    }
}
