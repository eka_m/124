  <div class="nk-fullpage nk-fullpage-nav-style-2">
    <div class="nk-fullpage-item nk-fullpage-item-bg-dark">
      <div class="nk-fullpage-content">
        <!-- START: Carousel -->
        <div class="nk-gap nk-gap-5"></div>
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="text-white nk-fullpage-title text-center dis">WE WORK <span class="text-main">WITH</span></h2>
            </div>
          </div>
        </div>
        <div class="nk-carousel-2 nk-carousel-x05 nk-carousel-all-visible nk-carousel-dots-3 nk-carousel-arrows-2 nk-carousel-arrows-bottom-center" data-autoplay="2000" data-loop="true" data-dots="false" data-arrows="true" data-cell-align="center" data-parallax="false">
          <div class="nk-carousel-inner">
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Italian Embassy</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/embasyitaly.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Israel Embassy</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/embasyisrael.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">The Landmark Baku</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/thelandmark.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">The Landmark Hotel Baku</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/thelandmarkhotel.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Hyatt Regency Baku</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/hayatregency.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Ca' Foscari University <br/> of Venice</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/foscari.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">SizinAvropa.az</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/sizinavropa.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">AMV</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/amv.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Scalini</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/scalini.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">United Cultures</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/unitedcultures.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Kokako Music</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/kokaomusic.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div>
                <div class="nk-carousel-absolute-item nk-carousel-active-show d-none d-sm-block">
                  <div class="row align-items-center">
                    <div class="col-lg-8">
                      <h3 class="display-3 text-white">Italian Trade Agency</h3>
                    </div>
                  </div>
                </div>
                <a href="#" class="nk-carousel-absolute-item"></a>
                <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                    <img src="{{asset('uploads/images/partners/italiantradeagency.jpg')}}" alt="" class="nk-img-stretch op-4">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END: Carousel -->
      </div>
    </div>
  </div>