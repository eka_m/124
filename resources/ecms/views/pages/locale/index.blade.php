@extends('ecms::layouts.base') 
@section('title', 'Localization') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('ecms.locales.store')}}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-status bg-teal"></div>
                    <div class="card-header">
                        <h3 class="card-title">Localization</h3>
                        <div class="card-options">
                            <a href="{{route('ecms.portfolio.create')}}" class="btn btn-teal" @click.prevent="$refs.locales.add">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <locales name="locales" ref="locales" current="{{json_encode(\Setting::get('languages.all'))}}"></locales>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-teal">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection