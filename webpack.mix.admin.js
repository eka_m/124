const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.browserSync({
//     open: 'external',
//     proxy: 'sizinavropa.dev',
//     host: 'sizinavropa.dev',
//     port: 8080
// });

mix.js('resources/ecms/js/app.js', 'public/ecms/js').extract([
    'jquery',
    'vue',
    'axios'
]).sourceMaps();
mix.babel('resources/ecms/js/custom.js', 'public/ecms/js/custom.js');
mix.babel('public/ecms/plugins/tinymce/plugins/gallery/plugin.js', 'public/ecms/plugins/tinymce/plugins/gallery/plugin.min.js');
mix.sass('resources/ecms/sass/app.scss', 'public/ecms/css')
    .options({
        processCssUrls: false,
        postcss: [
            require('autoprefixer')
        ],
    });

mix.webpackConfig({
    output: {
        publicPath: '/'
    }
});
// mix.copyDirectory('resources/ecms/img', 'public/ecms/img');
mix.copyDirectory('resources/ecms/fonts', 'public/ecms/fonts');
