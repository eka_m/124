@extends('layouts.base')
@section('title', " | ".$page->name)
@section("css")
	@parent
	<link rel="stylesheet" href="{{asset('plugins/fancybox/jquery.fancybox.min.css')}}">
@endsection
@section("js")
	@parent
	<!-- Isotope -->
	<script type="text/javascript" src="{{asset("plugins/fancybox/jquery.fancybox.min.js")}}"></script>
	<script type="text/javascript">
		$( '[data-fancybox="gallery"]' ).fancybox( {
			arrows: true,
		} );
	</script>
	<script type="text/javascript" src="{{asset('site/js/gallery.min.js')}}"></script>
@endsection
@section('content')
	<div class="nk-main bg-black">
		<!-- START: Works -->
		<div class="container-fluid bg-black">
			<div class="nk-gap-6"></div>
			<div class="nk-gap mt-5  d-none d-sm-block"></div>

			<h1 class="text-center text-white r-head">124...<span class="text-main"> CREATIVE WORKS</span></h1>
			{{--<div class="nk-subtitle-2 text-center mt-20">Branding / Design / Development</div>--}}
			<div class="nk-gap mnt-7 d-none d-sm-block"></div>

			<!-- START: Filter -->
			<div class="nk-pagination nk-pagination-nobg nk-pagination-center pb-40 text-white nk-isotope-filter-active">
				<a href="#nk-toggle-filter"><span class="nk-icon-squares"></span></a>
			</div>
			<ul class="nk-isotope-filter text-white nk-isotope-filter-active">
				<li data-filter="all" data-name="all">All</li>
				@foreach($categories as $category)
					<li data-filter=".{{str_slug($category)}}"
					    data-name="{{str_slug($category)}}">{{$category}}</li>
				@endforeach
			</ul>
			<!-- END: Filter -->

			<div id="gallery" class="nk-portfolio-list nk-isotope-4-cols loadmore_container isotope_load row" category="all">
				@foreach($items as $item)
					@include('partials.gallery-item', ['item' => $item])
				@endforeach
			</div>
		</div>
		<div class="nk-gap-3"></div>
		<div class="nk-pagination nk-pagination-center bg-dark text-white">
			<a href="{{route('page','gallery')}}"
			   id="loadmore_button_gallery"
			   total="{{$items->total()}}"
			   current-page="{{$items->currentPage()}}"
			   last-page="{{$items->lastPage()}}"
			   per-page="{{$items->perPage()}}"
			   class="d-none ">Load More Works</a>
		</div>

		<!-- START: Let's Work Together -->
		<div class="nk-box bg-black text-center">
			<div class="nk-gap-5"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<h2 class="text-white">LET`S WORK TOGETHER</h2>
						<div class="nk-gap-1 mnt-20"></div>
						<div class="nk-gap mnt-9"></div>
						<a href="{{route('page','contacts')}}"
						   class="nk-btn nk-btn-outline nk-btn-color-white nk-btn-hover-color-main">Contacts</a>
					</div>
				</div>
			</div>
			<div class="nk-gap-6"></div>
		</div>
		<!-- END: Let's Work Together -->

	</div>
	@include('partials.footer')
@endsection
