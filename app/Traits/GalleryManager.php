<?php

namespace App\Traits;

use Illuminate\Support\Facades\View;
use Sunra\PhpSimple\HtmlDomParser;
use App\Models\Site\Gallery;

trait GalleryManager
{

    private $styles = [
        "width" => 'width',
        "float" => "float",
        "margin" => "margin",
        "marginTop" => "margin-top",
        "marginRight" => "margin-right",
        "marginBottom" => "margin-bottom",
        "marginLeft" => "margin-left",
    ];

    public function makeGallery($html)
    {
        if (!$html && $html == "") return ["html" => $html, "galleries" => []];;
        $dom = HtmlDomParser::str_get_html($html);
        $tags = $dom->find('gallery');

        $galleries = collect([]);
        foreach ($tags as $tag) {
            $gallery = Gallery::find($tag->id);
            $stylesAttr = json_decode(htmlspecialchars_decode($tag->params));
            $styles = "";
            foreach ($stylesAttr as $key => $value) {
                $styles .= $this->styles[$key] . ":" . $value . "; ";
            }
            $styles .= "max-width:". $stylesAttr->width . ";";
            if ($gallery === null) continue;
            $galleries->push($gallery);
            $params = isJSON($gallery->params) ? json_decode($gallery->params, true) : $gallery->params;
            $view = View::make('partials.gallery', ["gallery" => $gallery, "params" => $params]);
            if ($template = (string)$view->render()) {
                $tag->outertext = "<div id='gallery-$gallery->id' class='content-gallery' style='$styles'>$template</div>";
            }
        }
        $html = $dom->save();

        return ["html" => $html, "galleries" => $galleries];
    }


    protected function makeGalleryOld($html)
    {
        if (!$html && $html == '') return $html;
        $libxmlPreviousState = libxml_use_internal_errors(true);
        $dom = new DOMDocument;
        $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();
        libxml_use_internal_errors($libxmlPreviousState);
        $elements = $dom->getElementsByTagName('gallery');
        $gallery = [];
        for ($i = $elements->length - 1; $i >= 0; $i--) {
            $element = $elements->item($i);
            $id_element = $element->getAttribute("id");
            $dbgallery = Gallery::find($id_element);
            if (!$dbgallery) return $html;
            $gallery[] = $dbgallery;
            $params = json_decode($dbgallery->params, true);
            $view = View::make('partials.unite-gallery', ["gallery" => $dbgallery, "params" => $params]);
            $template = $view->render();
            $template = (string)$view;
            if ($template) {
                $newdoc = new DOMDocument;
                $newdoc->loadHTML(mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8'));
                $element->parentNode->replaceChild($dom->importNode($newdoc->documentElement, TRUE), $element);
            }
        }
        return ["content" => $dom->saveHTML(), 'gallery' => empty($gallery) ? [] : $gallery];
    }
}