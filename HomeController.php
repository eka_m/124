<?php

namespace App\Http\Controllers\Site;
use App\Models\Ecms\Reviews;
use App\Models\Site\Slider;

class HomeController extends BaseController
{

    public function show()
    {

        // $partials = Partial::where(['name' => 'header'])
        //     ->orWhere(["name" => 'features'])
        //     ->get();
        // $features = $partials->where("name", "features")->first();

        // $doctors = Portfolio::where(['slug' => 'doctors'])->firstOrFail();
				// $doctors->{'items'} = PortfolioItem::where(['portfolio_id' => $doctors->id])->get();

        // $slider = Portfolio::where(['slug' => 'main-slider'])->firstOrFail();
        // $slider->{'items'} = PortfolioItem::where(['portfolio_id' => $slider->id])->get();

        // return view('site::pages.home.index', compact('pages','doctors', 'slider', 'features'));
        $reviews  = Reviews::orderBy('created_at','DESC')->get();
        $slides  = Slider::orderBy('created_at','DESC')->get();
        return view('site::pages.home.index', compact('reviews', 'slides'));
    }
}
