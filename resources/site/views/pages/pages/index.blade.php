@extends('layouts.base')
@section('title', " | ".$page->name)
@section('content')
  @yield('page-content')
@endsection
