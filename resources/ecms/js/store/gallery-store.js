export default {
    type: 'slider',
    types: {
        slider: 'Slider',
        comparison: 'Before & After'
    },
}
