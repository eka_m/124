import { options } from './parts/_options';
import { $ } from "./parts/_utility";

if ( typeof window.Skylith !== 'undefined' ) {
	window.Skylith.setOptions( options );
	window.Skylith.init();
}

$( document ).ready( function () {
	setTimeout( () => {
		$( "#content" ).addClass( 'enter' );
		if ( $( "#portfolio-show" ).length > 0 ) {
			animateProject();
		}
	}, 1000 );

	$( document ).on( "click", '.custom-tab .nav-link', function ( e ) {
		e.preventDefault();
		const elem = $( this );
		const current = elem.attr( "href" );
		$( '.custom-tab-content' ).find( '.active' ).removeClass( 'active show' );
		$( '.custom-tab-content' ).find( current ).addClass( 'active' ).delay( 2000 ).addClass( 'show' );

	} );

	/* Portfolio */
	function animateProject () {
		const container = $( '#portfolio-show' );
		const pr_main = $( "#portfolio-main" );
		const pr_separator = $( "#portfolio-separator" );
		const pr_cover = $( "#project-cover" );
		const pr_name = $( "#project-name" );
		const pr_line = $( "#pr_line" );
		const pr_desc = $( "#project-description" );
		const pr_client = $( "#project-client" );
		const pr_date = $( "#project-date" );
		const pr_skills = $( "#project-skills" );

		const pr_name_hd = $( "#pr_name_heading" );
		const pr_client_hd = $( "#pr_client_heading" );
		const pr_date_hd = $( "#pr_date_heading" );
		const pr_skill_hd = $( "#pr_skills_heading" );


		TweenMax.set( pr_cover, {
			y: "-130px",
			opacity: 0,
		} );
		TweenMax.set( [ pr_separator, pr_name, pr_desc, pr_client, pr_date, pr_skills, ], {
			opacity: 0,
		} );
		TweenMax.set( [ pr_name, pr_desc, pr_client, pr_date, pr_skills ], {
			y: "30px",
		} );
		TweenMax.set( pr_line, {
			width: 0,
			opacity: 0
		} );

		const tl = new TimelineMax();

		container.imagesLoaded( function () {
			tl.to( pr_cover, 0.3, { y: 0, opacity: 1 }, "0.5" )
				.to( pr_name_hd, 0.3, { text: "The Project" }, "+=0.4" )
				.to( pr_name, 0.3, { opacity: 1, y: 0 }, "+=0.4" )
				.to( pr_line, 0.3, { width: "40%", opacity: 1, }, "+=0.1" )
				.to( pr_desc, 0.3, { opacity: 1 }, "+=0.1" )
				.to( pr_separator, 0.1, { opacity: 1 }, "+=0.1" )
				.to( pr_client_hd, 0.2, { text: "Client" }, "+=0.2" )
				.to( pr_client, 0.2, { opacity: 1, y: 0 }, "+=0.2" )
				.to( pr_date_hd, 0.2, { text: "Year" }, "+=0.2" )
				.to( pr_date, 0.2, { opacity: 1, y: 0 }, "+=0.2" )
				.to( pr_skill_hd, 0.2, { text: "Skills" }, "+=0.2" )
				.to( pr_skills, 0.2, { opacity: 1, y: 0 }, "+=0.2" )
				.add( () => pr_separator.addClass( 'show-line' ), "+=0.2" );
		} );
	}


	/*LOADMORE*/

	const loadmore_container = $( ".loadmore_container" );
	const btn = $( ".loadmore_button" );
	const spinner = '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>';
	const btn_text = btn.html();
	const link = btn.attr( "href" );
	const total = btn.attr( "total" );
	const perPage = btn.data( "per-page" );
	const lastPage = btn.attr( "last-page" );
	if ( lastPage > 1 ) {
		btn.removeClass( 'd-none' )
	}
	btn.click( function ( e ) {
		e.preventDefault();
		let currentPage = btn.attr( "current-page" );
		let nextPage = Math.ceil( Number( currentPage ) + 1 );
		btn.html( spinner );
		$.getJSON( link + "?page=" + nextPage, function ( data, status ) {
			let items = [];
			if ( data.items.length === 0 ) {
				btn.html( 'No more records' );
			}
			if ( status === "success" && data.items.length > 0 ) {
				render( data.items );
				btn.html( data.pagination.current_page === data.pagination.last_page ? 'No more records' : btn_text );
				btn.attr( 'current-page', data.pagination.current_page );
				btn.attr( 'last-page', data.pagination.last_page );
			}
		} );
	} );

	function render ( items ) {
		items.forEach( ( item, index ) => {
			const elem = $( item );
			const cls = 'newitem' + index;
			elem.addClass( cls );
			elem.find( "img" ).addClass( "d-none" );
			const imgContainer = elem.find( '.gallery-item' );
			imgContainer.prepend( '<div class="spinContainer" style="background:#000000; width: 100%; height: 210px; display: flex; align-items: center; justify-content: center;"><div class="spinner"></div></div>' );
			elem.addClass( 'async-added' ).appendTo( loadmore_container );
			$( item ).imagesLoaded( () => {
				loadmore_container.find( '.' + cls ).find( '.spinContainer' ).remove();
				loadmore_container.find( '.' + cls ).find( "img" ).removeClass( "d-none" );
			} );
		} )
	}
} );
