<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
				$this->middleware('auth:ecms');

				$collections = Collection::all();

				View::share('collections', $collections);
    }
}
