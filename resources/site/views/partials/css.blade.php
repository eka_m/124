<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i%7cWork+Sans:400,500,700%7cPT+Serif:400i,500i,700i"
      rel="stylesheet" type="text/css">
@section('css')
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i%7cWork+Sans:400,500,700%7cPT+Serif:400i,500i,700i" rel="stylesheet" type="text/css">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap/dist/css/bootstrap.min.css')}}">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">

  <!-- Stroke 7 -->
  <link rel="stylesheet" href="{{asset('plugins/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css')}}">

  <!-- Flickity -->
  {{--<link rel="stylesheet" href="{{asset('plugins/flickity/dist/flickity.min.css')}}">--}}

@show

<link rel="stylesheet" href="{{asset('site/css/app.css')}}">
