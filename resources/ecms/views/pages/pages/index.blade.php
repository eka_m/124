@extends('ecms::layouts.base')
@section('title', 'Pages')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <pages pages="{{json_encode($pages)}}"></pages>
        </div>
    </div>
</div>
@endsection
