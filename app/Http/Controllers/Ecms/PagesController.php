<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Page;
use Illuminate\Http\Request;

class PagesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pages = Page::orderBy('position', 'ASC')->get()->toArray();
        $pages = Page::mapTree($pages);
        if ($request->ajax()) {
            return response()->json($pages);
        }
        return view('ecms::pages.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function createPage(Request $request)
    {
        // try {

            $lastPosition = Page::where('parent_id', $request->parent)->max('position');
            $page = new Page;
            $name = [
                \Setting::get('languages.default') =>$request->text,
            ];
            $page->name = $name;
            $page->slug = $request->slug;
            $page->position = $lastPosition ? ++$lastPosition : 1;
            $page->parent_id = $request->parent;
            $page->status = $request->status;
            if ($page->save()) {
                return response()->json(['message' => 'Page was successfully added!', 'node' => $page], 200);
            }
            return response()->json(['message' => json_encode($page->getErrors())], 400);
        // } catch (\Exception $e) {
        //     return response()->json([], 500);
        // }
    }

    public function setStatus(Request $request, $id)
    {
        try {
            $page = Page::find($id);
            $page->status = $request->status;
            if ($page->save()) {
                return response()->json(['message' => "The status of the page has been successfully changed to: '$request->status'"], 200);
            }
            return response()->json(['message' => json_encode($page->getErrors())], 400);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function store(Request $request)
    {

    }

    public function sort(Request $request)
    {
        $pages = $request->all();
        foreach ($pages as $value) {
            if ($value['id'] == 0) {
                continue;
            }

            $page = Page::find($value['id']);
            $page->position = $value['data']['position'];
            $page->parent_id = $value['parent'];
            $page->save();
        }
        return response()->json(['message' => 'Sorting successfully completed.!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\App\Models\Ecms\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\App\Models\Ecms\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('ecms::pages.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\App\Models\Ecms\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        try {
            $page->update($request->all());
            if ($page->save()) {
                \Toastr::success("Page was successfully saved!");
                return redirect()->route("ecms.pages.index");
            }
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.pages.index")->with(['errors' => $page->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.pages.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\App\Models\Ecms\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $items = explode(",", $request->items);
        foreach ($items as $item) {
            if ($item == 0) {
                continue;
            }

            $page = Page::find($item);
            $childrens = $page->children;
            if (!$childrens->isEmpty()) {
                $page->children()->update(['parent_id' => $page->parent_id, "position" => 9999]);
            }
            $page->delete();
        }
        return response()->json(['message' => 'Pages successfully deleted.'], 200);
    }
}
