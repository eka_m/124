@extends('ecms::layouts.base')
@section('title', 'Portfolio '. json_decode($portfolio->name,true)[\Setting::get('languages.default')])
@section('js')
<script src="{{asset('ecms/plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{route('ecms.portfolio.items.store',$portfolio->id)}}" method="POST">
				@csrf @method('POST')
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							<a href="{{route('ecms.portfolio.index')}}">Portfolio {{json_decode($portfolio->name,true)[\Setting::get('languages.default')]}}</a> items
						</h3>
					</div>
					<div class="card-body">
						<portfolio-items name="items" available="{{$portfolio->items}}" categories="{{$portfolio->categories}}"></portfolio-items>
					</div>
					<div class="card-footer text-right">
						<button type="submit" class="btn btn-teal">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
