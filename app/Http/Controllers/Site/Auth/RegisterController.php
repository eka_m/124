<?php

namespace App\Http\Controllers\Site\Auth;


use Validator;
use App\Mail\VerifyMail;
use App\Models\Base\User;
use Illuminate\Http\Request;
use App\Models\Base\VerifyUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Helpers\SMSC;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $unique_message = 'The email has already been taken.';
        if(is_numeric($data['email'])) {
            $unique_message = 'The phone number has already been taken.';
        }
        $messages = [
            'email.required' => 'This field is required',
            'phone_or_email' => 'Wrong format phone or email',
            'email.unique' => $unique_message
        ];
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|phoneOrEmail|max:255|unique:users,email,phone',
            'password' => 'required|min:6|confirmed',
        ],$messages);
    }


    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()) {
            return redirect('user/register')->withErrors($validator)
            ->withInput();
        } else {
        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
        }
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $phone = is_numeric($data['email']) ? $data['email'] : null;
        $email = !is_numeric($data['email']) ? $data['email'] : null;
        $user = User::create([
            'name' => $data['name'],
            'email' => $email,
            'phone' => $phone,
            'password' => bcrypt($data['password']),
        ]);
        $token = $phone ? random_int(100000,999999) : str_random(40);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => $token,
            'type' => $phone ? 'sms' : 'email',
        ]);
        if($phone) {
            $sms = new SMSC();
            $sms->send_sms($phone, "Your activation code is - $token");
        } else if ($email) {
            Mail::to($user->email)->send(new VerifyMail($user));
        }
        return $user;
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser)){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your account verified. You can now login.";
            }else{
                $status = "Your account is already verified. You can now login.";
            }
        }else{
            return redirect('/user/login')->with('warning', "Sorry your account cannot be identified.");
        }

        return redirect('/user/login')->with('status', $status);
    }

    public function smsVerify(Request $request) {
        $token =  $request->only('token');
        return $this->verifyUser($token);
    }
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('BASE.pages.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function registered(Request $request, $user)
    {
        if($user->verifyUser->type == 'sms') {
            return view('BASE.pages.auth.sms-verify');
        }
        return redirect('/user/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }
    protected function guard()
    {
        return Auth::guard('user');
    }
}
