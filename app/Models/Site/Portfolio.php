<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Portfolio extends Model
{
    protected $table = 'portfolio';

    use HasTranslations;
    public $translatable = ['name'];

    public function getCategoriesAttribute($categories)
    {
        $result = collect([]);
        foreach (json_decode($categories) as $category) {
            $name = property_exists($category->name, \LaravelLocalization::getCurrentLocale()) ?
            $category->name->{\LaravelLocalization::getCurrentLocale()} :
            $category->name->{\Setting::get('languages.default')};
            $slug = str_slug($name);

            $result->push(["name" => $name, "slug" => $slug]);
        }

        return $result;
    }

    public function items()
    {
        return $this->hasMany(PortfolioItem::class);
    }
}
