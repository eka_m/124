<div class="header py-4">
        <div class="container">
          <div class="d-flex">
            <a class="header-brand" href="./index.html">
              {{-- <img src="./demo/brand/tabler.svg" class="header-brand-img" alt="tabler logo"> --}}
              ECMS
            </a>
            <div class="d-flex order-lg-2 ml-auto">
              {{-- <div class="nav-item d-none d-md-flex">
                <a href="https://github.com/tabler/tabler" class="btn btn-sm btn-outline-primary" target="_blank">Source code</a>
              </div> --}}
             @include('ecms::partials.notifications-button')
             @include('ecms::partials.user-button')
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
              <span class="header-toggler-icon"></span>
            </a>
          </div>
        </div>
      </div>
