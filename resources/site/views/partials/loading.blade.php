<div class="loading-container" id="loading-container"
     style="position: absolute; top: 0; left:0; height: 100vh; width: 100%;">
  <style type="text/css">
    .loading-overlay {
      display: none;
      height: 100vh;
      position: relative;
      background: #000000 !important;
      z-index: 9999 !important;
      transition: all 2s;
    }

    .loading-overlay.show {
      display: block;
    }

    .loading-overlay.out {
      transform: scale(100) rotate(25deg);
    }

    .loading-bg {
      position: absolute;
      top: 0;
      left: 0;
      height: 100vh;
      justify-content: center;
      min-width: 100%;
      /*background-size: cover;*/
      {{--background: url('{{asset("site/img/loading-bg.svg")}}') no-repeat 0 50%;--}}
      opacity: 0;
      color: #46000f;
      font-size: 18em;
      transition: all 0.5s;
      white-space: nowrap;
      line-height: 100%;
      align-items: center;
      /*animation: marquee 30s linear infinite;*/
    }
    .loading-bg div {
      width: 100vw;
      height: 33.33%;
      display: flex;
      align-items: center;
      padding-left: 20px;
    }
    .loading-bg div.it {
      color:#3B7133;
    }

    .loading-bg div.az {
      color: #727272;
    }
    @media (min-width: 300px) {
      .loading-bg {
        font-size: 4.5em;
      }
    }
    @media (min-width: 640px) {
      .loading-bg {
        font-size: 7.5em;
      }
    }
    @media (min-width: 768px) {
      .loading-bg {
        font-size: 6em;
      }
    }
    @media (min-width: 1024px) {
      .loading-bg {
        font-size: 5.5em;
      }
    }
    @media (min-width: 1366px) {
      .loading-bg {
        font-size: 6.5em;
      }
    }
    @media (min-width: 1440px) {
      .loading-bg {
        font-size: 8em;
      }
    }
    @media (min-width: 1920px) {
      .loading-bg {
        font-size: 10em;
      }
    }

    @media (min-width: 2560px) {
      .loading-bg {
        font-size: 12.1em;
      }
    }

    .loading-main {
      position: relative;
      z-index: 2;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      height: 100%;

    }

    @keyframes marquee {
      0%   { transform: translate(0, 0); }
      100% { transform: translate(-100%, 0); }
    }

    .loading-logo {
      width: 250px;
      margin: 0 auto;

    }

    .loading-logo img {
      width: 100%;
    }

    .loading-loader {
      width: 250px;
      margin: 0 auto;
    }

    .loading-loader .loader-bar {
      width: 0;
      height: 3px;
      background-color: #46000f;
      box-shadow: 0 0 20px darkred;
    }
  </style>
  <div id="loading-overlay" class="loading-overlay">
    <div id="loading-bg" class="loading-bg">
      <div class="it">ITALIAN <br/> CREATIVITY </div>
      <div class="az">AZERBAIJAN <br/>IDENTITY</div>
      <div class="gl">GLOBAL <br/> VISION</div>
    </div>
    <div class="loading-main">
      <div class="loading-logo">
        <img src="{{asset('site/img/logo-white.png')}}" alt="">
      </div>
      <div class="loading-loader">
        <div class="loader-bar" id="loadbar"></div>
      </div>
      <div class="loader-precent" id="percent">
        0
      </div>
    </div>
  </div>

  <script type="text/javascript">
      var loading = document.getElementById('loading-container');
      var overlay = document.getElementById('loading-overlay');
      var main = document.getElementById('app');
      var loadbar = document.getElementById('loadbar');
      var percent = document.getElementById('percent');
      var st124 = document.getElementById('loading-bg');



      window.addEventListener('load', function () {
          if (!sessionStorage.getItem('loaded')) {
              overlay.classList.add("show");
              sessionStorage.setItem("loaded", true);
              load(0);
          } else {
              main.classList.remove('hiddenv');
          }
      });

      function load(status) {
          if (status <= 100) {
              setTimeout(function () {
                  loadbar.style.width = status + "%";
                  percent.innerText = status + "%";
                  let opacity = status / 100;
                  st124.style.opacity = opacity < 0.9 ? opacity : 0.9;
                  load(status + 1);
              }, 100);

          }
          if (status === 100) {
              setTimeout(function () {
                  overlay.classList.add('out');
                  main.classList.remove('hiddenv');
                  setTimeout(function () {
                      loading.parentNode.removeChild(loading);
                  }, 1000);
              }, 1000);
          }
      }
  </script>

</div>