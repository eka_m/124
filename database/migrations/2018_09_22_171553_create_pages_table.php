<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('parent_id')->nullable();
            $table->text('name');
            $table->string('slug');
            $table->longText('content')->nullable();
            $table->text('image')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->text('params')->nullable();
            $table->enum('status',['on','off','draft'])->default('draft');
            $table->smallInteger('position')->default(9999);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
