/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import Helpers from "./helpers";
import store from "./store-manager";
import Vue from "vue";
import VueStash from "vue-stash";

axios.interceptors.response.use(
    response => {
        switch (response.status) {
            case 200:
                toastr.success(response.data.message);
                break;
        }
        return response;
    },
    error => {
        switch (error.response.status) {
            case 500:
                toastr.error("Server error");
                toastr.error(error.response.data.message);
                break;
            case 400:
                toastr.error(error.response.data.message);
                break;
        }
        return Promise.reject(error);
    }
);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(Helpers);
Vue.use(VueStash);
Vue.component("pages", r =>
    require.ensure(
        [],
        () => r(require("./components/Pages/pages.vue")),
        "ecms/js/components/pages"
    )
);
Vue.component("language-toggler", r =>
    require.ensure(
        [],
        () => r(require("./components/Language-toggler/language-toggler.vue")),
        "ecms/js/components/language-toggler"
    )
);
Vue.component("translatable-input", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/translatable-input.vue")),
        "ecms/js/components/forms"
    )
);
Vue.component("translatable-textarea", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/translatable-textarea.vue")),
        "ecms/js/components/forms"
    )
);
Vue.component("slug", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/slug.vue")),
        "ecms/js/components/forms"
    )
);
Vue.component("file-input", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/fileinput.vue")),
        "ecms/js/components/forms"
    )
);
Vue.component("img-input", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/imginput.vue")),
        "ecms/js/components/forms"
    )
);
// Vue.component("grape", r =>
//   require.ensure(
//     [],
//     () => r(require("./components/Grapejs/grape.vue")),
//     "ecms/js/components/grapejs"
//   )
// );
Vue.component("tinymce", r =>
    require.ensure(
        [],
        () => r(require("./components/Form/tinymce.vue")),
        "ecms/js/components/editor"
    )
);
Vue.component("img-crop", r =>
    require.ensure(
        [],
        () => r(require("./components/ImgCrop/img-crop.slim.vue")),
        "ecms/js/components/img-crop"
    )
);
Vue.component("portfolio-categories", r =>
    require.ensure(
        [],
        () => r(require("./components/Portfolio/categories.vue")),
        "ecms/js/components/portfolio"
    )
);

Vue.component("portfolio-items", r =>
    require.ensure(
        [],
        () => r(require("./components/Portfolio/items.vue")),
        "ecms/js/components/portfolio"
    )
);

Vue.component("keyvalue", r =>
    require.ensure(
        [],
        () => r(require("./components/KeyValue/keyvalue.vue")),
        "ecms/js/components/keyvalue"
    )
);
Vue.component("scheme-generator", r =>
    require.ensure(
        [],
        () => r(require("./components/SchemeGenerator/scheme-generator.vue")),
        "ecms/js/components/scheme-generator"
    )
);

Vue.component("locales", r =>
    require.ensure(
        [],
        () => r(require("./components/Locale/locales.vue")),
        "ecms/js/components/locale"
    )
);

Vue.component("fa-iconlist", r =>
    require.ensure(
        [],
        () => r(require("./components/IconLists/faicons.vue")),
        "ecms/js/components/iconslist"
    )
);
Vue.component("gallery", r =>
    require.ensure(
        [],
        () => r(require("./components/Gallery/gallery.vue")),
        "ecms/js/components/gallery"
    )
);
Vue.component("gallery-params", r =>
    require.ensure(
        [],
        () => r(require("./components/Gallery/params.vue")),
        "ecms/js/components/gallery"
    )
);

const app = new Vue({
    el: "#app",
    data: {store},
    mounted() {
        this.setLanguagesStore();
        this.setAxiosBaseUrl();

    },
    methods: {
        setLanguagesStore() {
            this.store.init("lang", this.$helpers.languages());
            this.store.set("lang", 'current', this.$helpers.languages()['default']);
        },
        setAxiosBaseUrl() {
            axios.defaults.baseURL = this.store.get('helpers', "basePath");
        }
    }
});

