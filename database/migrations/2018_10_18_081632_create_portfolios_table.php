<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('slug');
            $table->longtext('categories')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->text('params')->nullable();
            $table->enum('status',['on','off','draft'])->default('draft');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('portfolio-items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_id');
            $table->string('type')->nullable();
            $table->string('content');
            $table->text('name')->nullable();
            $table->string('slug')->nullable();
            $table->longtext('info')->nullable();
            $table->text('params')->nullable();
            $table->enum('status',['on','off','draft'])->default('draft');
            $table->smallInteger('position')->default(9999);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio');
        Schema::dropIfExists('portfolio-items');
    }
}
