<?php

namespace App\Models\Ecms;

use Illuminate\Database\Eloquent\Model;

class PortfolioItem extends Model
{
    protected $table = 'portfolio-items';
    protected $fillable = [
        'name',
        'slug',
        'info',
        'type',
        'content',
        'category',
        'keywords',
        'description',
        'params',
        'status',
    ];

    protected $casts = [
        'name' => 'array',
        'info' => 'array',
        'content' => 'array',
        'params' => 'array',
        'keywords' => 'array',
        'description' => 'array',
    ];

    protected $rules = [
        'name' => 'required',
    ];

    public function portfolio() {
        return $this->belongsTo('App\Models\Ecms\Portfolio');
    }
}
