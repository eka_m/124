@extends('layouts.base')
@section('title', " | ".$page->name)
@section('content')
	<div class="nk-main bg-black">
		<div class="container-fluid max-width">
			<div class="nk-gap-6"></div>
			<h1 class="text-center text-white r-head">124… <span class="text-main">PORTFOLIO</span></h1>
			<div class="nk-gap"></div>
		</div>
		<div class="container-fluid m-auto" style="max-width: 1600px;">
			<div class="nk-portfolio-list pt-20 row loadmore_container">
				@foreach($projects as $project)
					@include('partials.portfolio-item',['project' => $project])
				@endforeach
			</div>
			<div class="nk-gap-4"></div>
		</div>
		<div class="nk-pagination nk-pagination-center bg-dark text-white">
			<a href="{{route('page','portfolio')}}"
			   total="{{$projects->total()}}"
			   current-page="{{$projects->currentPage()}}"
			   per-page="{{$projects->perPage()}}"
			   last-page="{{$projects->lastPage()}}"
			   class="loadmore_button d-none">Load More Works</a>
		</div>
	</div>
	@include('partials.footer')
@endsection
