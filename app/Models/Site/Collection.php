<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
	protected $casts = [ "scheme" => "array" ];

}
