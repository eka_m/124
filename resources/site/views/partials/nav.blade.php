<!--
    START: Navbar

    Additional Classes:
        .nk-navbar-items-collapsed
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
<nav
	class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide nk-navbar-items-collapsed nk-navbar-transparent nk-navbar-transparent-always nk-navbar-dark py-0">
	<div class="container-fluid">
		<div class="nk-nav-table">

			<a href="{{route('home')}}" class="nk-nav-logo al">
				<img src="{{asset('site/img/logo-white.png')}}" alt="" width="60" class="logo nk-nav-logo-img-dark">
				<img src="{{asset('site/img/logo-white.png')}}" alt="" width="60" class="logo nk-nav-logo-img-light">
			</a>

			<ul class="nk-nav nk-nav-right d-none d-lg-block  nk-nav-collapsed" data-nav-mobile="#nk-nav-mobile">
				<li class="active">
					<a href="{{route('home')}}">
						Home
					</a>
				</li>
				@foreach($pages as $page)
					<li>
						<a href="{{route('page', $page->slug)}}">
							{{$page->name}}
						</a>
					</li>
				@endforeach
			</ul>


			<ul class="nk-nav nk-nav-right nk-nav-icons">
				<li class="single-icon d-none d-lg-inline-block">
					<a href="#" class="nk-navbar-collapsed-toggle">
            <span class="nk-icon-burger">
                <span class="nk-t-1"></span>
                <span class="nk-t-2"></span>
                <span class="nk-t-3"></span>
            </span>
					</a>
				</li>
				<li class="single-icon d-lg-none">
					<a href="#" class="nk-navbar-full-toggle">
              <span class="nk-icon-burger">
                  <span class="nk-t-1"></span>
                  <span class="nk-t-2"></span>
                  <span class="nk-t-3"></span>
              </span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<!-- END: Navbar -->
