<?php

namespace App\Models\Ecms;

use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Base
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'content',
        'image',
        'cover',
        'keywords',
        'description',
        'params',
        'status',
        'position',
    ];

    protected $casts = [
        'keywords' => 'array',
        'description' => 'array'
    ];

    protected $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];

    public $emptyTranslations = true;
    public $translatable = ['name', 'content', 'keywords', 'description'];

    public $checkState = ['name', 'content', 'keywords', 'description', 'image', 'cover'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = isJSON($value) ? $value : json_encode($value);
    }
    public function setContentAttribute($value)
    {
        $this->attributes['content'] = isJSON($value) ? $value : json_encode($value);
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Ecms\Page', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Ecms\Page', 'parent_id');
    }

    public static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node['parent_id'] != $parent) {
                continue;
            }

            $node['children'] = self::mapTree($dataset, $node['id']);
            $tree[] = $node;
        }
        return $tree;
    }
}
