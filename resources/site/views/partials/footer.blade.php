
<footer class="nk-footer" style="background-color: #030506;">

	<div class="nk-footer-cont nk-footer-cont-md">
		<div class="container text-center">
			<div class="row vertical-gap sm-gap align-items-center">

				<div class="col-lg-6 order-lg-3 text-lg-right">
					<div class="nk-social text-lg-right text-gray-1">
						<ul class="d-inline-flex">
							<li><a class="nk-social-facebook" href="https://www.facebook.com/124studiosBaku"><span><span class="nk-social-front fa fa-facebook"></span><span class="nk-social-back fa fa-facebook"></span></span></a></li>
							<li><a class="nk-social-instagram" href="https://www.instagram.com/124studios_az/?hl=en"><span><span class="nk-social-front fa fa-instagram"></span><span class="nk-social-back fa fa-instagram"></span></span></a></li>
							<li><a class="nk-social-youtube" href="https://www.youtube.com/channel/UCBGa3nZSTiL1k4C1W4gnLog?view_as=subscriber"><span><span class="nk-social-front fa fa-youtube-play"></span><span class="nk-social-back fa fa-youtube"></span></span></a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-6 text-lg-left">
					<div class="nk-footer-text text-gray">
						<p>2018 &copy; Developed by 124studios</p>
					</div>
				</div>

				{{--<div class="col-lg-2">--}}

					{{--<a class="nk-footer-scroll-top-btn-2 nk-anchor nk-fullpage-scroll-up text-gray-1" href="#top">--}}
						{{--Go To Top--}}
					{{--</a>--}}

				{{--</div>--}}
			</div>
		</div>
	</div>
</footer>
<!-- END: Footer -->
