<?php
namespace App\Traits;

trait Translatable
{

    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);
        return !$this->isTranslatableAttribute($key) ? $value : $this->getTranslations($value);
    }

    public function getTranslations($value)
    {
        return !$this->emptyTranslations() ? $this->setTranslations($value) : $this->setEmptyTranslations($value);
    }

    public function setTranslations($value)
    {
        return $value;
    }

    public function setEmptyTranslations($value)
    {
        $result = is_array($value) ? (array) $value : json_decode($value, true);
        foreach ($this->getLanguages() as $slug => $lang) {
            if (isset($result[$slug])) {
                continue;
            }

            $result[$slug] = null;
        }
        return $result;
    }

    public function getLanguages()
    {
        return \Setting::get('languages.all') ?: [config('app.locale') => ["slug" => config('app.locale')]];
    }

    public function getDefaultLang()
    {
        return \Setting::get('languages.default') ?: config('app.locale');
    }

    public function getTranslatableAttributes()
    {
        return is_array($this->translatable)
        ? $this->translatable
        : [];
    }

    public function isTranslatableAttribute(string $key)
    {
        return in_array($key, $this->getTranslatableAttributes());
    }

    public function emptyTranslations()
    {
        return property_exists($this, 'emptyTranslations') ? $this->emptyTranslations : false;
    }

    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $name) {
            if (!isset($attributes[$name])) {
                continue;
            }

            $attributes[$name] = $this->getTranslations($attributes[$name], $name);
        }
        return $attributes;
    }
}
