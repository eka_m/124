@extends('ecms::layouts.base')
@section('title','Creating a gallery')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <form action="{{route('ecms.gallery.store')}}" method="post">
          <div class="card">
            @csrf
            <div class="card-status bg-teal"></div>
            <div class="card-header">
              <h3 class="card-title">New Gallery </h3>
              <div class="card-tabs pl-5 ml-5">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#main" role="tab"
                       aria-controls="home" aria-selected="true">Settings</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#content" role="tab"
                       aria-controls="content" aria-selected="false">Gallery</a>
                  </li>
                </ul>
              </div>
              <div class="card-options">
                <language-toggler></language-toggler>
                <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
              </div>
            </div>
            <div class="card-body">
              <div class="dimmer loading-content active">
                <div class="loader"></div>
                <div class="dimmer-content">
                  @include('ecms::pages.gallery.__form')
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="form-group text-right">
                <button class="btn btn-pill btn-teal">Save</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
