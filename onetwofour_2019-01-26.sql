# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25-0ubuntu0.16.04.2)
# Database: onetwofour
# Generation Time: 2019-01-26 10:49:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_password_resets`;

CREATE TABLE `admin_password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin','memmedkerimov@gmail.com',NULL,'$2y$10$EfTFjiWkvfhHwXEUe4QMD.eXtVAmk0oPtzymTSeRjRR3JsdJR149m',NULL,NULL,NULL);

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collections`;

CREATE TABLE `collections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheme` longtext COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;

INSERT INTO `collections` (`id`, `name`, `dname`, `icon`, `slug`, `scheme`, `active`, `created_at`, `updated_at`)
VALUES
	(2,'works','Our Works','far fa-handshake','works','[{\"id\":\"rP3GDo4Z7d380\",\"name\":\"title\",\"display\":true,\"required\":true,\"multilang\":false,\"slug\":true,\"type\":\"text\"},{\"id\":\"x4cmygRH0M231\",\"name\":\"description\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"editor\"},{\"id\":\"PDXKWOMywE772\",\"name\":\"image\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"image\"},{\"id\":\"446QdC45Dx63\",\"name\":\"cover\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"image\"},{\"id\":\"jyUCzOlcoE454\",\"name\":\"client\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"text\"},{\"id\":\"iKUsP8nGl9605\",\"name\":\"date\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"text\"},{\"id\":\"AvKAkoXbk1726\",\"name\":\"skills\",\"display\":false,\"required\":false,\"multilang\":false,\"slug\":false,\"type\":\"editor\"}]',0,'2019-01-12 09:26:24','2019-01-13 16:20:25');

/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table galleries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `items` longtext COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2017_07_31_123420_create_locales_table',1),
	(2,'2018_11_14_120743_create_collections_table',2),
	(3,'2018_11_22_181116_create_galleries_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `cover` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `position` smallint(6) NOT NULL DEFAULT '9999',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `name`, `slug`, `content`, `image`, `cover`, `keywords`, `description`, `params`, `status`, `position`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(6,0,'{\"en\":\"Financing\"}','financing','{\"en\":\"<div style=\\\"padding-left: 60px;\\\" data-mce-style=\\\"padding-left: 60px;\\\"><br></div><div style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\"><br><br>\\\"I rarely say this, but this is the real deal.&nbsp; It melts fat.&nbsp; Three to four treatments will produce 2 to 3 inches of loss in your waist line.\\\" -&nbsp;<strong>Dr. Jennifer Walden, MD,&nbsp;FACS Plastic Surgeon<br><br><br></strong></span></div><br><br><hr><h3 style=\\\"padding-left: 30px; text-align: center;\\\" data-mce-style=\\\"padding-left: 30px; text-align: center;\\\"><br><br><br><img class=\\\"\\\" style=\\\"float: left;\\\" src=\\\"/uploads/images/fn.png\\\" alt=\\\"\\\" width=\\\"323\\\" height=\\\"661\\\" data-mce-src=\\\"/uploads/images/fn.png\\\" data-mce-style=\\\"float: left;\\\"><strong><br><br><br><br><br><br><br><br>&nbsp;&nbsp;<span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\">Why Wait Any Longer?<br></span></strong><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong>You Deserve To Be In <br>Amazing <br>Shape Today!</strong></span><br><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong>Just Imagine Having That Perfect Figure <br>You\'ve Always Dreamed Of.</strong></span></h3><p><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong><br><br><br><br><br><br><br><br><br><br><br><br><br><br></strong><br></span></p><h4 class=\\\"paragraph\\\"><strong><span style=\\\"color: #2a2a2a; font-size: x-large;\\\" data-mce-style=\\\"color: #2a2a2a; font-size: x-large;\\\"><br></span></strong><br><strong><br></strong></h4><p style=\\\"padding-left: 180px;\\\" data-mce-style=\\\"padding-left: 180px;\\\"><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\"><br><br><br><br></span></strong></p><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><h4>LOW PAYMENTS FROM LESS THAN&nbsp;$97&nbsp;PER MONTH<br><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\">(OAC)<br><img class=\\\"img-fluid\\\" src=\\\"/uploads/images/lendingtree-1_orig.jpg\\\" alt=\\\"\\\" width=\\\"375\\\" height=\\\"154\\\" data-mce-src=\\\"/uploads/images/lendingtree-1_orig.jpg\\\"><br><br><img class=\\\"img-fluid\\\" src=\\\"/uploads/images/lending-usa-logo_2.jpg\\\" alt=\\\"\\\" width=\\\"384\\\" height=\\\"147\\\" data-mce-src=\\\"/uploads/images/lending-usa-logo_2.jpg\\\"><br></span></strong></h4></div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><img class=\\\"img-fluid\\\" style=\\\"float: right;\\\" src=\\\"/uploads/images/ripped-six-pack.png\\\" alt=\\\"\\\" width=\\\"357\\\" height=\\\"495\\\" data-mce-src=\\\"/uploads/images/ripped-six-pack.png\\\" data-mce-style=\\\"float: right;\\\"></div></div></section><p style=\\\"padding-left: 180px;\\\" data-mce-style=\\\"padding-left: 180px;\\\"><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\"><br><br><br></span></strong></p><h4 style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><br>• NO CREDIT CHECK FINANCING AVAILABLE FOR SPECIAL NEEDS&nbsp;•</h4>\"}',NULL,'{\"path\":\"/uploads/images/financing.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1920,\"height\":673,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',2,'2018-11-25 19:33:57','2018-11-14 08:49:28','2018-11-25 19:33:57'),
	(7,0,'{\"en\":\"Contacts\"}','contacts','{\"en\":\"<h2><br><br>LET\'S GET STARTED ON HELPING YOU LOOK AND FEEL GREAT TODAY!</h2><h3><strong><br>361-334-9999</strong></h3><h3 data-mce-style=\\\"text-align: center;\\\" style=\\\"text-align: center;\\\"><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">We Love Making Your Dreams Come True!</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Start Today With A&nbsp;FREE</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Beautiful Body Makeover Consultation</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">With Our Experts</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">\\\"Your Beauty is Our Business\\\"</span><strong><br><br></strong></h3><div style=\\\"text-align: left;\\\" data-mce-style=\\\"text-align: left;\\\"><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\" style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><img src=\\\"/uploads/images/_1416265748.png\\\" width=\\\"259\\\" height=\\\"102\\\" class=\\\"img-fluid\\\" alt=\\\"\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" data-mce-style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" data-mce-src=\\\"/uploads/images/_1416265748.png\\\">Livingsocial Member&nbsp;<br><br>&nbsp;</div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p><span style=\\\"font-size: 12pt;\\\" data-mce-style=\\\"font-size: 12pt;\\\">\\\"Incredible! I would give this place 6 stars if I could! It is painless, relaxing, clean, professional - everyone there is AMAZING - they make you feel so comfortable. And it works too! I lost one and half inches off my stomach area which has been such a problem area for me! I will be back!\\\"&nbsp;-&nbsp; Joyce W</span></p></div></div></section></div><div style=\\\"text-align: left;\\\" data-mce-style=\\\"text-align: left;\\\"><br></div><p><br></p>\"}',NULL,'{\"path\":\"/uploads/images/contacts.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1919,\"height\":612,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',4,'2018-11-25 19:33:57','2018-11-14 08:49:38','2018-11-25 19:33:57'),
	(9,0,'{\"en\":\"Who we are\"}','who-we-are','{\"en\":\"TEST\"}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',0,NULL,'2018-11-25 19:34:18','2018-12-20 19:12:32'),
	(10,0,'{\"en\":\"What we do\"}','what-we-do','{\"en\":null}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',1,NULL,'2018-11-26 14:53:08','2018-12-20 19:12:18'),
	(11,0,'{\"en\":\"Portfolio\"}','portfolio','{\"en\":null}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',2,NULL,'2018-11-26 14:53:45','2019-01-11 15:16:57'),
	(12,0,'{\"en\":\"Gallery\"}','gallery','{\"en\":null}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',3,NULL,'2018-11-26 14:53:51','2019-01-11 15:13:13'),
	(13,0,'{\"en\":\"Contacts\"}','contacts',NULL,NULL,NULL,NULL,NULL,NULL,'on',4,NULL,'2018-11-26 14:54:02','2018-11-26 14:54:16');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table partials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partials`;

CREATE TABLE `partials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `keyvalue` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table portfolio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolio`;

CREATE TABLE `portfolio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` longtext COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;

INSERT INTO `portfolio` (`id`, `name`, `slug`, `categories`, `keywords`, `description`, `params`, `status`, `deleted_at`, `created_at`, `updated_at`, `content`)
VALUES
	(1,'{\"en\":\"Gallery\"}','gallery','[{\"name\":{\"en\":\"DESIGN\"},\"slug\":null},{\"name\":{\"en\":\"PHOTOGRAPHY\"},\"slug\":null},{\"name\":{\"en\":\"VIDEO\"},\"slug\":null},{\"name\":{\"en\":\"SOUNDTRACKS\"},\"slug\":null}]',NULL,NULL,NULL,'draft',NULL,'2018-12-20 18:13:01','2019-01-12 09:08:59','\"{}\"');

/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table portfolio-items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolio-items`;

CREATE TABLE `portfolio-items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` longtext COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `position` smallint(6) NOT NULL DEFAULT '9999',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `portfolio-items` WRITE;
/*!40000 ALTER TABLE `portfolio-items` DISABLE KEYS */;

INSERT INTO `portfolio-items` (`id`, `portfolio_id`, `type`, `content`, `name`, `category`, `slug`, `info`, `params`, `status`, `position`, `deleted_at`, `created_at`, `updated_at`, `keywords`, `description`)
VALUES
	(1667,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/mezzo_restaurant_3.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#098776\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Hyatt Regency - Mezzo Restaurant 3\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',0,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1668,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/mezzo_restaurant_4.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#0D583F\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Hyatt Regency - Mezzo Restaurant 4\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',1,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1669,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/mezzo_restaurant_2.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#B89513\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Hyatt Regency - Mezzo Restaurant 2\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',2,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1670,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/mezzo_restaurant_1.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#B7092B\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Hyatt Regency - Mezzo Restaurant\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',3,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1671,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/shin_shin_restaurant_1.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#FA361E\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Shin Shin Restaurant \"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',4,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1672,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/shin_shin_restaurant_2.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#C326ED\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Shin Shin Restaurant 2\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',5,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1673,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/seto_restaurant.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#531A94\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Seto Restaurant\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',6,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1674,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/seto_restaurant_2.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#5DD8C6\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Seto Restaurant 2\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',7,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1675,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/sky_bar__amp__lounge.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#ABE2A7\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Sky Bar & Lounge\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',8,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1676,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/executive_lounge.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":835,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#634C3A\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Executive Lounge\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',9,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1677,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/landmark_hotel.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#BE5826\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark Hotel\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',10,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1678,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/caspian_grill__amp__terrace.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1251,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#909E3F\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Landmark - Snack & Salad Bar\"}','PHOTOGRAPHY',NULL,'[]',NULL,'draft',11,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1679,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/zoroaster.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":600,\"height\":400,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"JXfwZO0mR-8\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#D0C24B\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Italian-Azerbaijani International Film Festival\"}','VIDEO',NULL,'[]',NULL,'draft',12,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1680,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/fernando__amp__luis_costa_concert-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"yx3YXEPCNNw\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#C4C50B\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Fernando & Luis Costa \"}','VIDEO',NULL,'[]',NULL,'draft',13,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1681,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/scalini_restaurant-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"y_0AETZX7wM\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#2E8CC4\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Scalini Restaurant\"}','VIDEO',NULL,'[]',NULL,'draft',14,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1682,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/miroca_paris_in_concert_2-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"jAngkmZsS5Y\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#65655B\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Miroca Paris in Concert\"}','VIDEO',NULL,'[]',NULL,'draft',15,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1683,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/hyatt_regency_christmas-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"DT11YNS_bSM\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#58CE07\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Hyatt Regency Hotel Christmas Preparation\"}','VIDEO',NULL,'[]',NULL,'draft',16,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1684,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/lilach_sheeff-02.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"PJyt9XHgtRQ\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#B71442\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Lilach Sheeff - Self-defense Demonstration 2\"}','VIDEO',NULL,'[]',NULL,'draft',17,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1685,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/lilach_sheeff-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":\"GxexHZVMyno\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#8B2115\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Lilach Sheeff - Self-defense Demonstration\"}','VIDEO',NULL,'[]',NULL,'draft',18,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1686,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/the_week_of_italian_language-01.png\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"4Ix6oRo45-8\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#83F18A\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"The Week of Italian Language\"}','VIDEO',NULL,'[]',NULL,'draft',19,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1687,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/carlos_mota_-_founds_and_finding-01-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"Gx1T18TTHhI\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#E03206\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Carlos Mota - Found and Findings\"}','VIDEO',NULL,'[]',NULL,'draft',20,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1688,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/miroca_paris_in_concert-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":\"IAQavk_W-SU\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#22E559\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Miroca Paris in Concert\"}','VIDEO',NULL,'[]',NULL,'draft',21,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1689,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/cultural_heritage_year.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":\"yOmWu3bSERU\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#324B41\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"European Year of Cultural Heritage\"}','VIDEO',NULL,'[]',NULL,'draft',22,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1690,1,'video','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/celentano-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":\"neJWgYkfheA\",\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#8E5C00\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Adriano Celentano Film Festival\"}','VIDEO',NULL,'[]',NULL,'draft',23,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1691,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/businesscard.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#9A31B6\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"United Cultures Visit Card\"}','DESIGN',NULL,'[]',NULL,'draft',24,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1692,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/portfolio.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#E621C4\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"United Cultures Portfolio\"}','DESIGN',NULL,'[]',NULL,'draft',25,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1693,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/flyer.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#D4DCB6\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"United Cultures Flyer\"}','DESIGN',NULL,'[]',NULL,'draft',26,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1694,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/folder.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#7AC68A\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"United Cultures Folder\"}','DESIGN',NULL,'[]',NULL,'draft',27,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1695,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/fantazia_poster-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#0883BB\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Fantazia Festival\"}','DESIGN',NULL,'[]',NULL,'draft',28,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1696,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/flyer_fantazia_2.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#653671\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Flyer for Fantazia Festival 2\"}','DESIGN',NULL,'[]',NULL,'draft',29,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1697,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/fantazia_flyer_guided_tours-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#233CBC\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Flyer for Fantazia Festival Guided Tours\"}','DESIGN',NULL,'[]',NULL,'draft',30,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1698,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/flyer_fantazia_3.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1251,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#893D06\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Flyer for Fantazia Festival  3\"}','DESIGN',NULL,'[]',NULL,'draft',31,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1699,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/poster_week_of_italian_language.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1251,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#378F80\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Week of Italian Language Poster\"}','DESIGN',NULL,'[]',NULL,'draft',32,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1700,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/flyer_week_of_talian_language.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#2E4470\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Week of Italian Language Flyer\"}','DESIGN',NULL,'[]',NULL,'draft',33,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1701,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/ca_foscari_flyer.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#8DA0DA\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Ca\' Foscari Flyer\"}','DESIGN',NULL,'[]',NULL,'draft',34,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1702,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/poster_lilach_sheeff.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1251,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#054163\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Lilach Sheeff self-defense demonstration\"}','DESIGN',NULL,'[]',NULL,'draft',35,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1703,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/celentano_poster-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#F9182D\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Adriano Celentano Film Festival \"}','DESIGN',NULL,'[]',NULL,'draft',36,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1704,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/poster_japanese_concert-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1252,\"height\":835,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#E95087\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Shukuhachi Concert \"}','DESIGN',NULL,'[]',NULL,'draft',37,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1705,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/poster_for_senses_of_itay-01.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1250,\"height\":834,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#08DBB3\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Senses of Italy\"}','DESIGN',NULL,'[]',NULL,'draft',38,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]'),
	(1706,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/gallery\\/poster_miroca_paris.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":834,\"width\":1250,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":null,\"value\":[],\"parent\":null,\"color\":\"#735687\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"Poster for Miroca Paris concert\"}','DESIGN',NULL,'[]',NULL,'draft',39,NULL,'2019-01-25 16:52:07','2019-01-25 16:52:07','[]','[]');

/*!40000 ALTER TABLE `portfolio-items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'user',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table verify_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `verify_users`;

CREATE TABLE `verify_users` (
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table works
# ------------------------------------------------------------

DROP TABLE IF EXISTS `works`;

CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `date` text COLLATE utf8mb4_unicode_ci,
  `skills` longtext COLLATE utf8mb4_unicode_ci,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;

INSERT INTO `works` (`id`, `collection_id`, `date`, `skills`, `title`, `description`, `image`, `cover`, `client`, `slug`, `position`, `active`, `created_at`, `updated_at`)
VALUES
	(5,2,'September 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p><p>Media and Social Media promotion</p><p>Subtitling of movies from Italian to Russian</p><p>Organization of press office</p>\"}','ADRIANO CELENTANO FILM FESTIVAL','{\"en\":\"<p>Organization, promotion and communication of the Adriano Celentano Film Festival, creation of the graphic concept; realization of promo material; creation of a video promo of the Festival; subtitling of 5 movies from Italian to Russian language; organization of the press conference. The film Festival is a tribute to the famous Italian showmanon the occasion of his 80th birthday anniversary, financed by the Italian Embassy: screening of his most famous and beloved movies.</p>\"}','/images/portfolio/profie_picture/adriano-celentano-slide-home.jpg','/images/portfolio/cover/a_celentano-cover.jpg','Embassy of Italy','adriano-celentano-film-festival',19,0,'2019-01-14 13:05:53','2019-01-22 11:40:32'),
	(8,2,'November 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p><p>Media and Social Media promotion</p><p>Organization of press conference and workshop</p>\"}','MIROCA PARIS IN CONCERT','{\"en\":\"Creation of the concept, realization of graphic material, shooting and editing of photos and videos and promotion through Media and social media of Miroca Paris’s concert. The famous Cape Verdean multi-instrumentalist, Miroca Paris performed in the framework of Fantazia Cultural Heritage Festival in Baku. Beside being the opening event of Fantazia Festival, it was also one of the main stages of his “D’Alma Tour” around the world\"}','/images/portfolio/profie_picture/4.jpg','/images/portfolio/cover/miroca.jpg','United Cultures','miroca-paris-in-concert',16,0,'2019-01-15 11:39:50','2019-01-22 11:40:37'),
	(9,2,'November 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p><p>Media and Social Media promotion</p><p>Creation and management of a dedicated website</p><p>Organization of press conferences and press office</p>\"}','EUROPEAN UNION FANTAZIA CULTURAL HERITAGE FESTIVAL','{\"en\":\"<p>Communication and promotion of the cultural events organized in the framework of Fantazia Cultural Heritage Festival in Baku, financed by the European Union Delegation to Azerbaijan, in cooperation with European Member States Embassies, Embassies of Mexico, Morocco and Moldova; creation of the graphic concept and marketing materials; creation of 2 video promo of the Festival; creation and management of the website of the Festival; organization of two press conferences and promotion of the Festival through Media and Social Network.</p>\"}','/images/portfolio/profie_picture/fantazia-portfilo-box-2.jpg','/images/portfolio/cover/fantazia-cover.jpg','United Cultures','european-union-fantazia-cultural-heritage-festival',17,0,'2019-01-15 11:45:27','2019-01-22 11:40:35'),
	(10,2,'December 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p><p>Media and Social Media promotion</p><p>Organization of press office</p>\"}','FERNANDO & LUIS COSTA IN CONCERT','{\"en\":\"<p>Creation of graphic concept and marketing material; realization of a promo video of Fernando and Luis Costa; organization of their concert at Landmark Jazz Club; communication and promotion of the concert through Media and Social Networks. Photo/Video coverage of the event. &nbsp;Fernando and Luis Costa, twins, originally from Portugal, have been impressing the critics for their&nbsp;<strong>powerful and intimate-connected music</strong>. They have developed a great synchronization marked by a&nbsp;<strong>strong stage presence </strong>and the simplicity of the&nbsp;<strong>natural fluency of their music</strong>. This time they enriched their performance fusing it with the Azerbaijani National Music – Mugham.</p>\"}','/images/portfolio/profie_picture/7.jpg','/images/portfolio/cover/costa_brothers_cover.jpg','Embassy of Portugal','fernando-and-luis-costa-in-concert',1,0,'2019-01-15 12:06:21','2019-01-22 13:25:52'),
	(11,2,'November 2018','{\"en\":\"<p>Photo shooting and editing</p><p>Video shooting and editing</p>\"}','SCALINI RESTAURANT','{\"en\":\"<p>Shooting and editing of a promo video realized on the occasion of the 20th Anniversary of the Italian restaurant. Among the other things, “Scalini”, has been recently awarded as one of the 70 best restaurants with pizzeria in the world.</p>\"}','/images/portfolio/profie_picture/scalini.jpg','/images/portfolio/cover/scalini-cover.jpg','Scalini Restaurant','scalini-restaurant',8,0,'2019-01-15 12:08:40','2019-01-22 11:40:58'),
	(12,2,'November 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Design of the graphic concept</p><p>Media and Social Networks promotion</p>\"}','SENSES OF ITALY','{\"en\":\"<p>Creation of the graphic concept and marketing materials of the event organized by the Italian Trade Agency in collaboration with Bank of Georgia for promotion of Made in Italy luxury products.</p>\"}','/images/portfolio/profie_picture/senses-of-italy-portfilo-box.jpg','/images/portfolio/cover/senses_of_italy_cover.jpg','Italian Trade Agency (ICE)','senses-of-italy',5,0,'2019-01-15 12:15:02','2019-01-22 11:41:04'),
	(13,2,'November 2018','{\"en\":\"<p>Creation of a promo video</p><p>PR and communication</p><p>Promotion through Media and Social Media</p><p>Organization of press office</p>\"}','CARLOS MOTA CONTEMPORARY ART EXHIBITION','{\"en\":\"<p>PR and communication of the contemporary art exhibition of Carlos Mota; creation of a promo video; promotion of the art exhibition through Social Network. The exhibition \\\"Found and Findings\\\" by Carlos Mota , promoted by the Embassy of Portugal, took place at the Art Gallery of the Museum Center in Baku.</p>\"}','/images/portfolio/profie_picture/carlos-motta.jpg','/images/portfolio/cover/carlosmotto-cover.jpg','Embassy of Portugal','carlos-mota-contemporary-art-exhibition',10,0,'2019-01-15 12:16:49','2019-01-22 11:40:52'),
	(14,2,'October 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p><p>Media and Social Networks Promotion</p>Organization of press office\"}','THE WEEK OF ITALIAN LANGUAGE IN THE WORLD','{\"en\":\"<p>Communication and promotion of the events proposed in the framework of the Week of the Italian Language in the World through Media and Social Network; creation of the graphic concept; realization of promo material; organization of press conferences. The Week of the Italian Language is an international initiative aimed at promoting the Italian language abroad as the representative language of the classical and contemporary culture.&nbsp; Every year, on the third week of October, the cultural and diplomatic network of the Ministry of Foreign Affairs and International Cooperation of the Italian Republic organize the event on a different topic, on which conferences, exhibitions, shows and meetings are based.</p>\"}','/images/portfolio/profie_picture/colosseum.jpg','/images/portfolio/cover/week_of_itlang_cover.jpg','United Cultures','the-week-of-italian-language-in-the-world',15,0,'2019-01-15 12:23:55','2019-01-22 11:40:41'),
	(15,2,'November 2018/present day','{\"en\":\"<p>Photo shooting and editing</p><p>Video shooting and editing</p>\"}','HYATT REGENCY HOTEL','{\"en\":\"<p>Photo/Video shooting and editing for Hotel, outlets and facilities; pro-editing of photos; recording, montage and monthly production of two videos; creation of contents for Social Network. We recently started our collaboration with Hyatt Regency Hotel, the first 5 stars luxury hotel in Baku and still one of the best in the city so far.</p>\"}','/images/portfolio/profie_picture/hyatt-regency-2.jpg','/images/portfolio/cover/hyatt-regency-cover1.jpg','Hyatt Regency Hotel','hyatt-regency-hotel',3,0,'2019-01-15 12:44:23','2019-01-22 11:41:08'),
	(16,2,'December 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Video shooting and editing</p><p>Social Media promotion</p>\"}','LILACH SHEEFF – ISRAELI TRAINER','{\"en\":\"<p>Creation of the graphic concept; shooting and editing of 3 video of self-defense demonstration by Lilach Sheef, Israeli “Krav Maga” instructor; promotion through Social Media of Lilach Sheef workshops organized in the framework of the “16 days of activism against gender based violence”.&nbsp; The whole project was sponsored by the Embassy of Israel in Baku together with Azerbaijan State Committee for Family, Women and Children and the Ministry of Youth and Sport.</p>\"}','/images/portfolio/profie_picture/8.jpg','/images/portfolio/cover/israeli_trainer.jpg','Embassy of Israel','lilach-sheeff-israeli-trainer',4,0,'2019-01-15 12:55:58','2019-01-22 11:41:06'),
	(17,2,'August 2018/present day','{\"en\":\"<p>Management of marketing activities</p><p>Photo/video shooting and editing</p><p>Creation and maintenance of new website</p><p>Creation and issuing of newsletter</p><p>Design of a new outlet logo</p><p>Creation of graphic and promo material</p>\"}','THE LANDMARK BUSINESS CENTER','{\"en\":\"<p>Management of the marketing activities of Landmark Business Center, including 6 restaurants, 1 lounge bar, 1 café&nbsp; and 5 stars hotel; realization, maintenance and daily updating of the official website thelandmarkbaku.az; creation and monthly issuing of The Landmark News (a house organ presenting all events and happening in Landmark – almost 20 pages); creation of restaurants’ menu; photo coverage of the buildings (including conference center, restaurants, concert halls, hotel rooms, open spaces, etc.); photo-video reportage of all events in Landmark, including video montage; realization of the new brochure and business card; restyling and updating of social media profiles; design of logo for the new outlet Caspian Room.</p>\"}','/images/portfolio/profie_picture/thelandmark-1.jpg','/images/portfolio/cover/landmark-cover.jpg','The Landmark Business Center','the-landmark-business-center',2,0,'2019-01-15 13:00:33','2019-01-22 11:41:09'),
	(18,2,'August 2018/present day','{\"en\":\"<p>Photo shooting and editing</p><p>Design and Layout of the menu</p><p>Social Media promotion</p>\"}','SETO RESTAURANT','{\"en\":\"<p>Creation of&nbsp; the graphic concept, design and layout for the restaurant&nbsp; menu, photo coverage of the food and the restaurant itself. Situated on the ground floor of the Landmark III Building, Seto offers delicious and exclusive meals of the Japanese cuisine that make it one of the best Japanese restaurants in Baku.</p>\"}','/images/portfolio/profie_picture/seto.jpg','/images/portfolio/cover/seto-cover.jpg','The Landmark Business Center','seto-restaurant',12,0,'2019-01-15 13:10:23','2019-01-22 11:40:48'),
	(19,2,'August 2018/present day','{\"en\":\"<p>Photo shooting and editing</p><p>Design and Layout of the menu</p><p>Social Media promotion</p>\"}','CASPIAN GRILL & TERRACE','{\"en\":\"<p>Creation of&nbsp; the graphic concept, design and layout for the restaurant&nbsp; menu as well as for extra menu on special occasion, photo coverage of the food and the restaurant itself. Situated on the 20th floor of the Landmark III, Caspian Grill &amp; Terrace offers tasty meals of international cuisine with an amazing view of the Caspian Sea.</p>\"}','/images/portfolio/profie_picture/caspian-1.jpg','/images/portfolio/cover/caspians-cover.jpg','The Landmark Business Center','caspian-grill-and-terrace',11,0,'2019-01-15 13:14:47','2019-01-22 11:40:50'),
	(20,2,'August 2018/present day','{\"en\":\"<p>Photo shooting and editing</p><p>Design and Layout of the menu</p><p>Social Media promotion</p>\"}','SKY BAR & LOUNGE','{\"en\":\"<p>Creation of&nbsp; the graphic concept, design and layout for the restaurant&nbsp; menu as well as for extra menu on special occasion, photo coverage of the food and the restaurant itself. Situated on the 19th floor of the Landmark III, Sky Bar &amp; Lounge is the perfect place to enjoy a drink with a great view of the city.</p>\"}','/images/portfolio/profie_picture/skybar.jpg','/images/portfolio/cover/skybar-cover.jpg','The Landmark Business Center','sky-bar-and-lounge',13,0,'2019-01-15 13:19:14','2019-01-22 11:40:45'),
	(21,2,'August 2018/present day','{\"en\":\"<p>Photo shooting and editing</p><p>Design &amp; Layout of the menu</p><p>Social Media promotion</p>\"}','SHIN SHIN RESTAURANT','{\"en\":\"<p>Creation of&nbsp; the graphic concept, design and layout for the restaurant&nbsp; menu, photo coverage of the food and the restaurant itself. Situated on the ground floor of the Landmark III Building, Shin Shin offers exclusive and delicious meals of Chinese cuisine that make it one of the best Chinese restaurant in Baku.</p>\"}','/images/portfolio/profie_picture/shinshin.jpg','/images/portfolio/cover/shin-shin-cover.jpg','The Landmark Business Center','shin-shin-restaurant',14,0,'2019-01-15 13:24:52','2019-01-22 11:40:44'),
	(22,2,'September 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p>\"}','SHUKUHACHI CONCERT','{\"en\":\"<p>Creation of graphic and promo material, photo shooting and editing of the Shukuhachi concert, event promoted by the Embassy of Japan. <strong>Yosuke</strong><strong>&nbsp;</strong><strong>Irie</strong> is a renowned&nbsp;<strong>Japanese </strong><strong>Shakuhachi</strong>&nbsp;player who has so far performed in many countries, promoting Japanese culture, as well as peace through his&nbsp;music. Irie <strong>combines traditional and modern playing techniques</strong>, creating a unique sound.</p>\"}','/images/portfolio/profie_picture/shakuhachi.jpg','/images/portfolio/cover/shakuhachi_cover.jpg','United Cultures','shukuhachi-concert',18,0,'2019-01-15 13:34:03','2019-01-22 11:40:34'),
	(23,2,'November 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p>Organization of press office\"}','OFFICIAL OPENING OF CA\' FOSCARI DESK IN BAKU','{\"en\":\"<p>Creation of design and layout for all graphic and promo material and photo shooting of the official opening ceremony. After Suzhou,&nbsp;<strong>Baku</strong>. During the 150th Anniversary of its foundation,&nbsp;<strong>Ca’ Foscari University of&nbsp; Venice&nbsp;</strong>opens a&nbsp;brand-<strong>new representative desk abroad</strong>, in&nbsp;<strong>Azerbaijan</strong>, the&nbsp;<strong>second official office</strong>&nbsp;of the Venetian University in the world.</p>\"}','/images/portfolio/profie_picture/cafoscari.jpg','/images/portfolio/cover/cafoscari_cover.jpg','United Cultures','official-opening-of-ca-foscari-desk-in-baku',6,0,'2019-01-16 06:43:07','2019-01-22 11:41:02'),
	(24,2,'August 2018/present day','{\"en\":\"<p>Rebranding</p><p>Creation of graphic and promo material</p><p>Copywriting</p><p>Maintenance of Social Networks</p>Creation and maintenance of website\"}','UNITED CULTURES REBRANDING','{\"en\":\"<p>Rebranding of the company; creation of new graphic concept; realization of marketing material; copywriting; realization, maintenance and daily updating of the new website united-cultures.az; restyling and daily updating of social media profiles. United Cultures is an organization created with the aim of merging competence,&nbsp;<strong>passion, sensitivity and experience</strong>. It works alongside&nbsp;<strong>international organizations, diplomatic missions, public and private institutions</strong>, in order to create cultural and educational high value projects, supporting what is being described as Cultural Diplomacy.</p>\"}','/images/portfolio/profie_picture/united-cultures-portfolio-box.jpg','/images/portfolio/cover/united_cultures_cover.jpg','United Cultures','united-cultures-rebranding',7,0,'2019-01-16 08:32:16','2019-01-22 11:41:00'),
	(25,2,'November 2018','{\"en\":\"<p>Creation of graphic and promo material</p><p>Photo shooting and editing</p><p>Video shooting and editing</p>Promotion through social networks\"}','REMBETIKA MONOPATIA IN CONCERT','{\"en\":\"<p>Creation of graphic and promo material, promotion through social network, photo and video shooting of the concert of the famous Greek band, Rembetika Monopatia. The event was held on the occasion of the second edition of the Greek Experience at the 22nd floor of The Landmark Hotel.</p>\"}','/images/portfolio/profie_picture/greek-event.jpg','/images/portfolio/cover/greek_event_cover.jpg','The Landmark Business Center','rembetika-monopatia-in-concert',9,0,'2019-01-17 12:17:43','2019-01-22 11:40:55'),
	(26,2,'January 2019','{\"en\":\"<p>Creation of graphic concept</p><p>Creation of graphic and promo material</p><p>Creation of promo video</p><p>Creation of a dedicated Facebook page</p><p>Promotion through Media and Social Networks</p><p>Press Office</p>\"}','THE ITALIAN-AZERBAIJANI INTERNATIONAL FILM FESTIVAL','{\"en\":\"<p>Creation of graphic concept, realization of graphic and promo materials, creation and editing of promotional video, creation of a dedicated Facebook page, communication and promotion through Media and Social Networks of the first edition of the Italian-Azerbaijani International Film Festival organized by the Ministry of Cultures, United Cultures, Imago Group and Sandro Teti Editore.</p>\"}','/images/portfolio/profie_picture/az-it.jpg','/images/portfolio/cover/zoroaster_covers.jpg','United Cultures','the-italian-azerbaijani-international-film-festival',0,0,'2019-01-22 11:39:47','2019-01-23 13:25:56');

/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
