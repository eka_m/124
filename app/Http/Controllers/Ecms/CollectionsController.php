<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Collection;
use Illuminate\Http\Request;

class CollectionsController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Collection::orderBy('created_at', 'DESC')->get();
        return view('ecms::pages.collections.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection = new Collection;
        return view('ecms::pages.collections.create', compact('collection'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        try {
            $collection = new Collection();
            $collection->fill($request->all());
            if ($collection->save()) {
                \Toastr::success('Collection was successfully created!');
                return redirect()->route('ecms.collections.index');
            }
            \Toastr::error("Validation error ( ");
            return redirect()->route("ecms.collections.index")->with(['errors' => $collection->getErrors()]);
//        } catch (\Exception $e) {
//            \Toastr::success("Something went wrong ;( ");
//            return redirect()->route("ecms.collections.index");
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = Collection::find($id);
        return view('ecms::pages.collections.edit', compact('collection'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collection = Collection::find($id);
        $collection->fill($request->all());
        if ($collection->save()) {
            \Toastr::success('Collection was successfully saved!');
            return redirect()->route('ecms.collections.index');
        }
        \Toastr::error("Validation error ( ");
        return redirect()->route("ecms.collections.index")->with(['errors' => $collection->getErrors()]);
    }

    public function updateTableAttributes($modelName, $scheme) {
        //TODO:: Сделать так чтоб при изминении настройки мултиязычности значания менялись
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
