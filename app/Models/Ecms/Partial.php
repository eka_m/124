<?php

namespace App\Models\Ecms;

use Illuminate\Database\Eloquent\Model;

class Partial extends Model
{
    protected $fillable = [
        'name',
        'content',
        'keyvalue'
    ];
    
    protected $rules = [
        'name' => 'required',
    ];
}
