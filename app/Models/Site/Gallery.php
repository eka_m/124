<?php

namespace App\Models\Site;

use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use ImageManager;
    protected $casts = ['params' => 'array'];

    public function getItemsAttribute($value)
    {
        $params = $this->params;
        $images = makeCollection(json_decode($value));
        $images->map(function ($item) use ($params) {
            if ($params["type"] == 'comparison') {
                $item->images->comparison->before = $this->makeImage($item->images->comparison->before);
                $item->images->comparison->after = $this->makeImage($item->images->comparison->after);
            } elseif ($params["type"] == 'slider') {
                $item->images->slider = $this->makeImage($item->images->slider);
            }
            return $item;
        });
        return $images;
    }

}
