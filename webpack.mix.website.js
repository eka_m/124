const {mix} = require("laravel-mix");
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.browserSync({
    open: 'external',
    proxy: "124.ek",
    host: "124.ek",
    // port: 8000,
    scrollRestoreTechnique: "cookie",
    files: [
        "public/**/*.css",
        "resources/**/*",
        "resources/**/*.blade.php"
    ]
});
mix.sass("resources/site/sass/app.scss", "public/site/css")
    .js(
        "resources/site/js/scripts/custom.js",
        "public/site/js/custom.min.js"
    )
    .js(
        "resources/site/js/scripts/main.js",
        "public/site/js/main.min.js"
    )
    .js(
        "resources/site/js/scripts/animation.js",
        "public/site/js/animation.min.js"
    )
    .js(
        "resources/site/js/scripts/animation-home.js",
        "public/site/js/animation-home.min.js"
    )
	.js(
		"resources/site/js/scripts/gallery.js",
		"public/site/js/gallery.min.js"
	)
    // .babel(
    //     "resources/site/js/scripts/initmap.js",
    //     "public/site/js/initmap.min.js"
    // )
    .options({
        processCssUrls: false,
        postcss: [require("autoprefixer")]
    });
mix.webpackConfig({
    module: {
        rules: [{
            test: /\.js?$/,
            use: [{
                loader: 'babel-loader',
                options: mix.config.babel()
            }]
        }]
    }
});
