<?php

namespace App\Models\Ecms;

class TheCollectionsBase extends Base
{
    public function getDisplayAttribute()
    {
        try {
            $scheme = makeCollection($this->collection->scheme);
            $field = $scheme->firstWhere("display", true);
            if ($field !== null) {
                if ($field->multilang && isJSON($this->{$field->name})) {
                    return json_decode($this->{$field->name})->{\Setting('languages.default')};
                }
                return $this->{$field->name};
            }
            return "No name";
        } catch (\Exception $e) {
            return "No name";
        }
    }
}
