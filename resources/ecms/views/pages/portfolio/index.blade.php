@extends('ecms::layouts.base') 
@section('title', 'Portfolio') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-teal"></div>
                <div class="card-header">
                    <h3 class="card-title">Portfolio</h3>
                    <div class="card-options">
                        <a href="{{route('ecms.portfolio.create')}}" class="btn btn-teal">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($items as $item)
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="card-category">{{json_decode($item->name,true)[\Setting::get('languages.default')]}}</div>
                                    <div class="display-3 mt-4 mb-1">{{$item->items->count()}}</div>
                                    <ul class="list-unstyled leading-loose">
                                        <li>
                                            <a href="{{route('ecms.portfolio.items',$item->id)}}">Items</a>
                                        </li>
                                    </ul>
                                    <div class="text-center mt-6">
                                        <a href="{{route('ecms.portfolio.items',$item->id)}}" class="btn btn-indigo">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        <a href="{{route('ecms.portfolio.edit',$item->id)}}" class="btn btn-azure">
                                                <i class="fa fa-pencil-alt"></i>
                                            </a>
                                        <a href="{{route('ecms.portfolio.destroy',$item->id)}}" class="btn btn-danger" data-method="delete" data-token="{{csrf_token()}}"
                                            data-confirm="Are you sure?">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection