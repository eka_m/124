<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                    <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                    <div class="input-icon-addon">
                        <i class="fe fe-search"></i>
                    </div>
                </form>
            </div>
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item dropdown">
                        <a href="{{route('ecms.pages.index')}}" class="nav-link {{request()->has('pages') ? 'active' : ''}}" data-toggle="dropdown"><i class="fas fa-file-alt"></i> Pages</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="{{route('ecms.pages.index')}}" class="dropdown-item {{request()->has('pages') ? 'active' : ''}}"> All</a>
                            <a href="{{route('ecms.pages.create')}}" class="dropdown-item {{request()->has('pages/create') ? 'active' : ''}}"><i class="fas fa-plus"></i> New</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link {{request()->has('articles') ? 'active' : ''}}" data-toggle="dropdown"><i class="fas fa-newspaper"></i> Articles</a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-box"></i> Interface</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link {{request()->has('portfolio') ? 'active' : ''}}" data-toggle="dropdown"><i class="fe fe-calendar"></i> Components</a>
                        <div class="dropdown-menu dropdown-menu-arrow">
                            <a href="{{route('ecms.partials.index')}}" class="dropdown-item {{request()->has('partials') ? 'active' : ''}}">Partials</a>
                            <a href="{{route('ecms.portfolio.index')}}" class="dropdown-item {{request()->has('portfolio') ? 'active' : ''}}">Portfolio</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="./gallery.html" class="nav-link"><i class="fas fa-images"></i> Galleries</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('ecms.locales.index')}}" class="nav-link {{request()->is('admin/localization') ? 'active' : ''}}"><i class="fas fa-globe"></i> Localization</a>
                    </li>
                    <li class="nav-item">
                        <a href="./gallery.html" class="nav-link {{request()->is('admin/settings') ? 'active' : ''}}"><i class="fas fa-cog"></i> Settings</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>