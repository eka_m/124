<?php

Route::get('/login', 'Ecms\Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Ecms\Auth\LoginController@login')->name('login');
Route::get('/logout', 'Ecms\Auth\LoginController@logout')->name('logout');

Route::get('/', 'Ecms\HomeController@show')->name('home');

/** PAGES */
Route::post('/pages/sort', 'Ecms\PagesController@sort');
Route::post('/pages/createpage', 'Ecms\PagesController@createPage');
Route::post('/pages/setStatus/{id}', 'Ecms\PagesController@setStatus');
Route::resource('/pages', 'Ecms\PagesController');
/** END PAGES */

/** PORTFOLIO*/
Route::post('/portfolio/{id}/items', 'Ecms\PortfolioController@storeItems')->name('portfolio.items.store');
Route::put('/portfolio/{id}/items', 'Ecms\PortfolioController@updateItems')->name('portfolio.items.update');
Route::get('/portfolio/{id}/items', 'Ecms\PortfolioController@items')->name('portfolio.items');
Route::resource('/portfolio', 'Ecms\PortfolioController');
/** END PORTFOLIO*/

/** PARTIALS */
Route::resource('/partials', 'Ecms\PartialsController');
/** END PARTIALS */

/** LOCALE */
Route::get('/locales', 'Ecms\LocaleController@index')->name('locales.index');
Route::post('/locales', 'Ecms\LocaleController@store')->name('locales.store');
/** END LOCALE */

/** GALLERY */
Route::resource('/gallery', 'Ecms\GalleryController');
/** END GALLERY */

/** COLLECTIONS */
Route::resource('/collections', 'Ecms\CollectionsController');
Route::group(['prefix' => 'collection', 'as' => 'collection.'], function() {
	Route::get('/{slug}', 'Ecms\TheCollectionController@index')->name('index');
	Route::get('/create/{slug}', 'Ecms\TheCollectionController@create')->name('create');
	Route::post('/{slug}', 'Ecms\TheCollectionController@store')->name('store');
	Route::get('/{slug}/{record}', 'Ecms\TheCollectionController@show')->name('show');
	Route::get('/{slug}/{record}/edit', 'Ecms\TheCollectionController@edit')->name('edit');
    Route::put('/{slug}/{record}', 'Ecms\TheCollectionController@update')->name('update');
    Route::get('/sort/{slug}/{record}/{move}', 'Ecms\TheCollectionController@sort')->name('sort');
    Route::delete('/{slug}/{record}', 'Ecms\TheCollectionController@destroy')->name('destroy');

    Route::get('/async/{slug}', 'Ecms\TheCollectionController@async');
});
/** END COLLECTIONS */

/* UPLOADS */
Route::post('/files/upload', 'Ecms\FilesController@upload');
Route::get('/files/all', 'Ecms\FilesController@getAllImages');
Route::get('/files/froalablocks', 'Ecms\FilesController@froalaBlocks');
Route::get('/files/crop-image', 'Ecms\FilesController@cropImage');
