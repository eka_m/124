@extends('layouts.base')
@section('title', " | ".$page->name)
@section('content')
	<div class="nk-main nk-main-dark bg-black body-nice-scroll">
		<!-- START: Contact Info -->
		<div class="bg-black">
			<div class="container">
				<div class="nk-gap nk-gap-5 mnt-3"></div>
				<div class="nk-gap-5 mnt-10"></div>
				<div class="row vertical-gap">
					<div class="col-lg-5">
						<!-- START: Info -->
						<h2 class="r-head">124...<span class="text-main">CONTACTS</span></h2>
						<h4>You say, we hear.</h4>
						<div class="nk-gap mnt-3"></div>
						<ul class="nk-contact-info r-text">
							<li><strong>Address:</strong> The Landmark I / 96A Nizami St, Baku, Azerbaijan</li>
							<li><strong>Phone:</strong> +994 (50) 975 87 00</li>
							<li><strong>Email:</strong>info@124studios.az</li>
							<li>
								<div class="d-flex align-items-center ">
									<div>
										<strong>Social: </strong>
									</div>
									<div class="contact-social">
										<a href="https://www.facebook.com/124studiosBaku"><i class="fa fa-facebook-f r-text"></i></a>
										<a href="https://www.instagram.com/124studios_az/?hl=en"><i class="fa fa-instagram r-text"></i></a>
										<a href="https://www.youtube.com/channel/UCBGa3nZSTiL1k4C1W4gnLog?view_as=subscriber "><i
												class="fa fa-youtube-play r-text"></i></a>
									</div>

								</div>
							</li>
						</ul>
						<!-- END: Info -->
					</div>

					<div class="col-lg-7">
						<!-- START: Form -->
						<form action="" class="nk-form nk-form-ajax">
							<div class="row vertical-gap">
								<div class="col-md-6">
									<input type="text" class="form-control required" name="name" placeholder="Your Name">
								</div>
								<div class="col-md-6">
									<input type="email" class="form-control required" name="email" placeholder="Your Email">
								</div>
							</div>

							<div class="nk-gap-1"></div>
							<input type="text" class="form-control required" name="title" placeholder="Your Title">

							<div class="nk-gap-1"></div>
							<textarea class="form-control required" name="message" rows="8" placeholder="Your Message"
							          aria-required="true"></textarea>
							<div class="nk-gap-1"></div>
							<div class="nk-form-response-success"></div>
							<div class="nk-form-response-error"></div>
							<button class="nk-btn nk-btn-color-dark-3 text-black">Send Message</button>
						</form>
						<!-- END: Form -->
					</div>
				</div>
				<div class="nk-gap-5"></div>
			</div>
		</div>
		<!-- END: Contact Info -->
		<!-- START: Google Map -->
		<div id="google-map-contact" class="nk-gmaps nk-gmaps-lg"></div>
		<script type="text/javascript"
		        src="https://maps.google.com/maps/api/js?key=AIzaSyA5qMD6RTQQe8MFTv1CaS7cg8o8ivVT33w&sensor=false"></script>
		<script>
			function initializeGmaps () {
				var LatLng = { lat: 40.375619, lng: 49.850079 };
				var mapCanvas = document.getElementById( 'google-map-contact' );
				var mapOptions = {
					center: LatLng,
					scrollwheel: false,
					zoom: 18,
					backgroundColor: 'none',
					mapTypeId: 'nKStyle'
				};
				var map = new google.maps.Map( mapCanvas, mapOptions );
				var marker = new google.maps.Marker( {
					position: LatLng,
					map: map,
					icon: '/site/img/marker.png',
					title: '124 Studios'
				} );

				// style from https://snazzymaps.com/style/151/ultra-light-with-labels
				var styledMapType = new google.maps.StyledMapType( [ {
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [ { "color": "#e9e9e9" }, { "lightness": 17 } ]
				}, {
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [ { "color": "#f5f5f5" }, { "lightness": 20 } ]
				}, {
					"featureType": "road.highway",
					"elementType": "geometry.fill",
					"stylers": [ { "color": "#ffffff" }, { "lightness": 17 } ]
				}, {
					"featureType": "road.highway",
					"elementType": "geometry.stroke",
					"stylers": [ { "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 } ]
				}, {
					"featureType": "road.arterial",
					"elementType": "geometry",
					"stylers": [ { "color": "#ffffff" }, { "lightness": 18 } ]
				}, {
					"featureType": "road.local",
					"elementType": "geometry",
					"stylers": [ { "color": "#ffffff" }, { "lightness": 16 } ]
				}, {
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [ { "color": "#f5f5f5" }, { "lightness": 21 } ]
				}, {
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [ { "color": "#dedede" }, { "lightness": 21 } ]
				}, {
					"elementType": "labels.text.stroke",
					"stylers": [ { "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 } ]
				}, {
					"elementType": "labels.text.fill",
					"stylers": [ { "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 } ]
				}, { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, {
					"featureType": "transit",
					"elementType": "geometry",
					"stylers": [ { "color": "#f2f2f2" }, { "lightness": 19 } ]
				}, {
					"featureType": "administrative",
					"elementType": "geometry.fill",
					"stylers": [ { "color": "#fefefe" }, { "lightness": 20 } ]
				}, {
					"featureType": "administrative",
					"elementType": "geometry.stroke",
					"stylers": [ { "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 } ]
				} ], { name: 'nKStyle' } );
				map.mapTypes.set( 'nKStyle', styledMapType );
			}

			if ( typeof google !== 'undefined' ) {
				google.maps.event.addDomListener( window, 'load', initializeGmaps );
			}
		</script>
		<!-- END: Google Map -->
	</div>
	@include('partials.footer')
@endsection
