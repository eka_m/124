<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>124 studios @yield('title')</title>
	<meta name="description" content="@yield('keywords')">
	<meta name="author" content="@yield('description')">
	<meta id="theme" name="theme-color" content="#000000"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="apple-touch-icon-precomposed" sizes="57x57"
	      href="{{asset('site/img/favicon/apple-touch-icon-57x57.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
	      href="{{asset('site/img/favicon/apple-touch-icon-114x114.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="72x72"
	      href="{{asset('site/img/favicon/apple-touch-icon-72x72.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="144x144"
	      href="{{asset('site/img/favicon/apple-touch-icon-144x144.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="60x60"
	      href="{{asset('site/img/favicon/apple-touch-icon-60x60.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="120x120"
	      href="{{asset('site/img/favicon/apple-touch-icon-120x120.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="76x76"
	      href="{{asset('site/img/favicon/apple-touch-icon-76x76.png')}}"/>
	<link rel="apple-touch-icon-precomposed" sizes="152x152"
	      href="{{asset('site/img/favicon/apple-touch-icon-152x152.png')}}"/>
	<link rel="icon" type="image/png" href="{{asset('site/img/favicon/favicon-196x196.png')}}" sizes="196x196"/>
	<link rel="icon" type="image/png" href="{{asset('site/img/favicon/favicon-96x96.png')}}" sizes="96x96"/>
	<link rel="icon" type="image/png" href="{{asset('site/img/favicon/favicon-32x32.png')}}" sizes="32x32"/>
	<link rel="icon" type="image/png" href="{{asset('site/img/favicon/favicon-16x16.png')}}" sizes="16x16"/>
	<link rel="icon" type="image/png" href="{{asset('site/img/favicon/favicon-128.png')}}" sizes="128x128"/>
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF"/>
	<meta name="msapplication-TileImage" content="{{asset('site/img/favicon/mstile-144x144.png')}}"/>
	<meta name="msapplication-square70x70logo" content="{{asset('site/img/favicon/mstile-70x70.png')}}"/>
	<meta name="msapplication-square150x150logo" content="{{asset('site/img/favicon/mstile-150x150.png')}}"/>
	<meta name="msapplication-wide310x150logo" content="{{asset('site/img/favicon/mstile-310x150.png')}}"/>
	<meta name="msapplication-square310x310logo" content="{{asset('site/img/favicon/mstile-310x310.png')}}"/>

	@include('partials.css')
	<style>
		.hiddenv {
			visibility: hidden;
			/*pointer-events: none;*/
		}
	</style>

</head>

<body>

<div id="app" class="hiddenv">
	<main id="main">
		<header class="nk-header">
			@include('partials.nav')
		</header>
		@include('partials.mobile-nav')
		<div id="content">
			@yield('content')
		</div>
	</main>
</div>
@include('partials.loading')
@include('partials.js')
</body>

</html>
