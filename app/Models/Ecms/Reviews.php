<?php namespace App\Models\Ecms;

class Reviews extends TheCollectionsBase  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['slug','position','collection_id','active','name','review'];

    protected $rules = ['name' => 'required','review' => 'required'];

    protected $appends = ['display'];

    public function collection() {
        return $this->belongsTo('App\Models\Ecms\Collection');
    }
}
