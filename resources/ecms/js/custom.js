const app = {
  card: "div.card",
  initCards() {
    $('[data-toggle="card-remove"]').on("click", function(e) {
      let $card = $(this).closest(this.card);

      $card.remove();

      e.preventDefault();
      return false;
    });

    $('[data-toggle="card-collapse"]').on("click", function(e) {
      let $card = $(this)
        .parent()
        .parent(this.card);

      $card.toggleClass("card-collapsed");

      e.preventDefault();
      return false;
    });

    $('[data-toggle="card-fullscreen"]').on("click", function(e) {
      let $card = $(this)
        .parent()
        .parent()
        .parent(this.card);

      $card.toggleClass("card-fullscreen").removeClass("card-collapsed");

      e.preventDefault();
      return false;
    });
  },
  initTooltips() {
    $('[data-toggle="tooltip"]').tooltip();
  },
  loading() {
    $(".loading-content").toggleClass("active");
  },


  initDeleteButton() {
    var Laravel = {
      initialize: function() {
        this.methodLinks = $("a[data-method]");
        this.token = $("a[data-token]");
        this.registerEvents();
      },

      registerEvents: function() {
        this.methodLinks.on("click", this.handleMethod);
      },

      handleMethod: function(e) {
        e.preventDefault();
        var link = $(this);
        var httpMethod = link.data("method").toUpperCase();
        var form;
        if ($.inArray(httpMethod, ["PUT", "DELETE"]) === -1) {
          return false;
        }

        Laravel.verifyConfirm(link).done(function() {
          form = Laravel.createForm(link);
          form.submit();
        });
      },

      verifyConfirm: function(link) {
        var confirm = new $.Deferred();
        var userResponse = window.confirm(link.data("confirm"));
        if (userResponse) {
          confirm.resolve(link);
        } else {
          confirm.reject(link);
        }
        return confirm.promise();
      },

      createForm: function(link) {
        var form = $("<form>", {
          method: "POST",
          action: link.attr("href")
        });

        var token = $("<input>", {
          type: "hidden",
          name: "_token",
          value: link.data("token")
        });

        var hiddenInput = $("<input>", {
          name: "_method",
          type: "hidden",
          value: link.data("method")
        });

        return form.append(token, hiddenInput).appendTo("body");
      }
    };
    Laravel.initialize();
  },


  initSidebar() {
    $(".sidebar-dropdown > a").click(function() {
      $(".sidebar-submenu").slideUp(200);
      if (
        $(this)
          .parent()
          .hasClass("active")
      ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .parent()
          .removeClass("active");
      } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .next(".sidebar-submenu")
          .slideDown(200);
        $(this)
          .parent()
          .addClass("active");
      }
    });

    $("#close-sidebar").click(function() {
      $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(function() {
      $(".page-wrapper").addClass("toggled");
    });
  }
}

$(() => {
  app.initCards();
  app.initSidebar();
  app.initTooltips();
  app.loading();
  app.initDeleteButton();
});
