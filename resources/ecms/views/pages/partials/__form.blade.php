<div class="tab-content py-5" id="myTabContent">
	<div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="home-tab">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group translatable">
					<label class="form-label">Name</label>
					<input type="text" class="form-control" name="name" placeholder="Type partial name" value="{{old('name', $partial->name)}}">
				</div>
				<div class="form-group translatable">
					<label class="form-label">Content</label>
					<tinymce name="content"  @if($partial->content):current="{{$partial->content}}"@endif></tinymce>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content-tab">
		<div class="row">
			<div class="col-12 translatable">
				<keyvalue name="keyvalue" @if($partial->keyvalue):current="{{$partial->keyvalue}}" @endif></keyvalue>
			</div>
		</div>
	</div>
</div>
