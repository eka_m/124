<?php

namespace App\Models\Ecms;

use App\Traits\CollectionManager;

class Collection extends Base
{
    use CollectionManager;
    protected $appends = ['inputs'];
    protected $fillable = [
        'parent_id',
        'name',
        'dname',
        'icon',
        'slug',
        'scheme',
        'status',
    ];

    protected $rules = [
        'name' => 'required|unique:collections,name',
        'dname' => 'required',
        'slug' => 'required|unique:collections,slug',
    ];

    public static function getCollectionModel($name)
    {
        return "App\Models\Ecms\\" . ucfirst($name);
    }

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $model->createTable($model);
        });
        self::updated(function ($model) {
            $oldModel = $model->getOriginal();
            $model->updateTable($model,$oldModel);
        });

        self::deleted(function ($model) {

        });
    }

    public function getSchemeAttribute($value)
    {
        return json_decode($value);
    }

    public function setSchemeAttribute($value) {
        $scheme = json_decode($value);
        array_walk($scheme, function ($field, $key) {
           if(!isset($field->id) || !$field->id || $field->id == "" || $field->id == null) {
               $field->id =  str_random(10) . rand(1, 100) . $key;
           }
        });
        $this->attributes['scheme'] = json_encode($scheme);
    }

    public function getInputsAttribute()
    {
        $result = collect([]);
        foreach ($this->scheme as $key => $input) {
            $result->push($input);
        }
        return $result->groupBy('type');
    }
}
