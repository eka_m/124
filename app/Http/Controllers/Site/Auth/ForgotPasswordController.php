<?php

namespace App\Http\Controllers\Site\Auth;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\SMSC;
use App\Models\Base\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
     */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('BASE.pages.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|phoneOrEmail']);

        if (!is_numeric($request->email)) {
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );
            return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
        } else {
            $user = User::where('phone', ltrim($request->email, '+'))->first();
            if (!$user) {
                return redirect()->back()->with('message', 'This user not exists!');
            }
            $token        = random_int(100000, 999999);
            $hashed_token = Hash::make($token);
            $response     = DB::insert('insert into user_password_resets (email, token, created_at) values (?, ?, ?)', [$user->phone, $hashed_token, Carbon::now()]) ? 'passwords.sent' : false;
            $sms          = new SMSC();
            $sms->send_sms($user->phone, "Your password reset code is - $token");
            return $response == Password::RESET_LINK_SENT
            ? view('BASE.pages.auth.passwords.sms-token', ['token' => $token, 'phone' => $user->phone])
            : $this->sendResetLinkFailedResponse($request, $response);
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('users');
    }
}
