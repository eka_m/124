<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Collection;
use Illuminate\Http\Request;

class TheCollectionController extends BaseController
{


	/**
	 * Display a listing of the resource.
	 *
	 * @param $slug
	 * @return \Illuminate\Http\Response
	 */
	public function index ( $slug )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$maxpos = $model::max( 'position' );
		$records = $model::orderBy( 'position', 'ASC' )->paginate( 16 );
		return view( 'ecms::pages.thecollection.index', compact( 'collection', 'records', 'maxpos' ) );
	}

	public function async ( $slug )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$records = $model::orderBy( 'position', 'ASC' )->get();
		return response()->json( $records );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $slug
	 * @return \Illuminate\Http\Response
	 */
	public function create ( $slug )
	{

		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$record = new $model;
		return view( 'ecms::pages.thecollection.create', compact( 'collection', 'record' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param $slug
	 * @return \Illuminate\Http\Response
	 */
	public function store ( Request $request, $slug )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$record = new $model;
		$record->fill( $request->all() );
		if ( $record->save() ) {
			\Toastr::success( "Record was successfully saved!" );
			return redirect()->route( "ecms.collection.index", $slug );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param $slug
	 * @param  int $id
	 * @return void
	 */
	public function show ( $slug, $id )
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $slug
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ( $slug, $id )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$record = $model::find( $id );
		return view( 'ecms::pages.thecollection.edit', compact( 'collection', 'record' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param $slug
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update ( Request $request, $slug, $id )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$record = $model::find( $id );
		$record->fill( $request->all() );
		if ( $record->save() ) {
			\Toastr::success( "Record was successfully saved!" );
			return redirect()->route( "ecms.collection.index", $slug );
		}
	}

	public function sort ( $slug, $id, $move )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		$record = $model::find( $id );
		if ( $move === 'up' ) {
			$record->moveOrderUp();
		} elseif ( $move === 'down' ) {
			$record->moveOrderDown();
		}
		\Toastr::success( "Record was moved " . $move );
		return redirect()->back();
	}

	public function destroy ( $slug, $id )
	{
		$collection = Collection::slug( $slug )->firstOrFail();
		$model = Collection::getCollectionModel( $collection->name );
		if ( $model::destroy( $id ) ) {
			\Toastr::success( "Record was successfully deleted!" );
			return redirect()->route( "ecms.collection.index", $slug );
		}
	}
}
