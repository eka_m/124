<?php

namespace App\Http\Controllers\Ecms;

use App\Models\Ecms\Partial;
use Illuminate\Http\Request;

class PartialsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Partial::orderBy('created_at', 'DESC')->get();
        return view('ecms::pages.partials.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partial = new Partial();
        return view('ecms::pages.partials.create', compact('partial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $partial = new Partial;
            $partial->fill($request->all());
            if ($partial->save()) {
                \Toastr::success('Partial was successfully created!');
                return redirect()->route('ecms.partials.index');
            }
            \Toastr::error("Validation error ( ");
            return redirect()->route("ecms.partials.index")->with(['errors' => $page->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.pages.index");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ecms\Partial  $partial
     * @return \Illuminate\Http\Response
     */
    public function show(Partial $partial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ecms\Partial  $partial
     * @return \Illuminate\Http\Response
     */
    public function edit(Partial $partial)
    {
        return view('ecms::pages.partials.edit', compact('partial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ecms\Partial  $partial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partial $partial)
    {
        try {
            $partial->fill($request->all());
            if ($partial->save()) {
                \Toastr::success('Partial was successfully saved!');
                return redirect()->route('ecms.partials.index');
            }
            \Toastr::error("Validation error ( ");
            return redirect()->route("ecms.partials.index")->with(['errors' => $page->getErrors()]);
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong ;( ");
            return redirect()->route("ecms.pages.index");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ecms\Partial  $partial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partial $partial)
    {
        try {
            if ($partial->delete()) {
                \Toastr::success("Partial was successfully deleted!");
                return redirect()->route("ecms.partials.index");
            }
        } catch (\Exception $e) {
            \Toastr::success("Something went wrong");
            return redirect()->route("ecms.partials.index");
        }
    }
}
