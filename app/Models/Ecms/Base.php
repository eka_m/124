<?php

namespace App\Models\Ecms;

use Illuminate\Support\Facades\Event;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;
use App\Traits\StateManager;

class Base extends Model
{
    use ValidatingTrait;

    use Translatable;
    use StateManager;

    protected $appends = ['state'];

    protected $casts = [
        'params' => 'array',
    ];

    public function scopeSlug($query,$slug) {
        return $query->where('slug', $slug);
    }
}
