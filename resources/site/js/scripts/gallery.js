import { $ } from "./parts/_utility";

const gallery = {
	mixer: null,
	container: $( '#gallery' ),
	loadmore_btn: $( "#loadmore_button_gallery" ),
	btn_text: null,
	category: 'all',
	link: null,
	total: 0,
	current_page: 1,
	next_page: 0,
	last_page: 0,
	init () {
		this.btn_text = this.loadmore_btn.html();
		this.link = this.loadmore_btn.attr( "href" );
		this.last_page = this.loadmore_btn.attr( 'last-page' );
		this.next_page = Math.ceil( Number( this.current_page ) + 1 );
		if ( this.last_page > 1 ) {
			this.loadmore_btn.removeClass( 'd-none' )
		}
	},
	listenLoadMoreClick () {
		this.loadmore_btn.click( e => {
			e.preventDefault();
			this.loadmore_btn.html( '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>' );
			this.fetchData()
		} )
	},
	listenCategoryClick () {
		$( '[data-filter]' ).click( e => {
			this.container.html( '<div class="async-added" style="width: 100%; height: 300px; display: flex; align-items: center; justify-content: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>' );
			this.category = $( e.currentTarget ).data( 'name' );
			this.current_page = 1;
			this.next_page = 1;
			this.fetchData( true );
		} )
	},
	fetchData ( clearContainer = false ) {
		$.getJSON( this.link + '/' + this.category + "?page=" + this.next_page, ( data, status ) => {
			if ( data.items.length === 0 ) {
				this.loadmore_btn.html( 'No more records' );
			}
			if ( status === "success" && data.items.length > 0 ) {
				this.render( data.items, clearContainer );
				this.loadmore_btn.html( data.pagination.current_page === data.pagination.last_page ? 'No more records' : this.btn_text );
				this.current_page = data.pagination.current_page;
				this.last_page = data.pagination.last_page;
				this.next_page = Math.ceil( Number( this.next_page ) + 1 );
			}
		} );
	},
	render ( items, clearContainer ) {
		items.forEach( ( item, index ) => {
			if ( clearContainer && index === 0 ) {
				this.container.empty();
			}
			const elem = $( item );
			const cls = 'newitem' + index;
			elem.addClass( cls );
			elem.find( "img" ).addClass( "d-none" );
			const imgContainer = elem.find( '.nk-portfolio-item-video-cover' );
			imgContainer.append( '<div class="spinContainer" style="width: 100%; height: 210px; display: flex; align-items: center; justify-content: center;"><div class="spinner"></div></div>' );
			elem.addClass( 'async-added' ).appendTo( this.container );
			$( item ).imagesLoaded( () => {
				this.container.find( '.' + cls ).find( '.spinContainer' ).remove();
				this.container.find( '.' + cls ).find( "img" ).removeClass( "d-none" );
			} );
		} )
	}
};

$( document ).ready( function () {
	gallery.init();
	gallery.listenLoadMoreClick();
	gallery.listenCategoryClick();
} );