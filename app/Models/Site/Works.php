<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Works extends Model
{
	use HasTranslations;
	public $translatable = [ 'skills', 'description' ];
}
