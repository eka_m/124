<?php

namespace App\Models\Ecms;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
      'name',
      'description',
      'slug',
      'items',
      'params'
    ];

    protected $rules = ['name' => 'required'];
}
