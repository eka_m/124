<?php

namespace App\Http\Controllers\Ecms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FilesController extends BaseController
{
    public function upload(Request $request)
    {
        dd($request->all());
    }

    public function getAllImages()
    {
        $images = collect(Storage::disk('public')->allFiles('/uploads/images'))->map(function ($file) {
            return [
                'type' => 'image',
                'src' => Storage::disk('public')->url($file),
                'height' => \Image::make($file)->height(),
                'width' => \Image::make($file)->width(),
            ];
        });
        return response()->json($images, 200);
    }

    public function froalaBlocks()
    {
        $blocks = collect(Storage::disk('public')->allFiles('/ecms/plugins/froala-blocks'))->filter(function ($block) {
            return File::extension($block) == 'html';
        });
        dd($blocks);
    }
    public function cropImage()
    {
        $img = \Image::make(
        public_path().
        '/uploads/images/first-cover.jpg');
        $img->crop(311, 467, 242, 215);

        return view("site::welcomes", ['img' => $img->encode('data-url')]);
    }
}
