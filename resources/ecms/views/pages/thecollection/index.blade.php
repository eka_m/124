@extends('ecms::layouts.base')
@section('title', 'Collections')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">
							<i class="{{$collection->icon}}"></i>
							{{$collection->dname}}
						</h4>
						<div class="card-options text-right">
							<a href="{{route('ecms.collection.create',$collection->name, $collection->slug)}}"
							   class="btn btn-icon btn-teal"><i class="fas fa-plus"></i></a>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							@foreach ($records as $record)
								<div class="col-md-3 p-2">
									<div class="card  h-100">
										<div class="card-body d-flex align-items-center justify-content-center flex-column text-center">
											<div class="card-category">
												{{$record->display}}
											</div>
											<div class="text-center  mt-6">
												<a
													href="{{route('ecms.collection.edit', ['slug' => $collection->slug, 'record' => $record->id])}}"
													class="btn btn-azure">
													<i class="fa fa-pencil-alt"></i>
												</a>
												<a
													href="{{route('ecms.collection.destroy', ['slug' => $collection->slug, 'record' => $record->id])}}"
													class="btn btn-danger" data-method="delete" data-token="{{csrf_token()}}"
													data-confirm="Are you sure?">
													<i class="fa fa-trash"></i>
												</a>
											</div>
											<div class="text-center mt-6">
												@if($record->position !== $records->min('position'))
													<a
														href="{{route('ecms.collection.sort', ['slug' => $collection->slug, 'record' => $record->id, 'move' => 'up'])}}"
														class="btn">
														<i class="fa fa-chevron-left"></i>
													</a>
												@endif
												@if($record->position !== $maxpos)
													<a
														href="{{route('ecms.collection.sort', ['slug' => $collection->slug, 'record' => $record->id, 'move' => 'down'])}}"
														class="btn">
														<i class="fa fa-chevron-right"></i>
													</a>
												@endif
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
					<div class="card-footer text-center d-flex align-items-center justify-content-center">
						<div>
							{{ $records->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
