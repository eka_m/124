@extends('layouts.base')
@section('title', " | ".$page->name)
@section('content')
	<div class="nk-main bg-transparent ">
		<div class="nk-fullpage nk-fullpage-nav-style-2">
			<div class="nk-fullpage-item nk-fullpage-split nk-fullpage-item-bg-dark">
				<div class="nk-fullpage-content">
					<div class="container-fluid max-width">
						<div class="row">
							<div class="col-lg-6 d-none d-lg-block">
								<div class="bg-image text-center d-flex align-items-center justify-content-center">
									{!! svg('site/img/rocket.svg', 'rocket-svg') !!}
								</div>
							</div>
							<div class="col-lg-6 d-xl-flex justify-content-xl-center">
								<div class="bg-image text-center d-flex align-items-center justify-content-center d-lg-none d-block">
									{!! svg('site/img/rocket.svg', 'rocket-svg') !!}
								</div>
								<div class="nk-gap-2"></div>
								<div class="nk-box-1 mw-600 scroll optimize-h">
									<h2 class="fw-400 text-white brand-hover-effect r-head">1,2,4... <span
											class="text-main">GO!</span></h2>
									<p class="text-white r-text">
										We are a Baku based creative and music agency with a strong
										<span class="text-main">Italian creativity, Azerbaijani identity and International vision.</span>
										<br/>
										We started our journey as the communication department of United Cultures MMC, a reality that has a
										long experience in the creation of concepts and organization of cultural events, working alongside
										international organizations, public institutions and private companies. As projects requests kept
										coming, we thought essential to set up an agency that could fully dedicate itself to marketing and
										communication. So we did!
									</p>
								</div>
							</div>
						</div>
						<a class="nk-fullpage-scroll-down text-white d-none d-md-block" href="#"><span
								class="pe-7s-angle-down"></span></a>
					</div>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-split nk-fullpage-item-bg-dark">
				<div class="nk-fullpage-content">
					<div class="nk-fullpage-bg-image-custom">
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 order-2 order-md-1 pt-30 ">
								<div class="row">
									<div class="col-md-8 offset-md-2 col-sm-12 pt-50">
										<h2 class="text-white nk-fullpage-title r-head">124... <span class="text-main">REASONS</span></h2>
										<div class="text-white">
											<p class="r-text">
												Why choose us ? <br/>
												The short answer is because we are <span class="text-main">reliable.</span>
												<br/>
												The longer one, it is because we are simple but no boring, humble but no
												self-doubting, bold but
												no
												cheeky, <span class="text-main">different but not indifferent!</span>
												<br/>
												Communication is a matter of emotions and feelings and we know how to generate them. Inspired by
												beauty, we are able to build unique experiences to be shared with the people.
												The line between a successful and an unsuccessful communication project can be very thin though.
												We
												work to make that line go in the right direction. <span
													class="text-main">Where do you want to go?</span>
											</p>

										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 order-1 order-md-2 position-relative text-center">
								<div class="nk-rotatable-text nk-fullpage-title d-flex align-items-center justify-content-center
              text-white text-uppercase position-relative r-head">
									<div class="nk-gap-1 d-block d-md-none"></div>
									<div id="text-rotate" class="text-rotate text-main"
									     data-words="Ready?,Cool,Reliable,Trustworthy,Patient,Versatile,Deep,Passionate,Creative,Good,Sensitive,Accurate,Genuine,Authentic,Hard workers,Real,Efficient,Brave,Sparkling,Elegant,Careful,Proud,Keen,Responsive,Zealous,Enthusiastic,Clever,Smart,Attentive,Kind,Generous,Great,Modern,Young,Sexy,Fresh,Able,Professional,Awesome,Attractive,Ambitious,Boundless,Adaptable,Addicting,Awake,Aware,Serious,Resolute,Reflective,Brainy,Bright,Real,Romantic,Capable,Careful,Right,Responsive,Caring,Charming,Quick,Rare,Powerful,Curious,Precious,Ready,Determined,Decisive,Present,Possible,Pleasant,Outstanding,Observant,Entertaining,Exclusive,The Best,Exciting,The One,Gentle,Instinctive,Known,Nimble,Nonstop,Lovely,Magical,Magnificent,Necessary,New,Majestic,Mysterious,Many,Natural,Special,Confident,Sincere,Impressive,Solid,Available,Open,Friendly,Sweet,Superb,Talented,Tangible,Consistent,Pure,Unusual,Suitable,Capable,Huge,Wonderful,Willing,Interesting,Successful,Clear,Different,Happy,Thrilling,Wide-eyed,Vigorous,Vast,Valuable,Unique,Modest
">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="nk-gap-1"></div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				{{--<img src="{{asset("site/img/creaticve-minds.svg")}}" alt="" class="nk-fullpage-image">--}}
				<div class="nk-fullpage-content ">
					<div class="container full-height d-flex flex-column justify-content-center align-items-center">
						<div class="nk-gap-5 d-block d-sm-none"></div>
						<div class="nk-gap-10 mt-45 d-none d-sm-block"></div>
						<h2 class="text-center text-white r-head mt-35">WE <span class="text-main">BELIVE</span> IN</h2>
						<div class="custom-tab nk-tabs bg-dark">
							<ul class="nav nav-tabs nav-tabs-styled bg-transparent text-white text-center" role="tablist">
								<li class="nav-item text-white">
									<a class="nav-link active tab-link" href="#tab-sensitivity" role="tab"
									   data-toggle="tab">SENSITIVITY</a>
								</li>
								<li class="nav-item text-white">
									<a class="nav-link tab-link" href="#tab-reliability" data-target="#tab-reliability" role="tab"
									   data-toggle="tab">RELIABILITY</a>
								</li>
								<li class="nav-item text-white">
									<a class="nav-link tab-link" href="#tab-passion" role="tab" data-toggle="tab">PASSION</a>
								</li>
								<li class="nav-item text-white">
									<a class="nav-link tab-link" href="#tab-versatility" role="tab" data-toggle="tab">VERSATILITY</a>
								</li>
								<li class="nav-item text-white">
									<a class="nav-link tab-link" href="#tab-depth" role="tab" data-toggle="tab">DEPTH</a>
								</li>
							</ul>
							<div class="custom-tab-content tab-content tabs">
								<div role="tabpanel" class="tab-pane fade show active" id="tab-sensitivity">
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center">
											<div class="nk-gap d-none d-sm-block mt-md-7"></div>
											<div class="nk-box-1 text-justify pt-0 pb-0">
												<p class="text-white r-text">
													We believe that sensitivity is a great gift.
													We cultivated this gift to the point
													it has become our most important asset. We have an eye for details, emotional intuition and a
													sixth sense for the consequences of our actions: we notice and feel more. Being sensitive is
													ultimately what allows us to meet costumers real needs and to be winners in business.
												</p>
											</div>
											<div class="nk-gap d-none d-sm-block mt-md-9"></div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-reliability">
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center">
											<div class="nk-gap d-none d-sm-block mt-md-7"></div>
											<div class="nk-box-1 text-justify pt-0">
												<p class="text-white r-text">
													Everyone should have a person they can blindly trust. We want to be that person for your
													company. We take care of your project entirely and handle your requests in a timely manner.
													<span class="text-main">We are the ever-present wingers that lead you to your goal.</span>
												</p>
											</div>
											<div class="nk-gap d-none d-sm-block mt-md-9"></div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade tab-" id="tab-passion">
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center">
											<div class="nk-gap d-none d-sm-block mt-md-7"></div>
											<div class="nk-box-1 text-justify">
												<p class="text-white r-text">
													Passion is everything in life. You can’t excel at anything unless you put your heart and soul
													into it.
													<span class="text-main">We are a company with a soul. A wonderful
                        one.</span> We are passionate about life as well as
													about our job, and put the same amount of intensity from the smallest to the biggest of our
													projects.
												</p>
											</div>
											<div class="nk-gap d-none d-sm-block mt-md-9"></div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-versatility">
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center">
											<div class="nk-gap d-none d-sm-block mt-md-7"></div>
											<div class="nk-box-1 text-justify">
												<p class="text-white r-text">
													Communication is an art that deals with a variety of forms and means. We adapt to
													uncomfortable
													situations and succeed in facing the most different challenges because we are experienced
													problem solvers in our professional life as well as in our private one.
													<span class="text-main">
                          Being positive and life
                        enthusiasts allows us to join the dots and come up with tailored solutions.
                        </span>
												</p>
											</div>
											<div class="nk-gap d-none d-sm-block mt-md-9"></div>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab-depth">
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center">
											<div class="nk-gap d-none d-sm-block mt-md-7"></div>
											<div class="nk-box-1 text-justify">
												<p class="text-white r-text">
													To be remembered in a positive way we must be able to effectively convey messages, feelings,
													leaving a deep mark.
													<span class="text-main">In our work we create truly memorable things and experiences that are
                        willingly shared with friends and family.</span>
												</p>
											</div>
											<div class="nk-gap d-none d-sm-block mt-md-9"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="nk-gap-4"></div>
					</div>
					<div class="nk-gap-1"></div>
					<a class="nk-fullpage-scroll-down text-white d-none d-sm-block" href="#"><span
							class="pe-7s-angle-down"></span></a>
				</div>
			</div>
			<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
				<img src="{{asset("site/img/creaticve-minds.svg")}}" alt="" class="nk-fullpage-image">
				<div class="nk-fullpage-content">
					<div class="nk-gap-6"></div>
					<div
						class="container-fluid max-width height-80 d-flex flex-column justify-content-center align-items-center full-height">
						<div class="row">
							<div class="col-12 pb-md-9">
								<h2 class="text-white text-center nk-fullpage-title r-head">
									CREATIVE <span class="text-main">MINDS.</span>
								</h2>
							</div>
							<div class="nk-gap-2"></div>
						</div>
						<div class="row no-gutters nk-portfolio-list d-flex align-items-center justify-content-center scroll our-team">
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/mr.orique" target="_blank" class="nk-portfolio-item-link"></a>
								<img src="{{asset('uploads/images/team/orkhan.jpg')}}" class="img-fluid person-photo" alt="">
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Orkhan Nabiyev</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Executive Director</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href=" https://www.facebook.com/roberto.ghizzoni.1" target="_blank"
								   class="nk-portfolio-item-link"></a>
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Roberto Ghizzoni</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Social Media Manager</div>
									</div>
								</div>
								<img src="{{asset('uploads/images/team/roberto.jpg')}}" class="img-fluid person-photo" alt="">
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/profile.php?id=100015382842684" target="_blank"
								   class="nk-portfolio-item-link"></a>
								<img src="{{asset('uploads/images/team/giordano.jpg')}}" class="img-fluid person-photo" alt="">
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Giordano Sotiriou</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Multimedia Manager</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/stefmuscaritolo" target="_blank" class="nk-portfolio-item-link"></a>
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Stefano Muscaritolo</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Music Manager</div>
									</div>
								</div>
								<img src="{{asset('uploads/images/team/stefano.jpg')}}" class="img-fluid person-photo" alt="">
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/agnessa.tr" target="_blank" class="nk-portfolio-item-link"></a>
								<img src="{{asset('uploads/images/team/agnessa.jpg')}}" class="img-fluid person-photo" alt="">
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Agnessa Tariverdiyeva</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Graphic Designer</div>
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/profile.php?id=100005118346367" target="_blank"
								   class="nk-portfolio-item-link"></a>
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Khadija Izabakova</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Graphic Designer</div>
									</div>
								</div>
								<img src="{{asset('uploads/images/team/khadija.jpg')}}" class="img-fluid person-photo" alt="">
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/sabina.rza.1" target="_blank" class="nk-portfolio-item-link"></a>
								<img src="{{asset('uploads/images/team/sabina.jpg')}}" class="img-fluid person-photo" alt="">
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Sabina Rzayeva</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Content Developer</div>
									</div>
								</div>
								3
							</div>
							<div class="col-6 col-sm-3 col-md nk-portfolio-item nk-portfolio-item-info-style-6 person">
								<a href="https://www.facebook.com/eeeeeeeka" class="nk-portfolio-item-link"></a>
								<div class="nk-portfolio-item-info text-center">
									<div>
										<h2 class="nk-portfolio-item-title h5 text-white">Ekthiram Mammadkarimov</h2>
										<div class="nk-portfolio-item-category mt-0 text-white">Web Developer</div>
									</div>
								</div>
								<img src="{{asset('uploads/images/team/eka.jpg')}}" class="img-fluid person-photo" alt="">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="nk-gap-2 d-none d-md-block"></div>
								<p class="text-white nk-fullpage-title r-text mt-5">
									We are a group of designers, video makers, writers, alternative thinkers and digital natives who
									<span class="text-main">share the same passion for life.</span>
								</p>
							</div>
						</div>
					</div>
					@include('partials.footer')
				</div>
			</div>
		</div>
	</div>
@endsection
