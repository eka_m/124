<!--
    START: Main Content

    Additional Classes:
        .nk-main-dark
-->
<div class="nk-main bg-transparent">
	<div class="nk-fullpage nk-fullpage-nav-1">
		<div class="nk-fullpage-item nk-fullpage-item-bg-dark">
			<div class="nk-fullpage-content">
				<div class="container text-center" onclick="window.location.href = '/page/who-we-are'">
					<h2 class="h1 text-white welcome-text">Welcome.</h2>
					<h4 class="h1 text-white welcome-page-slogan">You Say, We Hear.</h4>
					<div class="nk-gap-1"></div>
					{{--<a href="{{route('page','who-we-are')}}" class="nk-btn nk-btn-outline nk-btn-color-white al">WHO WE ARE</a>--}}
				</div>
				{{--<a class="nk-fullpage-scroll-down text-white" href="#"><span class="pe-7s-angle-down"></span></a>--}}
			</div>
		</div>
	</div>
</div>
<!-- END: Main Content -->