import {$} from "./_utility";

export function initNiceScroll () {
    const scroll = $('.scroll');
    const bodyscroll = $('.body-nice-scroll');
    if(bodyscroll.length > 0) {
        $("html").niceScroll({
            emulatetouch: bodyscroll.data('cursor-touch') || false,
            cursorborder: "1px solid " + bodyscroll.data('cursor-color') || "#4f0813",
            cursoropacitymax: bodyscroll.data('cursor-opacity') || 1,
            cursorwidth: bodyscroll.data('cursor-width') || "1px",
            cursorborderradius: bodyscroll.data('cursor-border-radius') || "5px",
        });
    }

    if (scroll.length > 0) {
        scroll.each(function (e) {
            const elem = $(this);
            elem.niceScroll({
                emulatetouch: elem.data('cursor-touch') || false,
                cursorborder: "1px solid " + elem.data('cursor-color') || "#4f0813",
                cursoropacitymax: elem.data('cursor-opacity') || 1,
                cursorwidth: elem.data('cursor-width') || "1px",
                cursorborderradius: elem.data('cursor-border-radius') || "5px",
            })
        });
    }
}

