<script src="{{asset('ecms/js/manifest.js')}}"></script>
<script src="{{asset('ecms/js/vendor.js')}}"></script>
<script src="{{asset('ecms/js/app.js')}}"></script>
@yield('js')
<script src="{{asset('ecms/js/custom.js')}}"></script>
{!! Toastr::render() !!}
