@extends('layouts.base')
@section('title', " | ". $project->title)
@section('content')
	<div class="nk-main bg-black portfolio-show" id="portfolio-show">
		<div class="container-fluid max-width p-0">
			<div class="portfolio-cover">
				<img id="project-cover" src="{{asset("uploads".$project->cover)}}" class="img-fluid" alt="">
			</div>
		</div>
		<div class="container-fluid m-auto" style="max-width: 1600px;">
			<div class="portfolio-content">
				<div class="row pt-50">
					<div id="portfolio-main" class="offset-md-1 col-md-5 ">
						<span class="r-desc" id="pr_name_heading"></span>
						<h1 class="text-white" id="project-name">{{$project->title}}</h1>
						<span class="line" id="pr_line"></span>
						<div id="project-description" class="r-text">{!! $project->description !!}</div>
					</div>
					<div id="portfolio-separator" class="offset-md-2 col-md-4">
						<div class="nk-gap-3 d-block d-sm-none"></div>
						<span class="r-desc" id="pr_client_heading"></span>
						<p class="r-text mt-1" id="project-client">{{$project->client}}</p>
						<span class="r-desc" id="pr_date_heading"></span>
						<p class="r-text mt-1" id="project-date">{{$project->date}}</p>
						<span class="r-desc" id="pr_skills_heading"></span>
						<div class="r-text" id="project-skills">{!! $project->skills !!}</div>
					</div>
				</div>
				<div class="nk-gap-6"></div>
			</div>
		</div>
	</div>
	@include('partials.footer')
@endsection