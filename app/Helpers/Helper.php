<?php

function addAttributeIf($attr, $obj, $key, $json = false)
{
    if (isset($obj[$key]) && $obj[$key] != null) {
        $val = $obj[$key];
        if ($json) {
            $val = json_encode($obj[$key]);
        }
        return $attr . "=$val";
    }
    return "";
}

function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function svg($path, $class = '')
{

    $svg = new \DOMDocument();
    $svg->load(public_path($path), LIBXML_NOWARNING);
    $svg->documentElement->setAttribute("class", $class);
    $output = $svg->saveXML($svg->documentElement);
    return $output;
}

function makeCollection($arr) {
    $result = collect([]);
    if(!empty($arr)){
        foreach ($arr as $key => $value) {
            $result->put($key, $value);
        }
    }
    return $result;
}
