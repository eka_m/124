<?php namespace App\Models\Ecms;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Mix extends TheCollectionsBase implements Sortable {

    protected $table = 'mix';

    use SortableTrait;
    public $sortable = [
            'order_column_name' => 'position',
            'sort_when_creating' => true,
        ];

    protected $fillable = ['slug','position','collection_id','active','name','video','image','category'];

    protected $rules = ['name' => 'required'];

    protected $appends = ['display'];

    public function collection() {
        return $this->belongsTo('App\Models\Ecms\Collection');
    }
}
