<li class="menu-item {{!$item->childrens->isEmpty() ? 'has-children' : ''}}">
		<a class="cd-nav-item animsition-link" href="{{route('page',$item->slug)}}">
			{{$item->name}}
			{{--<h3>Laser-Sculpt Lipo™</h3>--}}
			<img src="{{asset('/uploads/images/laser-sculpting.jpg')}}" alt="Product Image">
			@if (!$item->childrens->isEmpty())
				<ul class="cd-nav-gallery is-hidden">
					@foreach ($item->childrens as $child)
						@include('partials.tree-nav.nav-item', ['item'  => $child])
					@endforeach
				</ul>
			@endif
		</a>
</li>
