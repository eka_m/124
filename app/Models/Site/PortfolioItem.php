<?php

namespace App\Models\Site;

use App\Traits\ImageManager;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class PortfolioItem extends Model
{

    use ImageManager;
    protected $table = 'portfolio-items';
    use HasTranslations;

    public $translatable = ['name'];

    public function getContentAttribute($value)
    {
        return json_decode($value);
//        if (!empty($value->image)) {
//            $value->image = $this->makeImage($value->image);
//        }
//        if (isset($value->params) && $value->params) {
//            $value->params = $this->keyvalue($value->params);
//        }
//        return $value;
    }


    public function getCategoryAttribute($value)
    {
        return str_slug($value);
    }

    public function portfolio()
    {
        return $this->belongsTo(Portfolio::class);
    }

    public function keyvalue($params)
    {
        $result = collect([]);
        foreach ($params as $param) {
            if ($param->multilang) {
                if (isset($param->value->{\LaravelLocalization::getCurrentLocale()})) {
                    $result->{$param->key} = $param->value->{\LaravelLocalization::getCurrentLocale()};
                } else {
                    $result->{$param->key} = isset($param->value->{\Setting::get('languages.default')}) ? $param->value->{\Setting::get('languages.default')} : $param->value;
                }
            } else {
                $result->{$param->key} = $param->value;
            }
        }
        return $result;
    }
}
