@section('js')
  <!-- START: Scripts -->
  <script src="{{asset('plugins/jquery/dist/jquery.min.js')}}"></script>

  <!-- Object Fit Polyfill -->
  <script src="{{asset('plugins/object-fit-images/dist/ofi.min.js')}}"></script>

  <!-- ImagesLoaded -->
  <script src="{{asset('plugins/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>

  <!-- GSAP -->
  <script src="{{asset('plugins/gsap/src/minified/TweenMax.min.js')}}"></script>
  <script src="{{asset('plugins/gsap/src/minified/plugins/TextPlugin.min.js')}}"></script>
  <script src="{{asset('plugins/gsap/src/minified/plugins/ScrollToPlugin.min.js')}}"></script>

  <!-- Popper -->
  {{--<script src="{{asset('plugins/popper.js/dist/umd/popper.min.js')}}"></script>--}}

  <!-- Bootstrap -->
  <script src="{{asset('plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>

  <!-- Sticky Kit -->
  <script src="{{asset('plugins/sticky-kit/dist/sticky-kit.min.js')}}"></script>


  {{--<!-- JustifiedGallery -->--}}
  {{--<script src="{{asset('plugins/justifiedGallery/dist/js/jquery.justifiedGallery.min.js')}}"></script>--}}

  <!-- Jquery Validation -->
  {{--<script src="{{asset('plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>--}}


  <!-- NanoSroller -->
  {{--<script src="{{asset('plugins/nanoscroller/bin/javascripts/jquery.nanoscroller.js')}}"></script>--}}

  <!-- Keymaster -->
  {{--<script src="{{asset('plugins/keymaster/keymaster.js')}}"></script>--}}

  <script src="{{asset('plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

  {{--<script src="{{asset('plugins/smoothscroll/smoothScroll.min.js')}}"></script>--}}
@show
<script type="text/javascript" src="{{asset('site/js/main.min.js')}}"></script>
<script type="text/javascript" src="{{asset('site/js/custom.min.js')}}"></script>
