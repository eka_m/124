<?php

namespace App\Http\Controllers\Site;



class HomeController extends BaseController
{

    public function show()
    {
        return view('site::pages.home.index', compact(''));
    }

    public function loading() {
        return view('site::partials.loading');
    }
}
