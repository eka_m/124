<nav id="sidebar" class="sidebar-wrapper">
  <div class="sidebar-content">
    <div class="sidebar-brand">
      <a class="display-4" href="#">ECMS</a>
      <div id="close-sidebar">
        <i class="fas fa-times"></i>
      </div>
    </div>
    <div class="sidebar-header">
      <div class="user-pic">
        {{-- <img class="img-responsive img-rounded" src="assets/img/user.jpg" alt="User picture"> --}} {!! Avatar::create(auth('ecms')->user()->name)->toSvg()
				!!}
      </div>
      <div class="user-info">
				<span class="user-name">
									<strong>{{auth('ecms')->user()->name}}</strong>
							</span> {{-- <span class="user-role">Administrator</span> --}}
        <span class="user-status">
									<i class="fa fa-circle"></i>
									<span>Online</span>
				</span>
      </div>
    </div>
    <!-- sidebar-header  -->
  {{--
  <div class="sidebar-search">
    <div>
      <div class="input-group">
        <input type="text" class="form-control search-menu" placeholder="Search...">
        <div class="input-group-append">
          <span class="input-group-text">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </span>
        </div>
      </div>
    </div>
  </div> --}}
  <!-- sidebar-search  -->
    <div class="sidebar-menu">
      <ul>
        <li class="header-menu">
          <span>Collections</span>
        </li>
        @foreach ($collections as $item)
          <li>
            <a href="{{route('ecms.collection.index',$item->slug)}}"
               class="nav-link {{request()->is('admin/collection/'.$item->name) ? 'active' : ''}}">
              @if($item->icon)
                <i class="{{$item->icon}}" aria-hidden="true"></i>
              @endif
              <span>{{$item->dname}}</span>
            </a>
          </li>
        @endforeach
        <li class="header-menu">
          <span>General</span>
        </li>
        {{--
        <li class="sidebar-dropdown">
          <a href="#">
                      <i class="fa fa-tachometer-alt"></i>
                      <span>Dashboard</span>
                      <span class="badge badge-pill badge-danger">New</span>
                  </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="#">Dashboard 1
                                  <span class="badge badge-pill badge-success">Pro</span>
                              </a>
              </li>
              <li>
                <a href="#">Dashboard 2</a>
              </li>
              <li>
                <a href="#">Dashboard 3</a>
              </li>
            </ul>
          </div>
        </li> --}}
        <li>
          <a href="{{route('ecms.pages.index')}}" class="{{request()->has('pages') ? 'active' : ''}}">
            <i class="fas fa-file-alt"></i> <span>Pages</span></a>
        </li>
        {{--<li>--}}
          {{--<a href="{{route('ecms.gallery.index')}}" class="{{request()->has('gallery') ? 'active' : ''}}">--}}
            {{--<i class="fas fa-images" aria-hidden="true"></i>--}}
            {{--<span>Gallery</span>--}}
          {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
          {{--<a href="{{route('ecms.portfolio.index')}}" class="{{request()->has('portfolio') ? 'active' : ''}}">--}}
            {{--<i class="fas fa-images" aria-hidden="true"></i>--}}
            {{--<span>Portfolio</span>--}}
          {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
          {{--<a href="{{route('ecms.partials.index')}}" class="{{request()->has('partials') ? 'active' : ''}}">--}}
            {{--<i class="fas fa-puzzle-piece" aria-hidden="true"></i>--}}
            {{--<span>Partials</span>--}}
          {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
          {{--<a href="{{route('ecms.collections.index')}}"--}}
             {{--class="nav-link {{request()->is('admin/collections') ? 'active' : ''}}">--}}
            {{--<i class="fas fa-layer-group" aria-hidden="true"></i>--}}
            {{--<span>Collections</span>--}}
          {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
          {{--<a href="{{route('ecms.locales.index')}}"--}}
             {{--class="nav-link {{request()->is('admin/localization') ? 'active' : ''}}">--}}
            {{--<i class="fas fa-globe" aria-hidden="true"></i>--}}
            {{--<span>Localization</span>--}}
          {{--</a>--}}
        {{--</li>--}}
      </ul>
    </div>
    <!-- sidebar-menu  -->
  </div>
  <!-- sidebar-content  -->
  <div class="sidebar-footer">
    {{--
    <div class="dropdown">
      <a href="#" class="" id="dropdownMenuNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-bell"></i>
              <span class="badge badge-pill badge-warning notification">3</span>
          </a>
      <div class="dropdown-menu notifications" aria-labelledby="dropdownMenuMessage">
        <div class="notifications-header">
          <i class="fa fa-bell"></i> Notifications
        </div>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
          <div class="notification-content">
            <div class="icon">
              <i class="fas fa-check text-success border border-success"></i>
            </div>
            <div class="content">
              <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
              <div class="notification-time">
                6 minutes ago
              </div>
            </div>
          </div>
        </a>
        <a class="dropdown-item" href="#">
          <div class="notification-content">
            <div class="icon">
              <i class="fas fa-exclamation text-info border border-info"></i>
            </div>
            <div class="content">
              <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
              <div class="notification-time">
                Today
              </div>
            </div>
          </div>
        </a>
        <a class="dropdown-item" href="#">
          <div class="notification-content">
            <div class="icon">
              <i class="fas fa-exclamation-triangle text-warning border border-warning"></i>
            </div>
            <div class="content">
              <div class="notification-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
              <div class="notification-time">
                Yesterday
              </div>
            </div>
          </div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item text-center" href="#">View all notifications</a>
      </div>
    </div>
    <div class="dropdown">
      <a href="#" class="" id="dropdownMenuMessage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-envelope"></i>
              <span class="badge badge-pill badge-success notification">7</span>
          </a>
      <div class="dropdown-menu messages" aria-labelledby="dropdownMenuMessage">
        <div class="messages-header">
          <i class="fa fa-envelope"></i> Messages
        </div>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
          <div class="message-content">
            <div class="pic">
              <img src="assets/img/user.jpg" alt="">
            </div>
            <div class="content">
              <div class="message-title">
                <strong> Jhon doe</strong>
              </div>
              <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
            </div>
          </div>

        </a>
        <a class="dropdown-item" href="#">
          <div class="message-content">
            <div class="pic">
              <img src="assets/img/user.jpg" alt="">
            </div>
            <div class="content">
              <div class="message-title">
                <strong> Jhon doe</strong>
              </div>
              <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
            </div>
          </div>

        </a>
        <a class="dropdown-item" href="#">
          <div class="message-content">
            <div class="pic">
              <img src="assets/img/user.jpg" alt="">
            </div>
            <div class="content">
              <div class="message-title">
                <strong> Jhon doe</strong>
              </div>
              <div class="message-detail">Lorem ipsum dolor sit amet consectetur adipisicing elit. In totam explicabo</div>
            </div>
          </div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item text-center" href="#">View all messages</a>

      </div>
    </div>
    <div class="dropdown">
      <a href="#" class="" id="dropdownMenuMessage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-cog"></i>
              <span class="badge-sonar"></span>
          </a>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuMessage">
        <a class="dropdown-item" href="#">My profile</a>
        <a class="dropdown-item" href="#">Help</a>
        <a class="dropdown-item" href="#">Setting</a>
      </div>
    </div> --}}
    <div>
      <a title="Exit" href="{{route('ecms.logout')}}">
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </div>
</nav>
