<div id="{{$item['id']}}"
     class="nk-isotope-item-custom  p-5 col-md-3 col-sm-6 col-12 gallery-item {{$item['category']}}">
	<div class="nk-portfolio-item nk-portfolio-item-gallery">
		@if(!empty($item['image']) && empty($item['video']))
			<a href="{!! $item['image'] !!}" data-fancybox="gallery" data-caption="{{$item['name']}}"
			   class="nk-portfolio-item-link nk-gallery-item"></a>
			<div class="nk-portfolio-item-video-cover">
				<div class="video-play">
					<i class="pe-7s-expand1 ml-2"></i>
				</div>
				<img class="nk-portfolio-item-image" src="{!! $item['image'] !!}" alt="">
			</div>
		@elseif(!empty(trim($item['video'])))
			<a href="https://www.youtube.com/watch?v={{$item['video']}}" data-fancybox="gallery"
			   data-caption="{{$item['name']}}"
			   class="nk-portfolio-item-link nk-gallery-item"></a>
			<div class="nk-portfolio-item-video-cover">
				<div class="video-play">
					<i class="pe-7s-play ml-5"></i>
				</div>
				<img class="nk-portfolio-item-image" src="{!! $item['image'] !!}" alt="">
			</div>
		@endif
		<div class="nk-portfolio-item-info"></div>
	</div>
</div>
