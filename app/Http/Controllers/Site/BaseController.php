<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public function __construct()
    {
        $pages = Page::with('childrens')->orderBy('position', 'ASC')->where(['status' => 'on'])->get();
        View::share('pages', $pages);
    }
}
