<?php namespace App\Models\Ecms;

use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Works extends TheCollectionsBase implements Sortable  {

    protected $table = 'works';

	use SortableTrait;
	public $sortable = [
		'order_column_name' => 'position',
		'sort_when_creating' => true,
	];

    protected $fillable = ['slug','position','collection_id','active','title','description','image','cover','client','date','skills'];

    protected $rules = ['title' => 'required'];

    protected $appends = ['display'];

    public function collection() {
        return $this->belongsTo('App\Models\Ecms\Collection');
    }
}
