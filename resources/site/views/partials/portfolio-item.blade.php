<div class="col-6 col-md-4 col-lg-3 p-5">
	<div class="nk-portfolio-item nk-portfolio-item-info-style-6 nk-portfolio-item-cs gallery-item"
	     onclick="window.location.href = '{{route('showProject', $project->slug)}}'">
		<img src="{{asset('uploads'. $project->image)}}" class="img-fluid gallery-item-image" alt="">
		<div class="nk-portfolio-item-info text-center ">
			<a href="{{route('showProject', $project->slug)}}" class="nk-portfolio-item-play-cs link-reset">
				<i class="pe-7s-play"></i></a>
			<h2 class="nk-portfolio-item-title h5 text-white mt-10">{{$project->title}}</h2>
		</div>
	</div>
</div>