<?php namespace App\Models\Ecms;

class Slider extends TheCollectionsBase  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slider';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['slug','position','collection_id','active','image','title','desc'];

    protected $rules = [];

    protected $appends = ['display'];

    public function collection() {
        return $this->belongsTo('App\Models\Ecms\Collection');
    }
}
