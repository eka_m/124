<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Partial extends Model
{
    use HasTranslations;

    protected $casts = [
        'content' => 'array',
    ];

    protected $types = ['text', 'textarea', 'editor', 'cropableimage', 'image', 'file'];

    public $translatable = ['content'];

    public function getKeyvalueAttribute($value)
    {
        $result = collect([]);
        foreach (json_decode($value) as $val) {
            if ($val->type == 'cropableimage') {
                $result->{$val->key} = $this->makeImage($val->value);
            } else {
								if (isset($val->value->{\LaravelLocalization::getCurrentLocale()})) {
									$result->{$val->key} = $val->value->{\LaravelLocalization::getCurrentLocale()};
							} else {
									$result->{$val->key} = isset($val->value->{\Setting::get('languages.default')}) ? $val->value->{\Setting::get('languages.default')} : $val->value;
							}
            }
        }
        return $result;
    }

    public function get($name)
    {
        return property_exists($this->keyvalue, $name) ? $this->keyvalue->{$name} : 'No property:' . $name;
    }

    protected function makeImage($image)
    {
        $img = \Image::make(public_path() . $image->path);
        $img->crop(round($image->params->width), round($image->params->height), round($image->params->x), round($image->params->y));
        return $img->encode('data-url');
    }
}
