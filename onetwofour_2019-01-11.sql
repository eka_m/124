# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.24-0ubuntu0.16.04.1)
# Database: onetwofour
# Generation Time: 2019-01-11 08:54:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_password_resets`;

CREATE TABLE `admin_password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin','memmedkerimov@gmail.com',NULL,'$2y$10$EfTFjiWkvfhHwXEUe4QMD.eXtVAmk0oPtzymTSeRjRR3JsdJR149m',NULL,NULL,NULL);

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table collections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collections`;

CREATE TABLE `collections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheme` longtext COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table galleries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `items` longtext COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2017_07_31_123420_create_locales_table',1),
	(2,'2018_11_14_120743_create_collections_table',2),
	(3,'2018_11_22_181116_create_galleries_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `cover` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `position` smallint(6) NOT NULL DEFAULT '9999',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `name`, `slug`, `content`, `image`, `cover`, `keywords`, `description`, `params`, `status`, `position`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,0,'{\"en\":\"About\"}','about',NULL,NULL,NULL,NULL,NULL,NULL,'on',0,'2018-11-25 19:33:57','2018-11-14 08:47:52','2018-11-25 19:33:57'),
	(2,0,'{\"en\":\"Procedures\"}','procedures',NULL,NULL,NULL,NULL,NULL,NULL,'on',1,'2018-11-25 19:33:57','2018-11-14 08:48:10','2018-11-25 19:33:57'),
	(3,0,'{\"en\":\"ULTRASONICS™\"}','ultrasonics','{\"en\":\"<p><br><br><br></p><img class=\\\"align-left\\\" style=\\\"margin-right: 20px;\\\" src=\\\"/uploads/images/_1416357579.png\\\" alt=\\\"\\\" width=\\\"141\\\" height=\\\"135\\\" data-mce-src=\\\"/uploads/images/_1416357579.png\\\" data-mce-style=\\\"margin-right: 20px;\\\"><p style=\\\"padding-left: 30px; text-align: left;\\\" data-mce-style=\\\"padding-left: 30px; text-align: left;\\\"><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">\\\"Now there is a revolutionary fat busting procedure that literally melts your fat away, without ever having to go under the knife\\\" - </span><strong><em><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Dr. Mehmet Oz, The Dr. Oz Show</span></em><br><br><br><br><br></strong></p><hr><h4 style=\\\"padding-left: 30px; text-align: center;\\\" data-mce-style=\\\"padding-left: 30px; text-align: center;\\\"><br><br><br><span style=\\\"color: #800080;\\\" data-mce-style=\\\"color: #800080;\\\"><span style=\\\"color: #993300;\\\" data-mce-style=\\\"color: #993300;\\\">Measurable Results Are GUARANTEED On Your Very First Appointment</span><br><br>&nbsp;<br><br></span></h4><p data-mce-style=\\\"text-align: center;\\\" style=\\\"text-align: center;\\\"><strong><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">​Tired of having trouble eliminating those stubborn areas</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">of fat and cellulite that resist diet and exercise?</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">&nbsp;This New Procedure offers a Pain-Free solution that Targets the Exact Areas you want to&nbsp; Reshape </span><em><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Instantly!</span><br><br><br></em></strong></p><p><br></p><gallery class=\\\"mceNonEditable\\\" id=\\\"2\\\" style=\\\"width: 350px; height: 200px; margin: 20px 0px 20px 20px; padding: 5px; text-align: center; background: rgb(0, 150, 243); color: rgb(255, 255, 255); display: flex; align-items: center; justify-content: center; float: right;\\\" params=\\\"{&quot;width&quot;:&quot;350px&quot;,&quot;float&quot;:&quot;right&quot;,&quot;margin&quot;:&quot;20px 0px 20px 20px&quot;}\\\" data-params=\\\"{\\\" data-mce-style=\\\"width: 100%; height: 200px; margin: 10px auto; padding: 5px; text-align: center; background: #0096F3; color: #ffffff; display: flex; align-items: center; justify-content: center;\\\" contenteditable=\\\"false\\\">Gallery:: <a style=\\\"color: #ffffff;\\\" href=\\\"/admin/gallery/2/edit\\\" data-mce-href=\\\"/admin/gallery/2/edit\\\" data-mce-style=\\\"color: #ffffff;\\\">ULTRASONICS™</a></gallery><h5><br><br><br>Tired of having trouble eliminating <br>those stubborn areas of fat and cellulite <br>that resist diet and exercise? <br>This New Procedure offers a Pain-Free solution that Targets the Exact Areas you want to Reshape Instantly<br><br><br><br><br><br><br><br></h5><hr><h4><br><br><br><span style=\\\"color: #993300;\\\" data-mce-style=\\\"color: #993300;\\\">JUST IMAGINE ACHIEVING THE SAME RESULTS AS A GRUELING ​</span><br><span style=\\\"color: #993300;\\\" data-mce-style=\\\"color: #993300;\\\">90 DAY DIET AND EXERCISE BOOT CAMP</span><br><span style=\\\"color: #993300;\\\" data-mce-style=\\\"color: #993300;\\\">WHILE RELAXING IN COMFORT</span><br><br><br><br></h4><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Treats: Waist, Back, Thighs, Hips, Arms &amp; Calves</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Hassle Free Patient Results Guarantee&nbsp;<br></span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Non-Surgical and Non-Invasive Instantly Removes Targeted Fat and Cellulite<br></span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Pain-Free - Relaxing 45 Minute Procedure<br></span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• See 2 to 3 Inches of Shapely Reduction Per Area<br></span></p></div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Allows Immediate Return to Normal Activities</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Beautifully Reduces &amp; Reshapes Targeted Areas</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Provides Silky Smooth, Lifted Appearance&nbsp;</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Offers Stunning, Measurable, Real-time Results&nbsp;</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Permanently Eliminates Targeted Fat Cells</span></p><p style=\\\"text-align: left; padding-left: 30px;\\\" data-mce-style=\\\"text-align: left; padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Proven Safe Worldwide and Convenient</span></p></div></div></section><section class=\\\"section  mceNonEditable\\\" contenteditable=\\\"false\\\"></section><div><span style=\\\"font-size: 12pt;\\\" data-mce-style=\\\"font-size: 12pt;\\\"><br><br></span></div><br><div style=\\\"text-align: left;\\\" data-mce-style=\\\"text-align: left;\\\"><br></div>\"}','{\"path\":\"/uploads/images/laser-sculpting3.jpg\",\"params\":{\"x\":38.818643162393165,\"y\":3.157451923076923,\"width\":1202.7142094017095,\"height\":902.035657051282,\"type\":\"initial\",\"rotation\":null}}','{\"path\":\"/uploads/images/d9e954e2eaa1319518c3b308fb7fad3d.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1919,\"height\":612,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',9999,'2018-11-25 19:34:03','2018-11-14 08:48:40','2018-11-25 19:34:03'),
	(4,0,'{\"en\":\"LASER-SCULPT LIPO™\"}','laser-sculpt-lipo','{\"en\":\"<div style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\"><br>\\\"It\'s a new cutting edge procedure, that studies show can melt inches off your waist, hips and thighs\\\"</span></div><div style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><em><strong><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">-&nbsp;&nbsp;Dr. Marie Savard, M.D. -&nbsp;ABC News Medical Contributor</span></strong></em></div><div style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><br></div><hr><h4 style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><br><br><span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\">&nbsp;•Target Fat Loss with Advanced Laser Body Shaping•</span><br><br><br></h4><gallery class=\\\"mceNonEditable\\\" id=\\\"4\\\" style=\\\"width: 350px; height: 200px; margin: 20px 0px 20px 20px; padding: 5px; text-align: center; background: rgb(0, 150, 243); color: rgb(255, 255, 255); display: flex; align-items: center; justify-content: center; float: right;\\\" params=\\\"{&quot;width&quot;:&quot;350px&quot;,&quot;float&quot;:&quot;right&quot;,&quot;margin&quot;:&quot;20px 0px 20px 20px&quot;}\\\" data-mce-style=\\\"width: 100%; height: 200px; margin: 10px auto; padding: 5px; text-align: center; background: #0096F3; color: #ffffff; display: flex; align-items: center; justify-content: center;\\\" contenteditable=\\\"false\\\">Gallery:: <a style=\\\"color: #ffffff;\\\" href=\\\"/admin/gallery/4/edit\\\" data-mce-href=\\\"/admin/gallery/4/edit\\\" data-mce-style=\\\"color: #ffffff;\\\">LASER-SCULPT LIPO™</a></gallery><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><br><span style=\\\"font-size: 16pt;\\\" data-mce-style=\\\"font-size: 16pt;\\\">Revolutionary New Soft Laser Technology&nbsp; Reduces Pockets of Unwanted Fat Painlessly - That Diet and Exercise Fail to Achieve.&nbsp;</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\"><span style=\\\"font-size: 16pt;\\\" data-mce-style=\\\"font-size: 16pt;\\\">Soft Laser Light Breaks Down Those Stubborn Fat Cells to Give You a Slimmer More Athletic Appearance as You Relax in&nbsp; Our Beautiful Spa Environmen</span><br><br><br></span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><br><br><br></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br></p><hr><h4 style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><br><br><br><span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\">Just Imagine a Slimmer-Firmer More Athletic Appearance<br>That Will Turn Heads!</span></h4><p><span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\">&nbsp;</span></p><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Safe, Proven Worldwide and Convenient</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Affordable Options; Less Than $95 a month*<br></span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Non-Surgical and Non-Invasive - Pain-Free<br></span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Instantly Removes Targeted Fat and Cellulite<br></span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Pain-Free - Relaxing 45 Minute Procedure<br></span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• See 2 to 3 Inches of Shapely Reduction Per Area</span></p></div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Allows Immediate Return to Normal Activities</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Beautifully Reduces &amp; Reshapes Targeted Areas</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Provides Silky Smooth, Lifted Appearance</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Offers Stunning, Measurable, Real-time Results</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Permanently Eliminates Targeted Fat Cells</span></p><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Treats: Under Chin, Waist, Back, Thighs, Hips, Arms &amp; Calves</span></p></div></div></section><p><span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\"><br><br><br><br><br></span></p><hr><p style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\"><br>*&nbsp;I</span>ndividual Treatments are Offered at $245.00<span style=\\\"color: #003366;\\\" data-mce-style=\\\"color: #003366;\\\"><br><br></span></p>\"}','{\"path\":\"/uploads/images/do-stretch-marks-go-away-if-you-lose-weight-1.jpg\",\"params\":{\"x\":47.97449252136752,\"y\":4.637486645299188,\"width\":641.8501602564103,\"height\":481.3876201923077,\"type\":\"initial\",\"rotation\":null}}','{\"path\":\"/uploads/images/lsl.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1919,\"height\":612,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',9999,'2018-11-25 19:34:03','2018-11-14 08:49:05','2018-11-25 19:34:03'),
	(5,0,'{\"en\":\"THERMA-LIFT™\"}','therma-lift','{\"en\":\"<p style=\\\"padding-left: 60px;\\\" data-mce-style=\\\"padding-left: 60px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\"><br>\\\"It is perfect for patients who have no desire for surgery, but are looking for reductions in problem areas. There may be some moderate tingling, but patients don\'t find this to be painful,\\\" -&nbsp;&nbsp;<em><strong>Rod J. Rohrich, MD,&nbsp;former&nbsp;President of the American Society of Plastic Surgeons.</strong></em></span><br><span style=\\\"font-size: 12pt;\\\" data-mce-style=\\\"font-size: 12pt;\\\">&nbsp;</span></p><hr><h4 style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><br>&nbsp;<br><br><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\">REGAIN YOUR VIBRANT YOUTHFUL APPEARANCE&nbsp;</span><br><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\">WITH NON-INVASIVE FACELIFT &amp; FULL BODY SKIN TIGHTENING</span><br><br></h4><p><br></p><gallery class=\\\"mceNonEditable\\\" id=\\\"5\\\" style=\\\"width: 350px; height: 200px; margin: 20px 0px 20px 20px; padding: 5px; text-align: center; background: rgb(0, 150, 243); color: rgb(255, 255, 255); display: flex; align-items: center; justify-content: center; float: right;\\\" params=\\\"{&quot;width&quot;:&quot;350px&quot;,&quot;float&quot;:&quot;right&quot;,&quot;margin&quot;:&quot;20px 0px 20px 20px&quot;}\\\" data-mce-style=\\\"width: 100%; height: 200px; margin: 10px auto; padding: 5px; text-align: center; background: #0096F3; color: #ffffff; display: flex; align-items: center; justify-content: center;\\\" contenteditable=\\\"false\\\">Gallery:: <a style=\\\"color: #ffffff;\\\" href=\\\"/admin/gallery/5/edit\\\" data-mce-href=\\\"/admin/gallery/5/edit\\\" data-mce-style=\\\"color: #ffffff;\\\">THERMA-LIFT™</a></gallery><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><br><span style=\\\"font-size: 16pt;\\\" data-mce-style=\\\"font-size: 16pt;\\\">​<br>Defy Age and Gravity! Eliminate Sagging &amp; Loose Skin any where on your Body including your Face &amp; Neck painlessly, with State of The Art Radio Frequency Therapy.</span><br><span style=\\\"font-size: 16pt;\\\" data-mce-style=\\\"font-size: 16pt;\\\">Now you can roll back Nature\'s Clock&nbsp;with the same secret that Celebrities &amp; Hollywood \\\"A\\\" Lister\'s use to stay Red Carpet ready<br><br><br><br><br><br><br></span></p><p><br></p><hr><h4><br><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><br><br>Just Imagine, Being Able To Look Ten Years Younger&nbsp;<br>Pain Free in Just Minutes</span>&nbsp;<br><br><br></h4><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"padding-left: 60px; text-align: left;\\\" data-mce-style=\\\"padding-left: 60px; text-align: left;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Totally Painless Facelift Procedure</span></p><p style=\\\"padding-left: 60px; text-align: left;\\\" data-mce-style=\\\"padding-left: 60px; text-align: left;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Last Up To One Year</span></p><p style=\\\"padding-left: 60px; text-align: left;\\\" data-mce-style=\\\"padding-left: 60px; text-align: left;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Replaces Painful Fillers &amp; Injections</span></p><p style=\\\"padding-left: 60px; text-align: left;\\\" data-mce-style=\\\"padding-left: 60px; text-align: left;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Perfect For All Ethnicities &amp; Skin Tones</span></p><p style=\\\"padding-left: 60px; text-align: left;\\\" data-mce-style=\\\"padding-left: 60px; text-align: left;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Treats All Five Layers of The Skin</span></p></div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Builds Up Your Skins Collagen &amp; Elastin&nbsp;&nbsp;</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Replaces Invasive Surgical Face Lifts</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• No Down Time</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Instant Results Anywhere on Your Body</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">• Increases Skin Elasticity &amp; Tone</span></p></div></div></section><p><br><br><br><br></p><hr><p style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><br>Individual Treatments are Offered at $350.00</p>\"}','{\"path\":\"/uploads/images/therma_lift_face_procedure_grande.jpg\",\"params\":{\"x\":18.370437007443083,\"y\":0.2950415936952715,\"width\":544.9399445417396,\"height\":408.7049584063047,\"type\":\"initial\",\"rotation\":null}}','{\"path\":\"/uploads/images/therma-lift-cover.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1920,\"height\":673,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',9999,'2018-11-25 19:34:03','2018-11-14 08:49:18','2018-11-25 19:34:03'),
	(6,0,'{\"en\":\"Financing\"}','financing','{\"en\":\"<div style=\\\"padding-left: 60px;\\\" data-mce-style=\\\"padding-left: 60px;\\\"><br></div><div style=\\\"padding-left: 30px;\\\" data-mce-style=\\\"padding-left: 30px;\\\"><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\"><br><br>\\\"I rarely say this, but this is the real deal.&nbsp; It melts fat.&nbsp; Three to four treatments will produce 2 to 3 inches of loss in your waist line.\\\" -&nbsp;<strong>Dr. Jennifer Walden, MD,&nbsp;FACS Plastic Surgeon<br><br><br></strong></span></div><br><br><hr><h3 style=\\\"padding-left: 30px; text-align: center;\\\" data-mce-style=\\\"padding-left: 30px; text-align: center;\\\"><br><br><br><img class=\\\"\\\" style=\\\"float: left;\\\" src=\\\"/uploads/images/fn.png\\\" alt=\\\"\\\" width=\\\"323\\\" height=\\\"661\\\" data-mce-src=\\\"/uploads/images/fn.png\\\" data-mce-style=\\\"float: left;\\\"><strong><br><br><br><br><br><br><br><br>&nbsp;&nbsp;<span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\">Why Wait Any Longer?<br></span></strong><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong>You Deserve To Be In <br>Amazing <br>Shape Today!</strong></span><br><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong>Just Imagine Having That Perfect Figure <br>You\'ve Always Dreamed Of.</strong></span></h3><p><span style=\\\"color: #333399;\\\" data-mce-style=\\\"color: #333399;\\\"><strong><br><br><br><br><br><br><br><br><br><br><br><br><br><br></strong><br></span></p><h4 class=\\\"paragraph\\\"><strong><span style=\\\"color: #2a2a2a; font-size: x-large;\\\" data-mce-style=\\\"color: #2a2a2a; font-size: x-large;\\\"><br></span></strong><br><strong><br></strong></h4><p style=\\\"padding-left: 180px;\\\" data-mce-style=\\\"padding-left: 180px;\\\"><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\"><br><br><br><br></span></strong></p><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><h4>LOW PAYMENTS FROM LESS THAN&nbsp;$97&nbsp;PER MONTH<br><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\">(OAC)<br><img class=\\\"img-fluid\\\" src=\\\"/uploads/images/lendingtree-1_orig.jpg\\\" alt=\\\"\\\" width=\\\"375\\\" height=\\\"154\\\" data-mce-src=\\\"/uploads/images/lendingtree-1_orig.jpg\\\"><br><br><img class=\\\"img-fluid\\\" src=\\\"/uploads/images/lending-usa-logo_2.jpg\\\" alt=\\\"\\\" width=\\\"384\\\" height=\\\"147\\\" data-mce-src=\\\"/uploads/images/lending-usa-logo_2.jpg\\\"><br></span></strong></h4></div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><img class=\\\"img-fluid\\\" style=\\\"float: right;\\\" src=\\\"/uploads/images/ripped-six-pack.png\\\" alt=\\\"\\\" width=\\\"357\\\" height=\\\"495\\\" data-mce-src=\\\"/uploads/images/ripped-six-pack.png\\\" data-mce-style=\\\"float: right;\\\"></div></div></section><p style=\\\"padding-left: 180px;\\\" data-mce-style=\\\"padding-left: 180px;\\\"><strong><span style=\\\"font-size: 11pt;\\\" data-mce-style=\\\"font-size: 11pt;\\\"><br><br><br></span></strong></p><h4 style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><br>• NO CREDIT CHECK FINANCING AVAILABLE FOR SPECIAL NEEDS&nbsp;•</h4>\"}',NULL,'{\"path\":\"/uploads/images/financing.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1920,\"height\":673,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',2,'2018-11-25 19:33:57','2018-11-14 08:49:28','2018-11-25 19:33:57'),
	(7,0,'{\"en\":\"Contacts\"}','contacts','{\"en\":\"<h2><br><br>LET\'S GET STARTED ON HELPING YOU LOOK AND FEEL GREAT TODAY!</h2><h3><strong><br>361-334-9999</strong></h3><h3 data-mce-style=\\\"text-align: center;\\\" style=\\\"text-align: center;\\\"><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">We Love Making Your Dreams Come True!</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Start Today With A&nbsp;FREE</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">Beautiful Body Makeover Consultation</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">With Our Experts</span><br><span style=\\\"font-size: 14pt;\\\" data-mce-style=\\\"font-size: 14pt;\\\">\\\"Your Beauty is Our Business\\\"</span><strong><br><br></strong></h3><div style=\\\"text-align: left;\\\" data-mce-style=\\\"text-align: left;\\\"><section class=\\\"section white-section mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"container mceNonEditable\\\" contenteditable=\\\"false\\\"><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\" style=\\\"text-align: center;\\\" data-mce-style=\\\"text-align: center;\\\"><img src=\\\"/uploads/images/_1416265748.png\\\" width=\\\"259\\\" height=\\\"102\\\" class=\\\"img-fluid\\\" alt=\\\"\\\" style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" data-mce-style=\\\"display: block; margin-left: auto; margin-right: auto;\\\" data-mce-src=\\\"/uploads/images/_1416265748.png\\\">Livingsocial Member&nbsp;<br><br>&nbsp;</div><div class=\\\"eight columns mceEditable\\\" contenteditable=\\\"true\\\"><p><span style=\\\"font-size: 12pt;\\\" data-mce-style=\\\"font-size: 12pt;\\\">\\\"Incredible! I would give this place 6 stars if I could! It is painless, relaxing, clean, professional - everyone there is AMAZING - they make you feel so comfortable. And it works too! I lost one and half inches off my stomach area which has been such a problem area for me! I will be back!\\\"&nbsp;-&nbsp; Joyce W</span></p></div></div></section></div><div style=\\\"text-align: left;\\\" data-mce-style=\\\"text-align: left;\\\"><br></div><p><br></p>\"}',NULL,'{\"path\":\"/uploads/images/contacts.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":1919,\"height\":612,\"type\":\"initial\",\"rotation\":null}}','\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',4,'2018-11-25 19:33:57','2018-11-14 08:49:38','2018-11-25 19:33:57'),
	(8,0,'{\"en\":\"CC Facials\"}','cc-facials',NULL,NULL,NULL,NULL,NULL,NULL,'on',3,'2018-11-25 19:33:57','2018-11-21 16:49:09','2018-11-25 19:33:57'),
	(9,0,'{\"en\":\"Who we are\"}','who-we-are','{\"en\":\"TEST\"}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',0,NULL,'2018-11-25 19:34:18','2018-12-20 19:12:32'),
	(10,0,'{\"en\":\"What we do\"}','what-we-do','{\"en\":null}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',1,NULL,'2018-11-26 14:53:08','2018-12-20 19:12:18'),
	(11,0,'{\"en\":\"Our works\"}','our-works',NULL,NULL,NULL,NULL,NULL,NULL,'on',2,NULL,'2018-11-26 14:53:45','2018-11-26 14:54:06'),
	(12,0,'{\"en\":\"Our partners\"}','partners','{\"en\":null}',NULL,NULL,'\"{\\\"en\\\":null}\"','\"{\\\"en\\\":null}\"',NULL,'on',3,NULL,'2018-11-26 14:53:51','2018-11-26 16:54:43'),
	(13,0,'{\"en\":\"Contacts\"}','contacts',NULL,NULL,NULL,NULL,NULL,NULL,'on',4,NULL,'2018-11-26 14:54:02','2018-11-26 14:54:16');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table partials
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partials`;

CREATE TABLE `partials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `keyvalue` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table portfolio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolio`;

CREATE TABLE `portfolio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` longtext COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;

INSERT INTO `portfolio` (`id`, `name`, `slug`, `categories`, `keywords`, `description`, `params`, `status`, `deleted_at`, `created_at`, `updated_at`, `content`)
VALUES
	(1,'{\"en\":\"OUR WORKS\"}','our-works','[{\"name\":{\"en\":\"DESIGN\"},\"slug\":null},{\"name\":{\"en\":\"PHOTOGRAPHY\"},\"slug\":null},{\"name\":{\"en\":\"VIDEO\"},\"slug\":null},{\"name\":{\"en\":\"SOUNDTRACKS\"},\"slug\":null}]',NULL,NULL,NULL,'draft',NULL,'2018-12-20 18:13:01','2018-12-20 18:25:34','\"{}\"');

/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table portfolio-items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolio-items`;

CREATE TABLE `portfolio-items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` longtext COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('on','off','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `position` smallint(6) NOT NULL DEFAULT '9999',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `portfolio-items` WRITE;
/*!40000 ALTER TABLE `portfolio-items` DISABLE KEYS */;

INSERT INTO `portfolio-items` (`id`, `portfolio_id`, `type`, `content`, `name`, `category`, `slug`, `info`, `params`, `status`, `position`, `deleted_at`, `created_at`, `updated_at`, `keywords`, `description`)
VALUES
	(139,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/2.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":800,\"height\":703,\"type\":\"manual\",\"rotation\":0}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"darkred\"},\"parent\":null,\"color\":\"#BFEECD\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',0,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(140,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/3.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":703,\"width\":800,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"green\"},\"parent\":null,\"color\":\"#16F415\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',1,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(141,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/1.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":800,\"height\":703,\"type\":\"manual\",\"rotation\":0}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"lightblue\"},\"parent\":null,\"color\":\"#118D27\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',2,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(142,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/4.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":400,\"height\":352,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"purple\"},\"parent\":null,\"color\":\"#53BA92\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',3,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(143,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/5.jpg\",\"params\":{\"x\":0,\"y\":0,\"width\":800,\"height\":703,\"type\":\"initial\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"orange\"},\"parent\":null,\"color\":\"#94A2CE\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',4,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(144,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/6.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":703,\"width\":800,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"greenyellow\"},\"parent\":null,\"color\":\"#90FE2F\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',5,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(145,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/7.jpg\",\"params\":{\"x\":0,\"y\":0,\"height\":703,\"width\":800,\"type\":\"auto\",\"rotation\":null}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"magenta\"},\"parent\":null,\"color\":\"#70AB31\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',6,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]'),
	(146,1,'image','{\"image\":{\"path\":\"\\/uploads\\/images\\/portfolio\\/8.jpg\",\"params\":{\"x\":0,\"y\":0.0712915560841254,\"width\":612.7608118361153,\"height\":532.7147765261973,\"type\":\"manual\",\"rotation\":0}},\"video\":{\"id\":null,\"type\":\"youtube\"},\"params\":[{\"key\":\"color\",\"value\":{\"en\":\"yellow\"},\"parent\":null,\"color\":\"#F9688A\",\"multilang\":true,\"type\":\"text\"}]}','{\"en\":\"#\"}','DESIGN',NULL,'[]',NULL,'draft',7,NULL,'2018-12-21 08:48:45','2018-12-21 08:48:45','[]','[]');

/*!40000 ALTER TABLE `portfolio-items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'user',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table verify_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `verify_users`;

CREATE TABLE `verify_users` (
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
